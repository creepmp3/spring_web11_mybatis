/**
 * 2015. 6. 11.
 * @author KDY
 */
package kr.co.hbilab.dto;

public class BoardDTO {
    private int bno;
    private String writer;
    private String title;
    private String contents;
    private String ip;
    private int readcount;
    private int hits;
    private String status;
    private String regdate;
    
    public BoardDTO() {
    }
    
    public BoardDTO(int bno, String writer, String title, String contents, String ip,
            int readcount, int hits, String status, String regdate) {
        super();
        this.bno = bno;
        this.writer = writer;
        this.title = title;
        this.contents = contents;
        this.ip = ip;
        this.readcount = readcount;
        this.hits = hits;
        this.status = status;
        this.regdate = regdate;
    }
    
    public int getBno() {
        return bno;
    }
    
    public void setBno(int bno) {
        this.bno = bno;
    }
    
    public String getWriter() {
        return writer;
    }
    
    public void setWriter(String writer) {
        this.writer = writer;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getContents() {
        return contents;
    }
    
    public void setContents(String contents) {
        this.contents = contents;
    }
    
    public String getIp() {
        return ip;
    }
    
    public void setIp(String ip) {
        this.ip = ip;
    }
    
    public int getReadcount() {
        return readcount;
    }
    
    public void setReadcount(int readcount) {
        this.readcount = readcount;
    }
    
    public int getHits() {
        return hits;
    }
    
    public void setHits(int hits) {
        this.hits = hits;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
    
    public String getRegdate() {
        return regdate;
    }
    
    public void setRegdate(String regdate) {
        this.regdate = regdate;
    }
    
}
