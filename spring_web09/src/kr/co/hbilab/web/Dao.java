package kr.co.hbilab.web;

import java.util.List;

/**
 * 2015. 6. 2.
 * @author KDY
 */
public interface Dao {
    public List<MemDTO> selectAll();
    public boolean loginChk(String id, String pw);
}
