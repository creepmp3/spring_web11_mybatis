<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="/WEB-INF/view/common/header.jsp"></jsp:include>
<script type="text/javascript">
function logout(){
	if(confirm("로그아웃 하시겠습니까")){
		location.href = "/spring_web09/logout";
		return false;
	}
}
</script>
    
    <c:if test="${sessionScope.id != null }">
        <strong><u>${sessionScope.id }</u></strong>
        님 환영합니다<br/>
        <a href="javascript:logout()">로그아웃</a>
    </c:if>
    <c:if test="${sessionScope.id == null }">
        로그인이 필요합니다
    </c:if>
</body>
</html>