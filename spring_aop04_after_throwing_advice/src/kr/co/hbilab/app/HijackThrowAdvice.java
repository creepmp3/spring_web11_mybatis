package kr.co.hbilab.app;

import org.springframework.aop.ThrowsAdvice;

public class HijackThrowAdvice implements ThrowsAdvice{
    public void afterThrowing(IllegalArgumentException e) throws Throwable{
        System.out.println("HijackThrow Exception : "+e.fillInStackTrace());
    }
}
