package dao;

public class ZipcodeVO {
	private int totalcount;
	private String zipcode_v;
	private String index_v;
	private String si_v;
	private String gu_v;
	private String dong_v;
	private String ri_v;
	private String doser_v;
	private String bunji_v;
	private String building_v;
	private String update_v;
	private String addr_v;
	
	
	public ZipcodeVO() {
	}
	
	public ZipcodeVO(String zipcode_v, String index_v, String si_v, String gu_v, String dong_v, String ri_v,
			String doser_v, String bunji_v, String building_v, String update_v, String addr_v) {
		super();
		this.zipcode_v = zipcode_v;
		this.index_v = index_v;
		this.si_v = si_v;
		this.gu_v = gu_v;
		this.dong_v = dong_v;
		this.ri_v = ri_v;
		this.doser_v = doser_v;
		this.bunji_v = bunji_v;
		this.building_v = building_v;
		this.update_v = update_v;
		this.addr_v = addr_v;
	}


	
	public ZipcodeVO(int totalcount, String zipcode_v, String index_v, String si_v, String gu_v,
			String dong_v, String ri_v, String doser_v, String bunji_v, String building_v, String update_v,
			String addr_v) {
		super();
		this.totalcount = totalcount;
		this.zipcode_v = zipcode_v;
		this.index_v = index_v;
		this.si_v = si_v;
		this.gu_v = gu_v;
		this.dong_v = dong_v;
		this.ri_v = ri_v;
		this.doser_v = doser_v;
		this.bunji_v = bunji_v;
		this.building_v = building_v;
		this.update_v = update_v;
		this.addr_v = addr_v;
	}

	public int getTotalcount() {
		return totalcount;
	}
	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}
	public String getZipcode_v() {
		return zipcode_v;
	}
	public void setZipcode_v(String zipcode_v) {
		this.zipcode_v = zipcode_v;
	}
	public String getIndex_v() {
		return index_v;
	}
	public void setIndex_v(String index_v) {
		this.index_v = index_v;
	}
	public String getSi_v() {
		return si_v;
	}
	public void setSi_v(String si_v) {
		this.si_v = si_v;
	}
	public String getGu_v() {
		return gu_v;
	}
	public void setGu_v(String gu_v) {
		this.gu_v = gu_v;
	}
	public String getDong_v() {
		return dong_v;
	}
	public void setDong_v(String dong_v) {
		this.dong_v = dong_v;
	}
	public String getRi_v() {
		return ri_v;
	}
	public void setRi_v(String ri_v) {
		this.ri_v = ri_v;
	}
	public String getDoser_v() {
		return doser_v;
	}
	public void setDoser_v(String doser_v) {
		this.doser_v = doser_v;
	}
	public String getBunji_v() {
		return bunji_v;
	}
	public void setBunji_v(String bunji_v) {
		this.bunji_v = bunji_v;
	}
	public String getBuilding_v() {
		return building_v;
	}
	public void setBuilding_v(String building_v) {
		this.building_v = building_v;
	}
	public String getUpdate_v() {
		return update_v;
	}
	public void setUpdate_v(String update_v) {
		this.update_v = update_v;
	}
	public String getAddr_v() {
		return addr_v;
	}
	public void setAddr_v(String addr_v) {
		this.addr_v = addr_v;
	}
	
	
}
