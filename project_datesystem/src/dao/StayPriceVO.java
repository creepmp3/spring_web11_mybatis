package dao;

public class StayPriceVO {
    private int totalcount;
    private int stay_price_no_n;
    private int stay_no_n;
    private String stay_price_nm_v;
    private String stay_d_w_v;
    private String stay_d_h_v;
    private String stay_s_w_v;
    private String stay_s_h_v;
    private String stay_use_yn_v;
    private String stay_reg_d;




    public StayPriceVO() {
    }




    public StayPriceVO(int stay_no_n, int stay_price_no_n) {
        super();
        this.stay_no_n = stay_no_n;
        this.stay_price_no_n = stay_price_no_n;
    }

    public StayPriceVO(String stay_price_nm_v, String stay_d_w_v, String stay_d_h_v, String stay_s_w_v,
            String stay_s_h_v, String stay_use_yn_v, String stay_reg_d) {
        super();
        this.stay_price_nm_v = stay_price_nm_v;
        this.stay_d_w_v = stay_d_w_v;
        this.stay_d_h_v = stay_d_h_v;
        this.stay_s_w_v = stay_s_w_v;
        this.stay_s_h_v = stay_s_h_v;
        this.stay_use_yn_v = stay_use_yn_v;
        this.stay_reg_d = stay_reg_d;
    }




    public StayPriceVO(int stay_no_n, int stay_price_no_n, String stay_price_nm_v, String stay_d_w_v,
            String stay_d_h_v, String stay_s_w_v, String stay_s_h_v) {
        super();
        this.stay_no_n = stay_no_n;
        this.stay_price_no_n = stay_price_no_n;
        this.stay_price_nm_v = stay_price_nm_v;
        this.stay_d_w_v = stay_d_w_v;
        this.stay_d_h_v = stay_d_h_v;
        this.stay_s_w_v = stay_s_w_v;
        this.stay_s_h_v = stay_s_h_v;
    }




    public StayPriceVO(int stay_no_n, String stay_price_nm_v, String stay_d_w_v, String stay_d_h_v,
            String stay_s_w_v, String stay_s_h_v, String stay_use_yn_v) {
        super();
        this.stay_no_n = stay_no_n;
        this.stay_price_nm_v = stay_price_nm_v;
        this.stay_d_w_v = stay_d_w_v;
        this.stay_d_h_v = stay_d_h_v;
        this.stay_s_w_v = stay_s_w_v;
        this.stay_s_h_v = stay_s_h_v;
        this.stay_use_yn_v = stay_use_yn_v;
    }




    public StayPriceVO(int totalcount, int stay_price_no_n, String stay_price_nm_v, String stay_d_w_v, String stay_d_h_v,
            String stay_s_w_v, String stay_s_h_v, String stay_use_yn_v, String stay_reg_d) {
        super();
        this.totalcount = totalcount;
        this.stay_price_no_n = stay_price_no_n;
        this.stay_price_nm_v = stay_price_nm_v;
        this.stay_d_w_v = stay_d_w_v;
        this.stay_d_h_v = stay_d_h_v;
        this.stay_s_w_v = stay_s_w_v;
        this.stay_s_h_v = stay_s_h_v;
        this.stay_use_yn_v = stay_use_yn_v;
        this.stay_reg_d = stay_reg_d;
    }


    public int getTotalcount() {
        return totalcount;
    }
    public void setTotalcount(int totalcount) {
        this.totalcount = totalcount;
    }
    public int getStay_price_no_n() {
        return stay_price_no_n;
    }
    public void setStay_price_no_n(int stay_price_no_n) {
        this.stay_price_no_n = stay_price_no_n;
    }
    public String getStay_price_nm_v() {
        return stay_price_nm_v;
    }
    public void setStay_price_nm_v(String stay_price_nm_v) {
        this.stay_price_nm_v = stay_price_nm_v;
    }
    public int getStay_no_n() {
        return stay_no_n;
    }
    public void setStay_no_n(int stay_no_n) {
        this.stay_no_n = stay_no_n;
    }
    public String getStay_d_w_v() {
        return stay_d_w_v;
    }
    public void setStay_d_w_v(String stay_d_w_v) {
        this.stay_d_w_v = stay_d_w_v;
    }
    public String getStay_d_h_v() {
        return stay_d_h_v;
    }
    public void setStay_d_h_v(String stay_d_h_v) {
        this.stay_d_h_v = stay_d_h_v;
    }
    public String getStay_s_w_v() {
        return stay_s_w_v;
    }
    public void setStay_s_w_v(String stay_s_w_v) {
        this.stay_s_w_v = stay_s_w_v;
    }
    public String getStay_s_h_v() {
        return stay_s_h_v;
    }
    public void setStay_s_h_v(String stay_s_h_v) {
        this.stay_s_h_v = stay_s_h_v;
    }
    public String getStay_use_yn_v() {
        return stay_use_yn_v;
    }
    public void setStay_use_yn_v(String stay_use_yn_v) {
        this.stay_use_yn_v = stay_use_yn_v;
    }
    public String getStay_reg_d() {
        return stay_reg_d;
    }
    public void setStay_reg_d(String stay_reg_d) {
        this.stay_reg_d = stay_reg_d;
    }


}
