package dao;

public class StayVO {
    private int totalcount;
    private int stay_no_n;
    private String stay_title_v;
    private String stay_info_title_v;
    private String stay_content_v;
    private String stay_tel_v;
    private String stay_open_time_v;
    private String stay_close_time_v;
    private String stay_price_v;
    private String stay_holiday_v;
    private String stay_type_v;
    private String stay_loc_v;
    private int stay_read_n;
    private int stay_recommend_n;
    private String stay_use_yn_v;
    private String stay_reg_d;
    private String stay_imgpath_1_v;
    private String stay_imgpath_2_v;
    private String stay_imgpath_3_v;
    private String stay_imgpath_4_v;
    private String stay_imgpath_5_v;
    private String stay_imgpath_6_v;
    private String stay_imgpath_7_v;
    private String stay_imgpath_8_v;

    public StayVO() {
    }

    public StayVO(int stay_no_n) {
        super();
        this.stay_no_n = stay_no_n;
    }

    public StayVO(int totalcount, int stay_no_n, String stay_title_v, String stay_info_title_v,
            String stay_imgpath_1_v, String stay_type_v, String stay_tel_v, String stay_loc_v, int stay_read_n, int stay_recommend_n,
            String stay_reg_d) {
        super();
        this.totalcount = totalcount;
        this.stay_no_n = stay_no_n;
        this.stay_title_v = stay_title_v;
        this.stay_info_title_v = stay_info_title_v;
        this.stay_tel_v = stay_tel_v;
        this.stay_type_v = stay_type_v;
        this.stay_loc_v = stay_loc_v;
        this.stay_read_n = stay_read_n;
        this.stay_recommend_n = stay_recommend_n;
        this.stay_reg_d = stay_reg_d;
        this.stay_imgpath_1_v = stay_imgpath_1_v;
    }

    public StayVO(int totalcount, int stay_no_n, String stay_title_v, String stay_type_v, String stay_loc_v,
            String stay_reg_d) {
        super();
        this.totalcount = totalcount;
        this.stay_no_n = stay_no_n;
        this.stay_title_v = stay_title_v;
        this.stay_type_v = stay_type_v;
        this.stay_loc_v = stay_loc_v;
        this.stay_reg_d = stay_reg_d;
    }

    public StayVO(String stay_title_v, String stay_info_title_v, String stay_content_v, String stay_tel_v,
            String stay_open_time_v, String stay_close_time_v, String stay_price_v, String stay_holiday_v,
            String stay_type_v, String stay_loc_v, String stay_imgpath_1_v, String stay_imgpath_2_v,
            String stay_imgpath_3_v, String stay_imgpath_4_v, String stay_imgpath_5_v,
            String stay_imgpath_6_v, String stay_imgpath_7_v, String stay_imgpath_8_v) {
        super();
        this.stay_title_v = stay_title_v;
        this.stay_info_title_v = stay_info_title_v;
        this.stay_content_v = stay_content_v;
        this.stay_tel_v = stay_tel_v;
        this.stay_open_time_v = stay_open_time_v;
        this.stay_close_time_v = stay_close_time_v;
        this.stay_price_v = stay_price_v;
        this.stay_holiday_v = stay_holiday_v;
        this.stay_type_v = stay_type_v;
        this.stay_loc_v = stay_loc_v;
        this.stay_imgpath_1_v = stay_imgpath_1_v;
        this.stay_imgpath_2_v = stay_imgpath_2_v;
        this.stay_imgpath_3_v = stay_imgpath_3_v;
        this.stay_imgpath_4_v = stay_imgpath_4_v;
        this.stay_imgpath_5_v = stay_imgpath_5_v;
        this.stay_imgpath_6_v = stay_imgpath_6_v;
        this.stay_imgpath_7_v = stay_imgpath_7_v;
        this.stay_imgpath_8_v = stay_imgpath_8_v;
    }

    public StayVO(int stay_no_n, String stay_title_v, String stay_info_title_v, String stay_content_v,
            String stay_tel_v, String stay_open_time_v, String stay_close_time_v, String stay_price_v,
            String stay_holiday_v, String stay_type_v, String stay_loc_v, String stay_imgpath_1_v,
            String stay_imgpath_2_v, String stay_imgpath_3_v, String stay_imgpath_4_v,
            String stay_imgpath_5_v, String stay_imgpath_6_v, String stay_imgpath_7_v, String stay_imgpath_8_v) {
        super();
        this.stay_no_n = stay_no_n;
        this.stay_title_v = stay_title_v;
        this.stay_info_title_v = stay_info_title_v;
        this.stay_content_v = stay_content_v;
        this.stay_tel_v = stay_tel_v;
        this.stay_open_time_v = stay_open_time_v;
        this.stay_close_time_v = stay_close_time_v;
        this.stay_price_v = stay_price_v;
        this.stay_holiday_v = stay_holiday_v;
        this.stay_type_v = stay_type_v;
        this.stay_loc_v = stay_loc_v;
        this.stay_imgpath_1_v = stay_imgpath_1_v;
        this.stay_imgpath_2_v = stay_imgpath_2_v;
        this.stay_imgpath_3_v = stay_imgpath_3_v;
        this.stay_imgpath_4_v = stay_imgpath_4_v;
        this.stay_imgpath_5_v = stay_imgpath_5_v;
        this.stay_imgpath_6_v = stay_imgpath_6_v;
        this.stay_imgpath_7_v = stay_imgpath_7_v;
        this.stay_imgpath_8_v = stay_imgpath_8_v;
    }

    public StayVO(int stay_no_n, String stay_title_v, String stay_info_title_v, String stay_content_v,
            String stay_tel_v, String stay_open_time_v, String stay_close_time_v, String stay_price_v,
            String stay_holiday_v, String stay_type_v, String stay_loc_v, int stay_read_n, int stay_recommend_n, String stay_use_yn_v,
            String stay_reg_d, String stay_imgpath_1_v, String stay_imgpath_2_v, String stay_imgpath_3_v,
            String stay_imgpath_4_v, String stay_imgpath_5_v, String stay_imgpath_6_v,
            String stay_imgpath_7_v, String stay_imgpath_8_v) {
        super();
        this.stay_no_n = stay_no_n;
        this.stay_title_v = stay_title_v;
        this.stay_info_title_v = stay_info_title_v;
        this.stay_content_v = stay_content_v;
        this.stay_tel_v = stay_tel_v;
        this.stay_open_time_v = stay_open_time_v;
        this.stay_close_time_v = stay_close_time_v;
        this.stay_price_v = stay_price_v;
        this.stay_holiday_v = stay_holiday_v;
        this.stay_type_v = stay_type_v;
        this.stay_loc_v = stay_loc_v;
        this.stay_read_n = stay_read_n;
        this.stay_recommend_n = stay_recommend_n;
        this.stay_use_yn_v = stay_use_yn_v;
        this.stay_reg_d = stay_reg_d;
        this.stay_imgpath_1_v = stay_imgpath_1_v;
        this.stay_imgpath_2_v = stay_imgpath_2_v;
        this.stay_imgpath_3_v = stay_imgpath_3_v;
        this.stay_imgpath_4_v = stay_imgpath_4_v;
        this.stay_imgpath_5_v = stay_imgpath_5_v;
        this.stay_imgpath_6_v = stay_imgpath_6_v;
        this.stay_imgpath_7_v = stay_imgpath_7_v;
        this.stay_imgpath_8_v = stay_imgpath_8_v;
    }

    public int getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(int totalcount) {
        this.totalcount = totalcount;
    }

    public int getStay_no_n() {
        return stay_no_n;
    }

    public void setStay_no_n(int stay_no_n) {
        this.stay_no_n = stay_no_n;
    }

    public String getStay_title_v() {
        return stay_title_v;
    }

    public void setStay_title_v(String stay_title_v) {
        this.stay_title_v = stay_title_v;
    }

    public String getStay_info_title_v() {
        return stay_info_title_v;
    }

    public void setStay_info_title_v(String stay_info_title_v) {
        this.stay_info_title_v = stay_info_title_v;
    }

    public String getStay_content_v() {
        return stay_content_v;
    }

    public void setStay_content_v(String stay_content_v) {
        this.stay_content_v = stay_content_v;
    }

    public String getStay_tel_v() {
        return stay_tel_v;
    }

    public void setStay_tel_v(String stay_tel_v) {
        this.stay_tel_v = stay_tel_v;
    }

    public String getStay_open_time_v() {
        return stay_open_time_v;
    }

    public void setStay_open_time_v(String stay_open_time_v) {
        this.stay_open_time_v = stay_open_time_v;
    }

    public String getStay_close_time_v() {
        return stay_close_time_v;
    }

    public void setStay_close_time_v(String stay_close_time_v) {
        this.stay_close_time_v = stay_close_time_v;
    }

    public String getStay_price_v() {
        return stay_price_v;
    }

    public void setStay_price_v(String stay_price_v) {
        this.stay_price_v = stay_price_v;
    }

    public String getStay_holiday_v() {
        return stay_holiday_v;
    }

    public void setStay_holiday_v(String stay_holiday_v) {
        this.stay_holiday_v = stay_holiday_v;
    }

    public String getStay_type_v() {
        return stay_type_v;
    }

    public void setStay_type_v(String stay_type_v) {
        this.stay_type_v = stay_type_v;
    }

    public String getStay_loc_v() {
        return stay_loc_v;
    }

    public void setStay_loc_v(String stay_loc_v) {
        this.stay_loc_v = stay_loc_v;
    }

    public int getStay_read_n() {
		return stay_read_n;
	}

	public void setStay_read_n(int stay_read_n) {
		this.stay_read_n = stay_read_n;
	}

	public int getStay_recommend_n() {
		return stay_recommend_n;
	}

	public void setStay_recommend_n(int stay_recommend_n) {
		this.stay_recommend_n = stay_recommend_n;
	}

	public String getStay_use_yn_v() {
        return stay_use_yn_v;
    }

    public void setStay_use_yn_v(String stay_use_yn_v) {
        this.stay_use_yn_v = stay_use_yn_v;
    }

    public String getStay_reg_d() {
        return stay_reg_d;
    }

    public void setStay_reg_d(String stay_reg_d) {
        this.stay_reg_d = stay_reg_d;
    }

    public String getStay_imgpath_1_v() {
        return stay_imgpath_1_v;
    }

    public void setStay_imgpath_1_v(String stay_imgpath_1_v) {
        this.stay_imgpath_1_v = stay_imgpath_1_v;
    }

    public String getStay_imgpath_2_v() {
        return stay_imgpath_2_v;
    }

    public void setStay_imgpath_2_v(String stay_imgpath_2_v) {
        this.stay_imgpath_2_v = stay_imgpath_2_v;
    }

    public String getStay_imgpath_3_v() {
        return stay_imgpath_3_v;
    }

    public void setStay_imgpath_3_v(String stay_imgpath_3_v) {
        this.stay_imgpath_3_v = stay_imgpath_3_v;
    }

    public String getStay_imgpath_4_v() {
        return stay_imgpath_4_v;
    }

    public void setStay_imgpath_4_v(String stay_imgpath_4_v) {
        this.stay_imgpath_4_v = stay_imgpath_4_v;
    }

    public String getStay_imgpath_5_v() {
        return stay_imgpath_5_v;
    }

    public void setStay_imgpath_5_v(String stay_imgpath_5_v) {
        this.stay_imgpath_5_v = stay_imgpath_5_v;
    }

    public String getStay_imgpath_6_v() {
        return stay_imgpath_6_v;
    }

    public void setStay_imgpath_6_v(String stay_imgpath_6_v) {
        this.stay_imgpath_6_v = stay_imgpath_6_v;
    }

    public String getStay_imgpath_7_v() {
        return stay_imgpath_7_v;
    }

    public void setStay_imgpath_7_v(String stay_imgpath_7_v) {
        this.stay_imgpath_7_v = stay_imgpath_7_v;
    }

    public String getStay_imgpath_8_v() {
        return stay_imgpath_8_v;
    }

    public void setStay_imgpath_8_v(String stay_imgpath_8_v) {
        this.stay_imgpath_8_v = stay_imgpath_8_v;
    }

}
