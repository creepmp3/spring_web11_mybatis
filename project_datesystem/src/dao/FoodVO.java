package dao;

public class FoodVO {
	private int totalcount;
	private int food_no_n;
	private String food_title_v;
	private String food_info_title_v;
	private String food_content_v;
	private String food_tel_v;
	private String food_open_time_v;
	private String food_close_time_v;
	private String food_price_v;
	private String food_holiday_v;
	private String food_type_v;
	private String food_loc_v;
	private int food_read_n;
	private int food_recommend_n;
	private String food_use_yn_c;
	private String food_reg_d;
	private String food_imgpath_1_v;
	private String food_imgpath_2_v;
	private String food_imgpath_3_v;
	private String food_imgpath_4_v;
	private String food_imgpath_5_v;
	private String food_imgpath_6_v;
	private String food_imgpath_7_v;
	private String food_imgpath_8_v;
	
	public FoodVO() {
		
	}
	
	
	
	
	public FoodVO(int food_no_n, String food_title_v, String food_info_title_v, String food_content_v,
			String food_tel_v, String food_open_time_v, String food_close_time_v,
			String food_price_v, String food_holiday_v, String food_type_v, String food_loc_v,
			String food_imgpath_1_v, String food_imgpath_2_v, String food_imgpath_3_v,
			String food_imgpath_4_v, String food_imgpath_5_v, String food_imgpath_6_v,
			String food_imgpath_7_v, String food_imgpath_8_v) {
		this.food_no_n = food_no_n;
		this.food_title_v = food_title_v;
		this.food_info_title_v = food_info_title_v;
		this.food_content_v = food_content_v;
		this.food_tel_v = food_tel_v;
		this.food_open_time_v = food_open_time_v;
		this.food_close_time_v = food_close_time_v;
		this.food_price_v = food_price_v;
		this.food_holiday_v = food_holiday_v;
		this.food_type_v = food_type_v;
		this.food_loc_v = food_loc_v;
		this.food_imgpath_1_v = food_imgpath_1_v;
		this.food_imgpath_2_v = food_imgpath_2_v;
		this.food_imgpath_3_v = food_imgpath_3_v;
		this.food_imgpath_4_v = food_imgpath_4_v;
		this.food_imgpath_5_v = food_imgpath_5_v;
		this.food_imgpath_6_v = food_imgpath_6_v;
		this.food_imgpath_7_v = food_imgpath_7_v;
		this.food_imgpath_8_v = food_imgpath_8_v;
		
	}
			
			
	
	
	public FoodVO(int food_no_n, String food_title_v, String food_info_title_v, String food_content_v,
            String food_tel_v, String food_open_time_v, String food_close_time_v, String food_price_v,
            String food_holiday_v, String food_type_v, String food_loc_v, int food_read_n, int food_recommend_n, String food_use_yn_c,
            String food_reg_d, String food_imgpath_1_v, String food_imgpath_2_v, String food_imgpath_3_v,
            String food_imgpath_4_v, String food_imgpath_5_v, String food_imgpath_6_v,
            String food_imgpath_7_v, String food_imgpath_8_v) {
        super();
        this.food_no_n = food_no_n;
        this.food_title_v = food_title_v;
        this.food_info_title_v = food_info_title_v;
        this.food_content_v = food_content_v;
        this.food_tel_v = food_tel_v;
        this.food_open_time_v = food_open_time_v;
        this.food_close_time_v = food_close_time_v;
        this.food_price_v = food_price_v;
        this.food_holiday_v = food_holiday_v;
        this.food_type_v = food_type_v;
        this.food_loc_v = food_loc_v;
        this.food_read_n = food_read_n;
        this.food_recommend_n = food_recommend_n;
        this.food_use_yn_c = food_use_yn_c;
        this.food_reg_d = food_reg_d;
        this.food_imgpath_1_v = food_imgpath_1_v;
        this.food_imgpath_2_v = food_imgpath_2_v;
        this.food_imgpath_3_v = food_imgpath_3_v;
        this.food_imgpath_4_v = food_imgpath_4_v;
        this.food_imgpath_5_v = food_imgpath_5_v;
        this.food_imgpath_6_v = food_imgpath_6_v;
        this.food_imgpath_7_v = food_imgpath_7_v;
        this.food_imgpath_8_v = food_imgpath_8_v;
    }
	
	public FoodVO(int totalcount, int food_no_n, String food_title_v, String food_type_v, String food_loc_v,
            String food_use_yn_c, String food_reg_d) {
        super();
        this.totalcount = totalcount;
        this.food_no_n = food_no_n;
        this.food_title_v = food_title_v;
        this.food_type_v = food_type_v;
        this.food_loc_v = food_loc_v;
        this.food_use_yn_c = food_use_yn_c;
        this.food_reg_d = food_reg_d;
    }
	
	public FoodVO( String food_title_v, String food_info_title_v, String food_content_v,
			String food_tel_v, String food_open_time_v,
			String food_close_time_v, String food_price_v,
			String food_holiday_v, String food_type_v, String food_loc_v,
			String food_use_yn_c, String food_imgpath_1_v,
			String food_imgpath_2_v, String food_imgpath_3_v,
			String food_imgpath_4_v, String food_imgpath_5_v,
			String food_imgpath_6_v, String food_imgpath_7_v,
			String food_imgpath_8_v) {
		super();
		this.food_title_v = food_title_v;
		this.food_info_title_v = food_info_title_v;
		this.food_content_v = food_content_v;
		this.food_tel_v = food_tel_v;
		this.food_open_time_v = food_open_time_v;
		this.food_close_time_v = food_close_time_v;
		this.food_price_v = food_price_v;
		this.food_holiday_v = food_holiday_v;
		this.food_type_v = food_type_v;
		this.food_loc_v = food_loc_v;
		this.food_use_yn_c = food_use_yn_c;
		this.food_imgpath_1_v = food_imgpath_1_v;
		this.food_imgpath_2_v = food_imgpath_2_v;
		this.food_imgpath_3_v = food_imgpath_3_v;
		this.food_imgpath_4_v = food_imgpath_4_v;
		this.food_imgpath_5_v = food_imgpath_5_v;
		this.food_imgpath_6_v = food_imgpath_6_v;
		this.food_imgpath_7_v = food_imgpath_7_v;
		this.food_imgpath_8_v = food_imgpath_8_v;
	}
	
	
	public FoodVO(int food_no_n, String food_title_v, String food_content_v,
			String food_tel_v, String food_open_time_v,
			String food_close_time_v, String food_price_v,
			String food_holiday_v, String food_type_v, String food_loc_v,
			String food_use_yn_c, String food_reg_d, String food_imgpath_1_v,
			String food_imgpath_2_v, String food_imgpath_3_v,
			String food_imgpath_4_v, String food_imgpath_5_v,
			String food_imgpath_6_v, String food_imgpath_7_v,
			String food_imgpath_8_v) {
		super();
		this.food_no_n = food_no_n;
		this.food_title_v = food_title_v;
		this.food_content_v = food_content_v;
		this.food_tel_v = food_tel_v;
		this.food_open_time_v = food_open_time_v;
		this.food_close_time_v = food_close_time_v;
		this.food_price_v = food_price_v;
		this.food_holiday_v = food_holiday_v;
		this.food_type_v = food_type_v;
		this.food_loc_v = food_loc_v;
		this.food_use_yn_c = food_use_yn_c;
		this.food_reg_d = food_reg_d;
		this.food_imgpath_1_v = food_imgpath_1_v;
		this.food_imgpath_2_v = food_imgpath_2_v;
		this.food_imgpath_3_v = food_imgpath_3_v;
		this.food_imgpath_4_v = food_imgpath_4_v;
		this.food_imgpath_5_v = food_imgpath_5_v;
		this.food_imgpath_6_v = food_imgpath_6_v;
		this.food_imgpath_7_v = food_imgpath_7_v;
		this.food_imgpath_8_v = food_imgpath_8_v;
	}

	
	public FoodVO(int totalcount, int food_no_n, String food_title_v, String food_info_title_v,
			String food_imgpath_1_v, String food_type_v, String food_tel_v, String food_loc_v, int food_read_n, int food_recommend_n,
			String food_reg_d) {
		this.totalcount = totalcount;
        this.food_no_n = food_no_n;
        this.food_title_v = food_title_v;
        this.food_info_title_v = food_info_title_v;
        this.food_tel_v = food_tel_v;
        this.food_type_v = food_type_v;
        this.food_loc_v = food_loc_v;
        this.food_read_n = food_read_n;
        this.food_recommend_n = food_recommend_n;
        this.food_reg_d = food_reg_d;
        this.food_imgpath_1_v = food_imgpath_1_v;
	}
	
	
	
	public String getFood_info_title_v() {
		return food_info_title_v;
	}

	public void setFood_info_title_v(String food_info_title_v) {
		this.food_info_title_v = food_info_title_v;
	}

	public int getFood_read_n() {
		return food_read_n;
	}

	public void setFood_read_n(int food_read_n) {
		this.food_read_n = food_read_n;
	}

	public int getFood_recommend_n() {
		return food_recommend_n;
	}

	public void setFood_recommend_n(int food_recommend_n) {
		this.food_recommend_n = food_recommend_n;
	}

	public int getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(int totalcount) {
		this.totalcount = totalcount;
	}

	public int getFood_no_n() {
		return food_no_n;
	}

	public void setFood_no_n(int food_no_n) {
		this.food_no_n = food_no_n;
	}

	public String getFood_title_v() {
		return food_title_v;
	}

	public void setFood_title_v(String food_title_v) {
		this.food_title_v = food_title_v;
	}

	public String getFood_content_v() {
		return food_content_v;
	}

	public void setFood_content_v(String food_content_v) {
		this.food_content_v = food_content_v;
	}

	public String getFood_tel_v() {
		return food_tel_v;
	}

	public void setFood_tel_v(String food_tel_v) {
		this.food_tel_v = food_tel_v;
	}

	public String getFood_open_time_v() {
		return food_open_time_v;
	}

	public void setFood_open_time_v(String food_open_time_v) {
		this.food_open_time_v = food_open_time_v;
	}

	public String getFood_close_time_v() {
		return food_close_time_v;
	}

	public void setFood_close_time_v(String food_close_time_v) {
		this.food_close_time_v = food_close_time_v;
	}

	public String getFood_price_v() {
		return food_price_v;
	}

	public void setFood_price_v(String food_price_v) {
		this.food_price_v = food_price_v;
	}

	public String getFood_holiday_v() {
		return food_holiday_v;
	}

	public void setFood_holiday_v(String food_holiday_v) {
		this.food_holiday_v = food_holiday_v;
	}

	public String getFood_type_v() {
		return food_type_v;
	}

	public void setFood_type_v(String food_type_v) {
		this.food_type_v = food_type_v;
	}

	public String getFood_loc_v() {
		return food_loc_v;
	}

	public void setFood_loc_v(String food_loc_v) {
		this.food_loc_v = food_loc_v;
	}

	public String getFood_use_yn_c() {
		return food_use_yn_c;
	}

	public void setFood_use_yn_c(String food_use_yn_c) {
		this.food_use_yn_c = food_use_yn_c;
	}

	public String getFood_reg_d() {
		return food_reg_d;
	}

	public void setFood_reg_d(String food_reg_d) {
		this.food_reg_d = food_reg_d;
	}

	public String getFood_imgpath_1_v() {
		return food_imgpath_1_v;
	}

	public void setFood_imgpath_1_v(String food_imgpath_1_v) {
		this.food_imgpath_1_v = food_imgpath_1_v;
	}

	public String getFood_imgpath_2_v() {
		return food_imgpath_2_v;
	}

	public void setFood_imgpath_2_v(String food_imgpath_2_v) {
		this.food_imgpath_2_v = food_imgpath_2_v;
	}

	public String getFood_imgpath_3_v() {
		return food_imgpath_3_v;
	}

	public void setFood_imgpath_3_v(String food_imgpath_3_v) {
		this.food_imgpath_3_v = food_imgpath_3_v;
	}

	public String getFood_imgpath_4_v() {
		return food_imgpath_4_v;
	}

	public void setFood_imgpath_4_v(String food_imgpath_4_v) {
		this.food_imgpath_4_v = food_imgpath_4_v;
	}

	public String getFood_imgpath_5_v() {
		return food_imgpath_5_v;
	}

	public void setFood_imgpath_5_v(String food_imgpath_5_v) {
		this.food_imgpath_5_v = food_imgpath_5_v;
	}

	public String getFood_imgpath_6_v() {
		return food_imgpath_6_v;
	}

	public void setFood_imgpath_6_v(String food_imgpath_6_v) {
		this.food_imgpath_6_v = food_imgpath_6_v;
	}

	public String getFood_imgpath_7_v() {
		return food_imgpath_7_v;
	}

	public void setFood_imgpath_7_v(String food_imgpath_7_v) {
		this.food_imgpath_7_v = food_imgpath_7_v;
	}

	public String getFood_imgpath_8_v() {
		return food_imgpath_8_v;
	}

	public void setFood_imgpath_8_v(String food_imgpath_8_v) {
		this.food_imgpath_8_v = food_imgpath_8_v;
	}



	
	
}



