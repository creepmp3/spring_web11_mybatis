package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import db.MakeConnection;

public class StayPriceDAO {
    Connection conn = null;
    PreparedStatement pstmt = null;
    StringBuffer sql = new StringBuffer();
    ResultSet rs = null;

    public StayPriceDAO(){
        conn = MakeConnection.getInstance().getConnection();
    }

    public ArrayList<StayPriceVO> selectAll(int stay_no_n){
        ArrayList<StayPriceVO> list = new ArrayList<StayPriceVO>();
        sql.setLength(0);
        sql.append("\n SELECT *                                                                                                                             ");
        sql.append("\n    FROM                                                                                                                                ");
        sql.append("\n        (SELECT ROW_NUMBER() OVER(ORDER BY stay_reg_d ASC) rnum        ");
        sql.append("\n              , COUNT(*) OVER()                             totalcount ");
        sql.append("\n              , stay_price_no_n                                       ");
        sql.append("\n              , stay_price_nm_v                                       ");
        sql.append("\n              , stay_d_w_v                                            ");
        sql.append("\n              , stay_d_h_v                                            ");
        sql.append("\n              , stay_s_w_v                                            ");
        sql.append("\n              , stay_s_h_v                                            ");
        sql.append("\n              , stay_use_yn_v                                         ");
        sql.append("\n              , stay_reg_d                                            ");
        sql.append("\n           FROM t_stay_price                                          ");
        sql.append("\n          WHERE stay_no_n = ?                                         ");
        sql.append("\n            AND stay_use_yn_v = 'Y'                                   ");
        sql.append("\n        )                                                             ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, stay_no_n);
            rs = pstmt.executeQuery();

            while(rs.next()){
                int totalcount = rs.getInt("totalcount");
                int stay_price_no_n = rs.getInt("stay_price_no_n");
                String stay_price_nm_v = rs.getString("stay_price_nm_v");
                String stay_d_w_v = rs.getString("stay_d_w_v");
                String stay_d_h_v = rs.getString("stay_d_h_v");
                String stay_s_w_v = rs.getString("stay_s_w_v");
                String stay_s_h_v = rs.getString("stay_s_h_v");
                String stay_use_yn_v = rs.getString("stay_use_yn_v");
                String stay_reg_d = rs.getString("stay_reg_d");

                StayPriceVO vo = new StayPriceVO(totalcount, stay_price_no_n, stay_price_nm_v, stay_d_w_v,
                        stay_d_h_v, stay_s_w_v, stay_s_h_v, stay_use_yn_v, stay_reg_d);
                list.add(vo);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public StayPriceVO selectOne(int stay_no_n, int stay_price_no_n){
        StayPriceVO vo = null;
        sql.setLength(0);
        sql.append("\n SELECT stay_price_nm_v      ");
        sql.append("\n      , stay_d_w_v           ");
        sql.append("\n      , stay_d_h_v           ");
        sql.append("\n      , stay_s_w_v           ");
        sql.append("\n      , stay_s_h_v           ");
        sql.append("\n      , stay_use_yn_v        ");
        sql.append("\n      , stay_reg_d           ");
        sql.append("\n   FROM t_stay_price         ");
        sql.append("\n  WHERE stay_no_n = ?        ");
        sql.append("\n    AND stay_price_no_n = ?  ");

        try {
            pstmt = conn.prepareStatement(sql.toString());
            pstmt.setInt(1, stay_no_n);
            pstmt.setInt(2, stay_price_no_n);
            rs = pstmt.executeQuery();

            if(rs.next()){
                String stay_price_nm_v = rs.getString("stay_price_nm_v");
                String stay_d_w_v = rs.getString("stay_d_w_v");
                String stay_d_h_v = rs.getString("stay_d_h_v");
                String stay_s_w_v = rs.getString("stay_s_w_v");
                String stay_s_h_v = rs.getString("stay_s_h_v");
                String stay_use_yn_v = rs.getString("stay_use_yn_v");
                String stay_reg_d = rs.getString("stay_reg_d");

                vo = new StayPriceVO(stay_price_nm_v, stay_d_w_v,
                        stay_d_h_v, stay_s_w_v, stay_s_h_v, stay_use_yn_v, stay_reg_d);

            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return vo;
    }

    public int insertOne(StayPriceVO vo){
        int result = 0;
        sql.setLength(0);
        sql.append("\n INSERT INTO t_stay_price(stay_price_no_n                  ");
        sql.append("\n                        , stay_no_n                        ");
        sql.append("\n                        , stay_price_nm_v                  ");
        sql.append("\n                        , stay_d_w_v                       ");
        sql.append("\n                        , stay_d_h_v                       ");
        sql.append("\n                        , stay_s_w_v                       ");
        sql.append("\n                        , stay_s_h_v                       ");
        sql.append("\n                        , stay_use_yn_v                    ");
        sql.append("\n                        , stay_reg_d                       ");
        sql.append("\n                         )VALUES(                          ");
        sql.append("\n                            stay_price_no_seq.nextval      "); // stay_price_no_n
        sql.append("\n                            , ?                            "); // stay_no_n
        sql.append("\n                            , ?                            "); // stay_price_nm_v
        sql.append("\n                            , ?                            "); // stay_d_w_v
        sql.append("\n                            , ?                            "); // stay_d_h_v
        sql.append("\n                            , ?                            "); // stay_s_w_v
        sql.append("\n                            , ?                            "); // stay_s_h_v
        sql.append("\n                            , ?                            "); // stay_use_yn_v
        sql.append("\n                            , SYSDATE                      "); // stay_reg_d
        sql.append("\n                           )                               ");

        try {
            pstmt = conn.prepareStatement(sql.toString());

            pstmt.setInt(1, vo.getStay_no_n());
            pstmt.setString(2, vo.getStay_price_nm_v());
            pstmt.setString(3, vo.getStay_d_w_v());
            pstmt.setString(4, vo.getStay_d_h_v());
            pstmt.setString(5, vo.getStay_s_w_v());
            pstmt.setString(6, vo.getStay_s_h_v());
            pstmt.setString(7, vo.getStay_use_yn_v());

            result = pstmt.executeUpdate();

            if(result>0){
                System.out.println("등록성공");
            }else{
                System.out.println("등록실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int updateOne(StayPriceVO vo){
        int result = 0;
        sql.setLength(0);
        sql.append("\n UPDATE t_stay_price SET      ");
        sql.append("\n       stay_price_nm_v = ? ,  ");
        sql.append("\n       stay_d_w_v  = ? ,      ");
        sql.append("\n       stay_d_h_v  = ? ,      ");
        sql.append("\n       stay_s_w_v  = ? ,      ");
        sql.append("\n       stay_s_h_v  = ?        ");
        sql.append("\n  WHERE stay_no_n = ?         ");
        sql.append("\n    AND stay_price_no_n = ?   ");

        try {
            pstmt = conn.prepareStatement(sql.toString());

            pstmt.setString(1, vo.getStay_price_nm_v());
            pstmt.setString(2, vo.getStay_d_w_v());
            pstmt.setString(3, vo.getStay_d_h_v());
            pstmt.setString(4, vo.getStay_s_w_v());
            pstmt.setString(5, vo.getStay_s_h_v());
            pstmt.setInt(6, vo.getStay_no_n());
            pstmt.setInt(7, vo.getStay_price_no_n());

            result = pstmt.executeUpdate();

            if(result>0){
                System.out.println("수정성공");
            }else{
                System.out.println("수정실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int deleteOne(StayPriceVO vo){
        int result = 0;
        sql.setLength(0);
        sql.append("\n UPDATE t_stay_price SET      ");
        sql.append("\n       stay_use_yn_v = 'N'    ");
        sql.append("\n  WHERE stay_no_n = ?         ");
        sql.append("\n    AND stay_price_no_n = ?   ");

        try {
            pstmt = conn.prepareStatement(sql.toString());

            pstmt.setInt(1, vo.getStay_no_n());
            pstmt.setInt(2, vo.getStay_price_no_n());

            result = pstmt.executeUpdate();

            if(result>0){
                System.out.println("삭제성공");
            }else{
                System.out.println("삭제실패");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
