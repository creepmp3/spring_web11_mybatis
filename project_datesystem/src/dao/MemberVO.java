package dao;

public class MemberVO {
    private int count;
    private int totalcount;
    private int member_no_n;
    private String member_id_v;
    private String member_pw_v;
    private String member_nick_v;
    private String member_nm_v;
    private String member_gender_v;
    private String member_bday_v;
    private String member_email_v;
    private String member_tel_v;
    private String member_cell_v;
    private String member_addr_v;
    private String member_ip_v;
    private String member_use_yn_v;
    private String member_type_v;
    private String mem_reg_d;

    public MemberVO() {
        super();
    }

    public MemberVO(int count, int member_no_n, String member_nick_v, String member_gender_v,
            String member_type_v, String member_cell_v) {
        this.count = count;
        this.member_no_n = member_no_n;
        this.member_nick_v = member_nick_v;
        this.member_gender_v = member_gender_v;
        this.member_type_v = member_type_v;
        this.member_cell_v = member_cell_v;
    }

    public MemberVO(String member_id_v, String member_pw_v, String member_nick_v, String member_nm_v,
            String member_gender_v, String member_bday_v, String member_email_v, String member_tel_v,
            String member_cell_v, String member_addr_v, String member_ip_v, String member_type_v) {
        super();
        this.member_id_v = member_id_v;
        this.member_pw_v = member_pw_v;
        this.member_nick_v = member_nick_v;
        this.member_nm_v = member_nm_v;
        this.member_gender_v = member_gender_v;
        this.member_bday_v = member_bday_v;
        this.member_email_v = member_email_v;
        this.member_tel_v = member_tel_v;
        this.member_cell_v = member_cell_v;
        this.member_addr_v = member_addr_v;
        this.member_ip_v = member_ip_v;
        this.member_type_v = member_type_v;
    }

    public MemberVO(int member_no_n, String member_id_v, String member_pw_v, String member_nick_v,
            String member_nm_v, String member_bday_v, String member_email_v, String member_tel_v,
            String member_cell_v, String member_addr_v, String member_ip_v, String member_use_yn_v,
            String member_type_v, String mem_reg_d) {
        super();
        this.member_no_n = member_no_n;
        this.member_id_v = member_id_v;
        this.member_pw_v = member_pw_v;
        this.member_nick_v = member_nick_v;
        this.member_nm_v = member_nm_v;
        this.member_bday_v = member_bday_v;
        this.member_email_v = member_email_v;
        this.member_tel_v = member_tel_v;
        this.member_cell_v = member_cell_v;
        this.member_addr_v = member_addr_v;
        this.member_ip_v = member_ip_v;
        this.member_use_yn_v = member_use_yn_v;
        this.member_type_v = member_type_v;
        this.mem_reg_d = mem_reg_d;
    }

    public MemberVO(int totalcount, int member_no_n, String member_id_v, String member_pw_v,
            String member_nick_v, String member_nm_v, String member_bday_v, String member_email_v,
            String member_tel_v, String member_cell_v, String member_addr_v, String member_ip_v,
            String member_use_yn_v, String member_type_v, String mem_reg_d) {
        super();
        this.totalcount = totalcount;
        this.member_no_n = member_no_n;
        this.member_id_v = member_id_v;
        this.member_pw_v = member_pw_v;
        this.member_nick_v = member_nick_v;
        this.member_nm_v = member_nm_v;
        this.member_bday_v = member_bday_v;
        this.member_email_v = member_email_v;
        this.member_tel_v = member_tel_v;
        this.member_cell_v = member_cell_v;
        this.member_addr_v = member_addr_v;
        this.member_ip_v = member_ip_v;
        this.member_use_yn_v = member_use_yn_v;
        this.member_type_v = member_type_v;
        this.mem_reg_d = mem_reg_d;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(int totalcount) {
        this.totalcount = totalcount;
    }

    public int getMember_no_n() {
        return member_no_n;
    }

    public void setMember_no_n(int member_no_n) {
        this.member_no_n = member_no_n;
    }

    public String getMember_id_v() {
        return member_id_v;
    }

    public void setMember_id_v(String member_id_v) {
        this.member_id_v = member_id_v;
    }

    public String getMember_pw_v() {
        return member_pw_v;
    }

    public void setMember_pw_v(String member_pw_v) {
        this.member_pw_v = member_pw_v;
    }

    public String getMember_nick_v() {
        return member_nick_v;
    }

    public void setMember_nick_v(String member_nick_v) {
        this.member_nick_v = member_nick_v;
    }

    public String getMember_nm_v() {
        return member_nm_v;
    }

    public void setMember_nm_v(String member_nm_v) {
        this.member_nm_v = member_nm_v;
    }

    public String getMember_gender_v() {
        return member_gender_v;
    }

    public void setMember_gender_v(String member_gender_v) {
        this.member_gender_v = member_gender_v;
    }

    public String getMember_bday_v() {
        return member_bday_v;
    }

    public void setMember_bday_v(String member_bday_v) {
        this.member_bday_v = member_bday_v;
    }

    public String getMember_email_v() {
        return member_email_v;
    }

    public void setMember_email_v(String member_email_v) {
        this.member_email_v = member_email_v;
    }

    public String getMember_tel_v() {
        return member_tel_v;
    }

    public void setMember_tel_v(String member_tel_v) {
        this.member_tel_v = member_tel_v;
    }

    public String getMember_cell_v() {
        return member_cell_v;
    }

    public void setMember_cell_v(String member_cell_v) {
        this.member_cell_v = member_cell_v;
    }

    public String getMember_addr_v() {
        return member_addr_v;
    }

    public void setMember_addr_v(String member_addr_v) {
        this.member_addr_v = member_addr_v;
    }

    public String getMember_ip_v() {
        return member_ip_v;
    }

    public void setMember_ip_v(String member_ip_v) {
        this.member_ip_v = member_ip_v;
    }

    public String getMember_use_yn_v() {
        return member_use_yn_v;
    }

    public void setMember_use_yn_v(String member_use_yn_v) {
        this.member_use_yn_v = member_use_yn_v;
    }

    public String getMember_type_v() {
        return member_type_v;
    }

    public void setMember_type_v(String member_type_v) {
        this.member_type_v = member_type_v;
    }

    public String getMem_reg_d() {
        return mem_reg_d;
    }

    public void setMem_reg_d(String mem_reg_d) {
        this.mem_reg_d = mem_reg_d;
    }

}
