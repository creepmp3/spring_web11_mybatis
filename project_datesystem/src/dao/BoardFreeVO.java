/**
 * FileName : BoardFreeVO.java
 * Comment  :
 * @version : 1.0
 * @author  : Kim Doyeon
 * @date    : 2015. 5. 6.
 */
package dao;

public class BoardFreeVO {
    private int totalcount;
    private int lv;
    private int seq_n;
    private int member_no_n;
    private String member_id_v;
    private String member_nick_v;
    private String title_v;
    private String content_v;
    private int read_cnt_n;
    private int ref_n;
    private String use_yn_v;
    private String reg_d;

    public BoardFreeVO() {
    }

    public BoardFreeVO(int seq_n, String title_v, String content_v) {
        super();
        this.seq_n = seq_n;
        this.title_v = title_v;
        this.content_v = content_v;
    }

    public BoardFreeVO(int member_no_n, String title_v, String content_v, int ref_n) {
        super();
        this.member_no_n = member_no_n;
        this.title_v = title_v;
        this.content_v = content_v;
        this.ref_n = ref_n;
    }

    public BoardFreeVO(int member_no_n, String member_id_v, String member_nick_v, int seq_n, String title_v, String content_v, int read_cnt_n, int ref_n,
            String reg_d) {
        super();
        this.member_no_n = member_no_n;
        this.member_id_v = member_id_v;
        this.member_nick_v = member_nick_v;
        this.seq_n = seq_n;
        this.title_v = title_v;
        this.content_v = content_v;
        this.read_cnt_n = read_cnt_n;
        this.ref_n = ref_n;
        this.reg_d = reg_d;
    }

    public BoardFreeVO(int totalcount, int lv, int seq_n, int member_no_n, String member_id_v, String member_nick_v, String title_v, int read_cnt_n, int ref_n,
            String reg_d) {
        super();
        this.totalcount = totalcount;
        this.lv = lv;
        this.seq_n = seq_n;
        this.member_no_n = member_no_n;
        this.member_id_v = member_id_v;
        this.member_nick_v = member_nick_v;
        this.title_v = title_v;
        this.read_cnt_n = read_cnt_n;
        this.ref_n = ref_n;
        this.reg_d = reg_d;
    }

    public int getTotalcount() {
        return totalcount;
    }

    public void setTotalcount(int totalcount) {
        this.totalcount = totalcount;
    }

    public int getLv() {
        return lv;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public int getSeq_n() {
        return seq_n;
    }

    public void setSeq_n(int seq_n) {
        this.seq_n = seq_n;
    }

    public int getMember_no_n() {
        return member_no_n;
    }

    public void setMember_no_n(int member_no_n) {
        this.member_no_n = member_no_n;
    }

    public String getMember_id_v() {
        return member_id_v;
    }

    public void setMember_id_v(String member_id_v) {
        this.member_id_v = member_id_v;
    }

    public String getMember_nick_v() {
        return member_nick_v;
    }

    public void setMember_nick_v(String member_nick_v) {
        this.member_nick_v = member_nick_v;
    }

    public String getTitle_v() {
        return title_v;
    }

    public void setTitle_v(String title_v) {
        this.title_v = title_v;
    }

    public String getContent_v() {
        return content_v;
    }

    public void setContent_v(String content_v) {
        this.content_v = content_v;
    }

    public int getRead_cnt_n() {
        return read_cnt_n;
    }

    public void setRead_cnt_n(int read_cnt_n) {
        this.read_cnt_n = read_cnt_n;
    }

    public int getRef_n() {
        return ref_n;
    }

    public void setRef_n(int ref_n) {
        this.ref_n = ref_n;
    }

    public String getUse_yn_v() {
        return use_yn_v;
    }

    public void setUse_yn_v(String use_yn_v) {
        this.use_yn_v = use_yn_v;
    }

    public String getReg_d() {
        return reg_d;
    }

    public void setReg_d(String reg_d) {
        this.reg_d = reg_d;
    }

}
