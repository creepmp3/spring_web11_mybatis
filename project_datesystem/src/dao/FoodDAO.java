package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import db.MakeConnection;

public class FoodDAO {
	 Connection conn = null;
	    PreparedStatement pstmt = null;
	    StringBuffer sql = new StringBuffer();
	    ResultSet rs = null;
	    
	    String foodUploadPath = "/project_datesystem/upload/food/";
	    
	    public FoodDAO() {
	    	conn = MakeConnection.getInstance().getConnection();
	    }
	    
	    public int insertOne(FoodVO vo) {
	    	int result = 0;
	    	sql.setLength(0);
	    	sql.append("\n INSERT INTO t_food(food_no_n		");
	    	sql.append("\n 					, food_title_v				");
	    	sql.append("\n					, food_info_title_v 		");	
	    	sql.append("\n 					, food_content_v			");
	    	sql.append("\n 					, food_imgpath_1_v		");
	    	sql.append("\n 					, food_imgpath_2_v		");
	    	sql.append("\n 					, food_imgpath_3_v		");
	    	sql.append("\n 					, food_imgpath_4_v		");
	    	sql.append("\n 					, food_imgpath_5_v		");
	    	sql.append("\n 					, food_imgpath_6_v		");
	    	sql.append("\n 					, food_imgpath_7_v		");
	    	sql.append("\n 					, food_imgpath_8_v		");
	    	sql.append("\n 					, food_tel_v					");
	    	sql.append("\n 					, food_open_time_v		");
	    	sql.append("\n 					, food_close_time_v		");
	    	sql.append("\n 					, food_price_v				");
	    	sql.append("\n 					, food_holiday_v			");
	    	sql.append("\n 					, food_type_v				");
	    	sql.append("\n 					, food_loc_v					");
	    	sql.append("\n					, food_read_n				");
	    	sql.append("\n					, food_recommend_n	");
	    	sql.append("\n 					, food_use_yn_c			");
	    	sql.append("\n 					, food_reg_d				");
	    	sql.append("\n 					)VALUES(					");
	    	sql.append("\n						food_no_seq.nextval "); // food_no_n
	    	sql.append("\n						, ?							"); // food_title_v
	    	sql.append("\n						, ?							"); // food_info_title_v
	    	sql.append("\n						, ?							"); // food_content_v
	    	sql.append("\n						, ?							"); // food_imgpath_1_v
	    	sql.append("\n						, ?							"); // food_imgpath_2_v
	    	sql.append("\n						, ?							"); // food_imgpath_3_v
	    	sql.append("\n						, ?							"); // food_imgpath_4_v
	    	sql.append("\n						, ?							"); // food_imgpath_5_v
	    	sql.append("\n						, ?							"); // food_imgpath_6_v
	    	sql.append("\n						, ?							"); // food_imgpath_7_v
	    	sql.append("\n						, ?							"); // food_imgpath_8_v
	    	sql.append("\n						, ?							"); // food_tel_v
	    	sql.append("\n						, ?							"); // food_open_time_v
	    	sql.append("\n						, ?							"); // food_close_time_v
	    	sql.append("\n						, ?							"); // food_price_v
	    	sql.append("\n						, ?							"); // food_holiday_v
	    	sql.append("\n						, ?							"); // food_type_v
	    	sql.append("\n						, ?							"); // food_loc_v
	    	sql.append("\n                       , 0              		    "); // food_read_n
	        sql.append("\n                       , 0              		    "); // food_recommend_n
	    	sql.append("\n						, 'Y'						"); // food_use_yn_c
	    	sql.append("\n						, SYSDATE				"); // food_reg_d
	    	sql.append("\n						)							");
	    	
	    	 try {
	 	    	pstmt = conn.prepareStatement(sql.toString());
	 	    	int i = 1;
	 	    	pstmt.setString(i++, vo.getFood_title_v());
	 	    	pstmt.setString(i++, vo.getFood_info_title_v());
	 	    	pstmt.setString(i++, vo.getFood_content_v());
	 	    	pstmt.setString(i++, foodUploadPath+vo.getFood_imgpath_1_v());
	 	    	pstmt.setString(i++, foodUploadPath+vo.getFood_imgpath_2_v());
	 	    	pstmt.setString(i++, foodUploadPath+vo.getFood_imgpath_3_v());
	 	    	pstmt.setString(i++, foodUploadPath+vo.getFood_imgpath_4_v());
	 	    	pstmt.setString(i++, foodUploadPath+vo.getFood_imgpath_5_v());
	 	    	pstmt.setString(i++, foodUploadPath+vo.getFood_imgpath_6_v());
	 	    	pstmt.setString(i++, foodUploadPath+vo.getFood_imgpath_7_v());
	 	    	pstmt.setString(i++, foodUploadPath+vo.getFood_imgpath_8_v());
	 	    	pstmt.setString(i++, vo.getFood_tel_v());
	 	    	pstmt.setString(i++, vo.getFood_open_time_v());
	 	    	pstmt.setString(i++, vo.getFood_close_time_v());
	 	    	pstmt.setString(i++, vo.getFood_price_v());
	 	    	pstmt.setString(i++, vo.getFood_holiday_v());
	 	    	pstmt.setString(i++, vo.getFood_type_v());
	 	    	pstmt.setString(i++, vo.getFood_loc_v());
	 	    	//pstmt.setString(i++, vo.getFood_use_yn_c());
	 	    	
	 	    	result = pstmt.executeUpdate();
	 	    	
	 	    	if ( result > 0 ) {
	 	    		System.out.println("등록성공");
	 	    	} else {
	 	    		System.out.println("등록실패");
	 	    	}
	 	    } catch ( SQLException e ) {
	 	    	e.printStackTrace();
	 	    }
	    	 return result;
	    }
	    
	   public ArrayList<FoodVO> selectAll(int startNum, int endNum, String searchOption, String searchKeyword, String orderType) {
		   ArrayList<FoodVO> list = new ArrayList<FoodVO>();
		   sql.setLength(0);
	       sql.append("\n SELECT *                                                              ");
	       sql.append("\n    FROM                                                               ");
	       sql.append("\n        (SELECT ROW_NUMBER() OVER(ORDER BY food_reg_d DESC) rnum       ");
	       sql.append("\n              , COUNT(*) OVER()                             totalcount ");
	       sql.append("\n              , food_no_n                                              ");
	       sql.append("\n              , food_title_v                                           ");
	       sql.append("\n              , food_info_title_v                                      ");
	       sql.append("\n              , food_imgpath_1_v                                       ");
	       sql.append("\n              , food_type_v                                            ");
	       sql.append("\n              , food_tel_v                                             ");
	       sql.append("\n              , food_loc_v                                             ");
	       sql.append("\n              , food_read_n                                            ");
	       sql.append("\n              , food_recommend_n                                       ");
	       sql.append("\n              , food_reg_d                                             ");
	       sql.append("\n           FROM t_food                                                 ");
	       sql.append("\n          WHERE food_use_yn_c = 'Y'                                    ");
	       if (searchOption.equals("title")) {
	           sql.append("\n            AND food_title_v like '%" + searchKeyword + "%'        ");
	       }
	       if (searchOption.equals("loc")) {
	           sql.append("\n            AND food_loc_v like '%" + searchKeyword + "%'          ");
	       }
	       if (searchOption.equals("holiday")) {
	           sql.append("\n            AND food_holiday_v like '%" + searchKeyword + "%'      ");
	       }
	       if (searchOption.equals("tel")) {
	           sql.append("\n            AND food_tel_v like '%" + searchKeyword + "%'          ");
	       }
	       sql.append("\n        )                                                              ");
	       sql.append("\n WHERE rnum BETWEEN " + startNum + " AND " + endNum);
	       if(!orderType.equals("")){
		       	if(orderType.equals("date")){
		       		sql.append("\n ORDER BY food_reg_d DESC                                      ");
		       	}else if(orderType.equals("recommend")){
		       		sql.append("\n ORDER BY food_recommend_n DESC                                ");
		       	}
	       }
	       
	       try {
			pstmt = conn.prepareStatement(sql.toString());
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				int totalcount = rs.getInt("totalcount");
				int food_no_n = rs.getInt("food_no_n");
				String food_title_v = rs.getString("food_title_v");
				String food_info_title_v = rs.getString("food_info_title_v");
				String food_imgpath_1_v = rs.getString("food_imgpath_1_v");
				String food_type_v = rs.getString("food_type_v");
				String food_tel_v = rs.getString("food_tel_v");
				String food_loc_v = rs.getString("food_loc_v");
				int food_read_n = rs.getInt("food_read_n");
				int food_recommend_n = rs.getInt("food_recommend_n");
				String food_reg_d = rs.getString("food_reg_d");
				
				FoodVO vo = new FoodVO(totalcount, food_no_n, food_title_v, food_info_title_v,
                        food_imgpath_1_v, food_type_v, food_tel_v, food_loc_v, food_read_n, food_recommend_n, food_reg_d);
				list.add(vo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	       return list;
	   }
	    
	   
	   public FoodVO selectOne(int food_no_n) {
		   FoodVO vo = null;
		   sql.setLength(0);
	        sql.append("\n SELECT food_title_v      ");
	        sql.append("\n      , food_info_title_v		");
	        sql.append("\n      , food_content_v    ");
	        sql.append("\n      , food_imgpath_1_v  ");
	        sql.append("\n      , food_imgpath_2_v  ");
	        sql.append("\n      , food_imgpath_3_v  ");
	        sql.append("\n      , food_imgpath_4_v  ");
	        sql.append("\n      , food_imgpath_5_v  ");
	        sql.append("\n      , food_imgpath_6_v  ");
	        sql.append("\n      , food_imgpath_7_v  ");
	        sql.append("\n      , food_imgpath_8_v  ");
	        sql.append("\n      , food_tel_v        ");
	        sql.append("\n      , food_open_time_v  ");
	        sql.append("\n      , food_close_time_v ");
	        sql.append("\n      , food_price_v      ");
	        sql.append("\n      , food_holiday_v    ");
	        sql.append("\n      , food_type_v       ");
	        sql.append("\n      , food_loc_v        ");
	        sql.append("\n      , food_read_n        ");
	        sql.append("\n      , food_recommend_n");
	        sql.append("\n      , food_use_yn_c     ");
	        sql.append("\n      , food_reg_d        ");
	        sql.append("\n   FROM t_food            ");
	        sql.append("\n  WHERE food_no_n = ?     ");
	        
	        try {
				pstmt = conn.prepareStatement(sql.toString());
				pstmt.setInt(1, food_no_n);
				rs = pstmt.executeQuery();
				
				if ( rs.next() ) {
					 String food_title_v = rs.getString("food_title_v");
					 String food_info_title_v = rs.getString("food_info_title_v");
		             String food_content_v = rs.getString("food_content_v");
		             String food_imgpath_1_v = rs.getString("food_imgpath_1_v");
		             String food_imgpath_2_v = rs.getString("food_imgpath_2_v");
		             String food_imgpath_3_v = rs.getString("food_imgpath_3_v");
	                 String food_imgpath_4_v = rs.getString("food_imgpath_4_v");
	                 String food_imgpath_5_v = rs.getString("food_imgpath_5_v");
	                 String food_imgpath_6_v = rs.getString("food_imgpath_6_v");
	                 String food_imgpath_7_v = rs.getString("food_imgpath_7_v");
	                 String food_imgpath_8_v = rs.getString("food_imgpath_8_v");
	                 String food_tel_v = rs.getString("food_tel_v");
	                 String food_open_time_v = rs.getString("food_open_time_v");
	                 String food_close_time_v = rs.getString("food_close_time_v");
	                 String food_price_v = rs.getString("food_price_v");
	                 String food_holiday_v = rs.getString("food_holiday_v");
	                 String food_type_v = rs.getString("food_type_v");
	                 String food_loc_v = rs.getString("food_loc_v");
	                 int food_read_n = rs.getInt("food_read_n");
	                 int food_recommend_n = rs.getInt("food_recommend_n");
	                 String food_use_yn_c = rs.getString("food_use_yn_c");
	                 String food_reg_d = rs.getString("food_reg_d");
	                 
	                 vo = new FoodVO(food_no_n, food_title_v, food_info_title_v, food_content_v,
	                         food_tel_v,  food_open_time_v,  food_close_time_v,  food_price_v,
	                          food_holiday_v,  food_type_v,  food_loc_v,  food_read_n,  food_recommend_n, food_use_yn_c,
	                          food_reg_d,  food_imgpath_1_v,  food_imgpath_2_v,  food_imgpath_3_v,
	                          food_imgpath_4_v,  food_imgpath_5_v,  food_imgpath_6_v,
	                          food_imgpath_7_v,  food_imgpath_8_v);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
	        return vo;
	   }
	   
	   
	   public int updateOne(FoodVO vo) {
		   int result = 0;
		   sql.setLength(0);
		   sql.append("\n UPDATE t_food SET            ");
	       sql.append("\n        food_title_v = ?      ");
	       sql.append("\n      , food_info_title_v = ? ");
	       sql.append("\n      , food_content_v = ?    ");
	       sql.append("\n      , food_tel_v = ?        ");
	       sql.append("\n      , food_open_time_v = ?  ");
	       sql.append("\n      , food_close_time_v = ? ");
	       sql.append("\n      , food_price_v = ?      ");
	       sql.append("\n      , food_holiday_v = ?    ");
	       sql.append("\n      , food_type_v = ?       ");
	       sql.append("\n      , food_loc_v = ?        ");
	       
	       if (vo.getFood_imgpath_1_v() != null) {
	            sql.append("\n      , food_imgpath_1_v = ?  ");
	       }
	       if (vo.getFood_imgpath_2_v() != null) {
	           sql.append("\n      , food_imgpath_2_v = ?  ");
	       }
	       if (vo.getFood_imgpath_3_v() != null) {
	           sql.append("\n      , food_imgpath_3_v = ?  ");
	       }
	       if (vo.getFood_imgpath_4_v() != null) {
	           sql.append("\n      , food_imgpath_4_v = ?  ");
	       }
	       if (vo.getFood_imgpath_5_v() != null) {
	           sql.append("\n      , food_imgpath_5_v = ?  ");
	       }
	       if (vo.getFood_imgpath_6_v() != null) {
	           sql.append("\n      , food_imgpath_6_v = ?  ");
	       }
	       if (vo.getFood_imgpath_7_v() != null) {
	           sql.append("\n      , food_imgpath_7_v = ?  ");
	       }
	       if (vo.getFood_imgpath_8_v() != null) {
	           sql.append("\n      , food_imgpath_8_v = ?  ");
	       }
	       sql.append("\n WHERE food_no_n = ?         ");
	       
	       try {
	    	   pstmt = conn.prepareStatement(sql.toString());
				
			   int i = 1;
			   pstmt.setString(i++, vo.getFood_title_v());
		       pstmt.setString(i++, vo.getFood_info_title_v());
		       pstmt.setString(i++, vo.getFood_content_v());
		       pstmt.setString(i++, vo.getFood_tel_v());
		       pstmt.setString(i++, vo.getFood_open_time_v());
		       pstmt.setString(i++, vo.getFood_close_time_v());
		       pstmt.setString(i++, vo.getFood_price_v());
		       pstmt.setString(i++, vo.getFood_holiday_v());
		       pstmt.setString(i++, vo.getFood_type_v());
		       pstmt.setString(i++, vo.getFood_loc_v());
		        
		       if (vo.getFood_imgpath_1_v() != null) {
		            pstmt.setString(i++, foodUploadPath + vo.getFood_imgpath_1_v());
		       }
		       if (vo.getFood_imgpath_2_v() != null) {
		            pstmt.setString(i++, foodUploadPath + vo.getFood_imgpath_2_v());
		       }
		       if (vo.getFood_imgpath_3_v() != null) {
		            pstmt.setString(i++, foodUploadPath + vo.getFood_imgpath_3_v());
		       }
		       if (vo.getFood_imgpath_4_v() != null) {
		            pstmt.setString(i++, foodUploadPath + vo.getFood_imgpath_4_v());
		       }
		       if (vo.getFood_imgpath_5_v() != null) {
		            pstmt.setString(i++, foodUploadPath + vo.getFood_imgpath_5_v());
		       }
		       if (vo.getFood_imgpath_6_v() != null) {
		            pstmt.setString(i++, foodUploadPath + vo.getFood_imgpath_6_v());
		       }
		       if (vo.getFood_imgpath_7_v() != null) {
		            pstmt.setString(i++, foodUploadPath + vo.getFood_imgpath_7_v());
		       }
		       if (vo.getFood_imgpath_8_v() != null) {
		            pstmt.setString(i++, foodUploadPath + vo.getFood_imgpath_8_v());
		       }
		       pstmt.setInt(i++, vo.getFood_no_n());
		       
		       result = pstmt.executeUpdate();
		        
			} catch (SQLException e) {
				e.printStackTrace();
			}
	       return result;
	   }
	   
	   
	   
	   
	   
	   public int deleteOne(int food_no_n) {
		   int result = 0;
		   sql.setLength(0);
		   sql.append("\n UPDATE t_food SET 		");
		   sql.append("\n food_use_yn_c = 'N'   		");
		   sql.append("\n WHERE food_no_n = ?    ");
		   
		   try {
			pstmt = conn.prepareStatement(sql.toString());
			
			pstmt.setInt(1, food_no_n);
			
			result = pstmt.executeUpdate();
			
			if ( result > 0 ) {
				System.out.println("삭제성공");
			} else {
				System.out.println("삭제실패");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		   return result;
	   }
	   
}
