<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.ZipcodeDAO, dao.ZipcodeVO, java.util.ArrayList, util.StringUtil" %>
<%
	/* ******************** paging ******************** */
	int totalcount = 0;
	int pageCount = 0;
	int pageNo = Integer.parseInt(StringUtil.nvl(request.getParameter("pageNo"), "1"));
	int pageSize = 10;
	int startNum = ((pageNo - 1) * pageSize) + 1;
	int endNum = ((pageNo - 1) * pageSize) + pageSize;
	
	int searchStart = 0;
	int searchEnd = 0;
	searchStart = (pageNo-5 <= 0)?1:pageNo-5;
	searchEnd = (pageNo-5 <=0)?10:pageNo+5;
	
	String searchKeyword = StringUtil.nvl(request.getParameter("searchKeyword"), "");
	/* ******************** paging ******************** */
	
	String URI = request.getRequestURI();
	String PAGENAME = URI.substring(URI.lastIndexOf("/")+1, URI.length());
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title> 오후 8:14:13</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript">
$(function(){
	$("a[name='addr']").on("click", function(){
		var zipcode = $(this).parent().prev().text();
		var zipcode1 = zipcode.substr(0,3);
		var zipcode2 = zipcode.substr(4, 7);
		var gu = $(this).parent().prev().prev().prev().prev().text();
		var addr1 = $(this).text().substr(0, $(this).text().indexOf("구")+1);
		var addr2 = $(this).text().substr($(this).text().indexOf("구")+2, $(this).text().length);
		opener.document.getElementById("member_addr1_v").value=addr1;
		opener.document.getElementById("member_addr2_v").value=addr2;
		opener.document.getElementById("zipcode1_v").value=zipcode1;
		opener.document.getElementById("zipcode2_v").value=zipcode2;
		window.close();
	});
});
</script>
<style type="text/css">
a {text-decoration:none;}
body {
	
}
#wrap {
	width:100%;
}
#searchWrap {
	margin-bottom:5px;
}
.tableList {
	margin:5px 2px 5px 1px;
	width:100%;
}
.tableList, .tableList th, .tableList td {
	color:#505739;
	border:1px solid #A9A9A9;
	border-collapse:collapse;
	text-align:center;
}
.tableList td a {
	color:#505739;
}
.tableList th {
	background:linear-gradient(to bottom, #eae0c2 5%, #ccc2a6 150%);
	background-color:#eae0c2;
}
.tableList td:nth-child(6) {
	padding-left:5px;
	text-align:left;
}
#btnArea {
	clear:both;
	float:right;
}
#paging {
	width:550px;
	height:30px;
	border:0px solid black;
	margin-top:-5px;
	float:right;
}

.pagingTalbe a, .pagingTalbe p {
	float:left;
	width:20px;
	margin-right:2px;
}
p.pagingBtn {
	font-weight:bold;
}

.pagingBtn {
	-moz-box-shadow: 0px 0px 2px 0px #1c1b18;
	-webkit-box-shadow: 0px 0px 2px 0px #1c1b18;
	box-shadow: 0px 0px 2px 0px #1c1b18;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #eae0c2), color-stop(1, #ccc2a6));
	background:linear-gradient(to bottom, #eae0c2 5%, #ccc2a6 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#eae0c2', endColorstr='#ccc2a6',GradientType=0);
	background-color:#eae0c2;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	border:0px solid #333029;
	display:inline-block;
	cursor:pointer;
	color:#505739;
	font-family:arial;
	font-size:14px;
	padding:5px 5px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
}
.pagingBtn:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ccc2a6), color-stop(1, #eae0c2));
	background:-moz-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:-webkit-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:-o-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:-ms-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:linear-gradient(to bottom, #ccc2a6 5%, #eae0c2 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ccc2a6', endColorstr='#eae0c2',GradientType=0);
	background-color:#ccc2a6;
}
.pagingBtn:active {
	position:relative;
	top:1px;
}

.pagingTalbe {
	text-align:center;
	width:100%;
	margin:-5px;
	height:30px;
}
.pagingTalbe, .pagingTalbe td {
	padding:0px;
	border:0px solid black;
	border-collapse:collapse;
}
#searchWrap {
	margin:0px;
	float:right;
}
</style>
</head>
<body>
	<div id="wrap">
		<div id="searchWrap">
			<form action="<%=PAGENAME%>">
				<input type="text" name="searchKeyword" id="" />
				<input type="submit" value="검색" />
			</form>
		</div>
		<form action="board_deleteOk.jsp" id="frm">
			<table class="tableList">
				<tr>
					<th>시</th>
					<th>구</th>
					<th>동</th>
					<th>번지</th>
					<th>우편번호</th>
					<th>주소</th>
				</tr>
			<%
				ZipcodeDAO dao = new ZipcodeDAO();
				ArrayList<ZipcodeVO> list = dao.selectAll(startNum, endNum, searchKeyword);
				
				for(ZipcodeVO vo : list){
				totalcount = vo.getTotalcount();
				if(totalcount%pageSize==0){
					pageCount = totalcount/pageSize;
				}else{
					pageCount = totalcount/pageSize+1;
				}
				String zipcode = vo.getZipcode_v();
				String zipcode1 = zipcode.substring(0,3);
				String zipcode2 = zipcode.substring(3,6);
			%>
				<tr>
					<td><%=vo.getSi_v() %></td>
					<td><%=vo.getGu_v() %></td>
					<td><%=vo.getDong_v() %></td>
					<td><%=StringUtil.nvl(vo.getBunji_v(), "") %></td>
					<td><%=zipcode1 %>-<%=zipcode2 %></td>
					<td><a href="#" name="addr" ><%=vo.getAddr_v() %></a></td>
				</tr>
			<%
				}
			%>
			</table>
		</form>
		<div id="paging">
			<table class="pagingTalbe">
				<%
				
				
				out.println("<tr>");
				for(int i=searchStart; i<=pageCount; i++){
					if(i<=0){
						continue;
					}else if(i>searchEnd || totalcount<10){
						break;
					}else if(i==pageNo){
						out.println("<td><p class='pagingBtn'>"+i+"</p></td>");
					}else{
						out.println("<td><a class='pagingBtn' href='"+PAGENAME+"?pageNo="+i+"&searchKeyword="+searchKeyword+" '>"+i+"</a></td>");
					}
				}
				out.println("<td><span>("+pageNo+"/"+pageCount+" 총" +totalcount+"개)</span></td>");
				out.println("</tr>");
				%>
				</table>
		</div>
	</div>
</body>
</html>