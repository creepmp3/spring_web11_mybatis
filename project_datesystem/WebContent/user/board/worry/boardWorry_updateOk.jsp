<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.BoardWorryDAO, dao.BoardWorryVO, util.StringUtil"%>
<%
	int seq_n = Integer.parseInt(StringUtil.nvl(request.getParameter("seq_n"), "0"));
	String title_v = StringUtil.nvl(request.getParameter("title_v"), "");
	String content_v = StringUtil.nvl(request.getParameter("content_v"), "");
	
	BoardWorryDAO dao = new BoardWorryDAO();
	BoardWorryVO vo = new BoardWorryVO(seq_n, title_v, content_v);
	int result = dao.updateOne(vo);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
var result = "<%=result%>";
var msg = "수정에 실패하였습니다";
if(result>0){
	msg = "수정되었습니다";
}
alert(msg);
location.href = "./boardWorry_list.jsp";
</script>
<title></title>
</head>
<body>

</body>
</html>