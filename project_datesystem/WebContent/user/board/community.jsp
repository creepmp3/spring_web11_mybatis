<%@page import="dao.BoardFreeVO"%>
<%@page import="dao.BoardFreeDAO"%>
<%@page import="dao.BoardWorryVO"%>
<%@page import="dao.BoardWorryDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.BoardManDAO, dao.BoardManVO"%>
<%@ page import="dao.BoardWoManDAO, dao.BoardWoManVO"%>
<%
	BoardManDAO mdao = new BoardManDAO();
	ArrayList<BoardManVO> boardM_list = mdao.selectAll(1, 5, "", "", "");
	
	BoardWoManDAO wdao = new BoardWoManDAO();
	ArrayList<BoardWoManVO> boardW_list = wdao.selectAll(1, 5, "", "", "");
	
	BoardWorryDAO worrydao = new BoardWorryDAO();
	ArrayList<BoardWorryVO> boardWorry_list = worrydao.selectAll(1, 5, "", "", "");
	
	BoardFreeDAO fdao = new BoardFreeDAO();
	ArrayList<BoardFreeVO> boardFree_list = fdao.selectAll(1, 5, "", "", "");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/community_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/main/weather.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="/project_datesystem/js/main/movie_js_style.js" /></script>
<title>Insert title here</title>
<script type="text/javascript">
$(function(){
	$("a[name='boardM']").each(function(i, e){
		$(e).on("click", function(){
			
			if("<%=session_member_no_n%>" > 0){
				if("<%=session_member_gender_v%>" == 'M' || "<%=session_member_type_v%>" == "S"){
					$("#seq_n").val($(e).attr("seq"));
					$("#moveForm").attr("action", "./m/boardM_detail.jsp").submit();
				}else{
					alert("남성전용 게시판입니다");
					return false;
				}
			}else{
				if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
					$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
					return false;
				}
			}
			return false;
		});
	});
	
	$("a[name='boardW']").each(function(i, e){
		$(e).on("click", function(){
			
			if("<%=session_member_no_n%>" > 0){
				if("<%=session_member_gender_v%>" == 'W' || "<%=session_member_type_v%>" == "S"){
					$("#seq_n").val($(e).attr("seq"));
					$("#moveForm").attr("action", "./w/boardW_detail.jsp").submit();
				}else{
					alert("여성전용 게시판입니다");
					return false;
				}
			}else{
				if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
					$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
					return false;
				}
			}
			return false;
		});
	});
	
	$("a[name='boardWorry']").each(function(i, e){
		$(e).on("click", function(){
			
			if("<%=session_member_no_n%>" > 0){
				$("#seq_n").val($(e).attr("seq"));
				$("#moveForm").attr("action", "./worry/boardWorry_detail.jsp").submit();
			}else{
				if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
					$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
					return false;
				}
			}
			return false;
		});
	});
	
	$("a[name='boardFree']").each(function(i, e){
		$(e).on("click", function(){
			
			if("<%=session_member_no_n%>" > 0){
				$("#seq_n").val($(e).attr("seq"));
				$("#moveForm").attr("action", "./free/boardFree_detail.jsp").submit();
			}else{
				if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
					$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
					return false;
				}
			}
			return false;
		});
	});
	
});

function moveBoardM(){
	if("<%=session_member_no_n%>" > 0){
		if("<%=session_member_gender_v%>" == 'M' || "<%=session_member_type_v%>" == "S"){
			location.href = "./m/boardM_list.jsp";
		}else{
			alert("남성전용 게시판입니다");
			return false;
		}
	}else{
		if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
			$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
			return false;
		}
	}
}

function moveBoardW(){
	if("<%=session_member_no_n%>" > 0){
		if("<%=session_member_gender_v%>" == 'W' || "<%=session_member_type_v%>" == "S"){
			location.href = "./w/boardW_list.jsp";
		}else{
			alert("여성전용 게시판입니다");
			return false;
		}
	}else{
		if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
			$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
			return false;
		}
	}
}

function moveBoardWorry(){
	if("<%=session_member_no_n%>" > 0){
		location.href = "./worry/boardWorry_list.jsp";
	}else{
		if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
			$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
			return false;
		}
	}
}

function moveBoardFree(){
	if("<%=session_member_no_n%>" > 0){
		location.href = "./free/boardFree_list.jsp";
	}else{
		if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
			$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
			return false;
		}
	}
}
</script>
<style type="text/css">
.regd {
	position: absolute;
	right: 5px;
}



.li_st a, .li_st a:ACTIVE, .li_st a:HOVER , .li_st a:VISITED {	
	color:#4186A1;
	text-shadow: 1px 1px 3px #4184B0;
}
</style>
</head>
<body>
	<form id="moveForm" action="#">
		<input type="hidden" id="seq_n" name="seq_n" />
		<input type="hidden" id="retUrl" name="retUrl" value="<%=PAGENAME %>" />
	</form>
		<div id = "main">
			<jsp:include page="/contents/content_menu.jsp"></jsp:include>
			<div id = "sub_menu">
				<div id = "profile">
					<%=session_member_nick_v %>님 <br/>
					상태 상태 <br/>
					뭐야 뭐야 <br/>
				</div>
				<ul>
					<li>사이드 메뉴1</li>
					<li>사이드 메뉴2</li>
					<li>사이드 메뉴3</li>
					<li>사이드 메뉴4</li>
					<li>사이드 메뉴5</li>
					<li>사이드 메뉴6</li>
					<li>사이드 메뉴7</li>
				</ul>
			</div>
			<div id = "sign_area">광고 영역</div>
			<div id = "board_content">
				<div id = "men_board">
					<div id = "board_title"><a href="#" onclick="return moveBoardM()">남성 게시판</a></div>

				<ul>
					<%
					for(BoardManVO mvo : boardM_list){
					    String reStr = "";
					    String reSp = "";
						if(mvo.getRef_n()>0){
						    for(int i=1; i<mvo.getLv(); i++){
						        reSp+="　";
						        reStr = "└>";
						    }
						}
					%>
					<li class="li_st">
						<a href="#" name="boardM" seq="<%=mvo.getSeq_n()%>"><%=reSp%><%=reStr%><%=mvo.getTitle_v() %></a> 
						<span class="regd"><%=mvo.getReg_d() %></span>
					</li>
					<%
					}
					%>
				</ul>

			</div>
				<div id = "girl_board">
				<div id = "board_title"><a href="#" onclick="return moveBoardW()">여성 게시판</a></div>
					<ul>
					<%
					for(BoardWoManVO wvo : boardW_list){
					    String reStr = "";
					    String reSp = "";
						if(wvo.getRef_n()>0){
						    for(int i=1; i<wvo.getLv(); i++){
						        reSp+="　";
						        reStr = "└>";
						    }
						}
					%>
						<li class="li_st">
						<a href="#" name="boardW" seq="<%=wvo.getSeq_n()%>"><%=reSp%><%=reStr%><%=wvo.getTitle_v() %></a> 
						<span class="regd"><%=wvo.getReg_d() %></span>
					<%
					}
					%>
					</li>
					</ul>
				</div>
				<div id = "worry_board">
				<div id = "board_title"><a href="#" onclick="return moveBoardWorry()">고민 게시판</a></div>
					<ul>
					<%
					for(BoardWorryVO worryvo : boardWorry_list){
					    String reStr = "";
					    String reSp = "";
						if(worryvo.getRef_n()>0){
						    for(int i=1; i<worryvo.getLv(); i++){
						        reSp+="　";
						        reStr = "└>";
						    }
						}
					%>
						<li class="li_st">
						<a href="#" name="boardWorry" seq="<%=worryvo.getSeq_n()%>"><%=reSp%><%=reStr%><%=worryvo.getTitle_v() %></a> 
						<span class="regd"><%=worryvo.getReg_d() %></span>
					<%
					}
					%>
					</li>
					</ul>
				</div>
				<div id = "free_board">
				<div id = "board_title"><a href="#" onclick="return moveBoardFree()">자유 게시판</a></div>
					<ul>
					<%
					for(BoardFreeVO fvo : boardFree_list){
					    String reStr = "";
					    String reSp = "";
						if(fvo.getRef_n()>0){
						    for(int i=1; i<fvo.getLv(); i++){
						        reSp+="　";
						        reStr = "└>";
						    }
						}
					%>
						<li class="li_st">
						<a href="#" name="boardWorry" seq="<%=fvo.getSeq_n()%>"><%=reSp%><%=reStr%><%=fvo.getTitle_v() %></a> 
						<span class="regd"><%=fvo.getReg_d() %></span>
					<%
					}
					%>
					</li>
					</ul>
				</div>
			</div>
		</div>
</body>
</html>