<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.json.simple.*, db.MakeConnection, java.sql.Connection, java.sql.PreparedStatement, java.sql.ResultSet, util.StringUtil"%>
<%
	request.setCharacterEncoding("UTF-8");

	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;
	
	StringBuffer sql = null;

	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
	int recommend = Integer.parseInt(StringUtil.nvl(request.getParameter("recommend"), "0"));
	String member_id_v = StringUtil.nvl((String)session.getAttribute("member_id_v"), "");
	
	int result = 0;
	int recommendn = 0;
	
	JSONObject obj = null;
	
	try{
		conn = MakeConnection.getInstance().getConnection();
		
		obj = new JSONObject();
		
		sql = new StringBuffer();
		sql.append("\n SELECT COUNT(*) FROM T_STAY_MEMBER WHERE STAY_NO_N = ? AND MEMBER_ID_V = ? ");
		pstmt = conn.prepareStatement(sql.toString());
		int idx = 1;
		pstmt.setInt(idx++, stay_no_n);
		pstmt.setString(idx++, member_id_v);
		rs = pstmt.executeQuery();
		int count = 0;
		if(rs.next()){
			count = rs.getInt(1);
		}
		
		rs.close();
		pstmt.close();
		
		if(count > 0){
			obj.put("message", "duplicate");
		}else{
			sql.setLength(0);
			
			sql.append("\n INSERT INTO T_STAY_MEMBER(STAY_NO_N , MEMBER_ID_V) ");
			sql.append("\n                          VALUES(? , ?)             ");
			pstmt = conn.prepareStatement(sql.toString());
			
			idx = 1;
			pstmt.setInt(idx++, stay_no_n);
			pstmt.setString(idx++, member_id_v);
			result = pstmt.executeUpdate();
			
			if(result>0){
				sql.setLength(0);
				sql.append("\n UPDATE t_stay SET          ");
				sql.append("\n       stay_recommend_n = ? ");
				sql.append("\n  WHERE stay_no_n = ?       ");
				pstmt = conn.prepareStatement(sql.toString());
				idx = 1;
				pstmt.setInt(idx++, recommend+1);
				pstmt.setInt(idx++, stay_no_n);
				result = pstmt.executeUpdate();			
				obj.put("recommend", recommend+1);
				obj.put("message", "success");
			}
		}
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		if(pstmt != null){ try{ pstmt.close(); }catch(Exception e){} }
		//if(conn != null){ try{ conn.close(); }catch(Exception e){} }
	}
	out.print(obj);
%>
