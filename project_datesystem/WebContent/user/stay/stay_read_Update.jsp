<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="db.MakeConnection, java.sql.*, util.StringUtil"%>
<%
	request.setCharacterEncoding("UTF-8");

	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
	int result = 0;
	
	Connection conn = null;
	PreparedStatement pstmt = null;
	
	try{
		conn = MakeConnection.getInstance().getConnection();
		StringBuffer sql = new StringBuffer();
		sql.setLength(0);
	    sql.append("\n UPDATE T_STAY                        ");
	    sql.append("\n    SET STAY_READ_N = STAY_READ_N + 1 ");
	    sql.append("\n  WHERE STAY_NO_N = ?                 ");
	
		pstmt = conn.prepareStatement(sql.toString());
	
	    pstmt.setInt(1, stay_no_n);
	    result = pstmt.executeUpdate();
	}catch(SQLException e){
		e.printStackTrace();
	}finally{
		if(pstmt != null){ try{ pstmt.close(); }catch(Exception e){} }
		//if(conn != null){ try{ conn.close(); }catch(Exception e){} }
	}

%>
{"result":"<%=result%>"}