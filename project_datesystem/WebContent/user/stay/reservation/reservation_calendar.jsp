<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="../../../common/sub_header.jsp" %>
<%@ page import="java.util.Calendar, dao.ReservationDAO, dao.ReservationVO, java.util.Date, java.text.SimpleDateFormat" %>
<%
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
	String userDate = StringUtil.nvl(request.getParameter("userDate"), "");
	
	Date date = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	String today = sdf.format(date);
	
	Calendar cal = Calendar.getInstance();
	int year = cal.get(Calendar.YEAR);
	int month = cal.get(Calendar.MONTH)+1;
	String startDate = "";
	String lastDate = "";
	
	int userYear = 0;
	int userMonth = 0;
	if(!userDate.equals("")){
		userYear = Integer.parseInt(userDate.substring(0, 4));
		userMonth = Integer.parseInt(userDate.substring(5, 7));
		userMonth -= 1;
		cal.set(userYear, userMonth, 1);
		startDate = userYear+"-"+(userMonth<10?"0"+userMonth:userMonth)+"-"+"01";
		lastDate = cal.get(Calendar.YEAR)+"-"+(userMonth<10?"0"+userMonth:userMonth)+"-"+cal.getActualMaximum(Calendar.DATE);
		//lastDate = "2015-04-30";
	}else{
		
		startDate = year+"-"+(month<10?("0"+month):month)+"-01";
		lastDate = cal.get(Calendar.YEAR)+"-"+(month<10?("0"+month):month)+"-"+cal.getMaximum(Calendar.DAY_OF_MONTH);
	}
	ReservationDAO dao = new ReservationDAO();
	
	ArrayList<ReservationVO> list = dao.selectAll(0, 0, startDate, lastDate, "calendar", "");
	int size = list.size();
	System.out.println(PAGENAME);

%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="../../../css/main/main_layout.css" />
<link rel="stylesheet" href="../../../css/main/top_menu.css" />
<link rel="stylesheet" href="../../../css/main/top_menu.css" />
<link rel="stylesheet" href="../../../css/main/tour_style.css" />

<link rel='stylesheet' href='../../../js/fullcalendar-2.3.1/lib/cupertino/jquery-ui.min.css' />
<link href='../../../js/fullcalendar-2.3.1/fullcalendar.css' rel='stylesheet' />
<link href='../../../js/fullcalendar-2.3.1/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='../../../js/fullcalendar-2.3.1/lib/moment.min.js'></script><!-- 
<script src='../../../js/fullcalendar-2.3.1/lib/jquery.min.js'></script> -->
<script src='../../../js/fullcalendar-2.3.1/fullcalendar.js'></script>
<script src='../../../js/fullcalendar-2.3.1/lang-all.js'></script>




<link rel='stylesheet' href='/project_datesystem/js/fullcalendar-2.3.1/lib/cupertino/jquery-ui.min.css' />
<link href='/project_datesystem/js/fullcalendar-2.3.1/fullcalendar.css' rel='stylesheet' />
<link href='/project_datesystem/js/fullcalendar-2.3.1/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='/project_datesystem/js/fullcalendar-2.3.1/lib/moment.min.js'></script>
<script src='/project_datesystem/js/fullcalendar-2.3.1/fullcalendar.js'></script>
<script src='/project_datesystem/js/fullcalendar-2.3.1/lang-all.js'></script>
<script type="text/javascript">
$(function(){
	
	/* ********************** Calendar ********************** */
	var currentLangCode = 'ko';
	
	function renderCalendar() {
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: "<%=today%>",
			lang: currentLangCode,
			buttonIcons: false, // show the prev/next text
			weekNumbers: true,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				<%
			    int i = 0;
				for(ReservationVO vo : list){
				    //out.println("vo : "+vo.getReservation_date_v());
				    //out.println("vot : "+vo.getReservation_time_v());
				    //out.println("t : "+today);
				    
				%> 
					
						{
							title: "<%=vo.getStay_price_nm_v()%> <%=StringUtil.nvl(vo.getPay_yn_v(), "N").equals("N")?"미결제":"결제완료"%>",
							start: "<%=vo.getReservation_date_v()%>T<%=vo.getReservation_time_v()%>",
							detail: "<%=vo.getUser_tel_v()%>"
							//end: '2015-04-12T12:30:00'
						}
						
		 	<%  
 					i++;
				 	if(i!=vo.getTotalcount()){
						out.println(",");
					}
		 		}
			 	
				
		 	%> 
			]
		});
	}

	renderCalendar();
	
	$(".detail").hide();
	
	$(".fc-time").each(function(i, e){
		$(e).on("click", function(){
			$(this).next().next().toggle();
		});
	});

	
	$(".fc-title").each(function(i, e){
		$(e).on("click", function(){
			$(this).next().toggle();
		});
	});
	
	$(".resBtn").each(function(i, e){
		$(e).on("click", function(){
			var resDate = $(e).parent().attr("data-date");
			$("#resDate").val(resDate);
			$("#moveForm").attr("action", "reservation_insert.jsp").submit();
		});
	});
	
	
	/* ********************** Calendar ********************** */
	
	
	$("#writeBtn").on("click", function(){
		location.href = "stay_insert.jsp";
	});

	$("a[name='titlev']").each(function(i, elem){
		$(this).on("click", function(){
			var no = $(this).parent().prev().text();
			$("#stay_no_n").val(no);
			$("#moveForm").attr("action", "stay_view.jsp").submit();
		});
	});
	
	$("#reservationBtn").on("click", function(){
		location.href = "./reservation_insert.jsp";
	});
	
});
</script>
<style type="text/css">
.reservation_cont{
	clear:both;
}
</style>
</head>
<body>
	<div id="content">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		<!-- <div id="checklist">
			<div id="checktext">
				<br />지역<br />세부사항
			</div>
			<div id="check_box">
				<ul>
					컨텐츠 추가 및 삭제는 추후에 진행함 지역구를 어떻게 나눠야 할지 모르겠음
					<li><input type="checkbox" name="" id="chb">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
				</ul>
			</div>
			<div id="checktext">
				<br />테마<br />세부사항
			</div>
			<div id="check_box">
				<ul>
					테마 나누는게 힘듬... 한식 일식 양식 중식 인도 이탈리안 카페 이런식은 어떤지??
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
				</ul>
			</div>
		</div> -->

		<div id="list_search">
			
		</div>

		<div id="search_box">
			<div id="line">
				<h2>숙박 예약현황</h2>
			</div>
		</div>
		<div class="reservation_cont">
			
			<form id="moveForm">
				<input type="hidden" name="stay_no_n" id="stay_no_n" value="<%=stay_no_n %>" />
				<input type="hidden" name="resDate" id="resDate" />
			</form>
			<div id='calendar'></div>
						
			
		</div>
	</div>
</body>
</html>