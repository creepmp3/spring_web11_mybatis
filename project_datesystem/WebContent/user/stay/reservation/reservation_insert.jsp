<%@page import="com.sun.xml.internal.fastinfoset.Encoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.StayDAO, dao.StayVO, dao.StayPriceDAO, dao.StayPriceVO, util.StringUtil" %>
<%@ include file="/common/sub_header.jsp" %>
<%
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "7"));
	String resDate = StringUtil.nvl(request.getParameter("resDate"), "");
	String member_cell_v = StringUtil.nvl((String)session.getAttribute("member_cell_v"), "");

	StayDAO dao = new StayDAO();
	StayVO vo = dao.selectOne(stay_no_n);
	
	StayPriceDAO p_dao = new StayPriceDAO();
	ArrayList<StayPriceVO> paylist = p_dao.selectAll(stay_no_n);
	
	
    String type = "";
    if(vo.getStay_type_v().equals("H")){
        type="호텔";
    }else if(vo.getStay_type_v().equals("M")){
        type="모텔";
    }else if(vo.getStay_type_v().equals("G")){
        type="게스트하우스";
    }
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!--  jQuery 기본 js파일 -->
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=b997e5bb20d6753e8f9185256ba673b2&libraries=services"></script>
<script type="text/javascript" src="/project_datesystem/js/daum_map.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	var resDate = "<%=resDate%>";
	if(resDate != ""){
		document.getElementById('reservation_date_v').valueAsDate = new Date(resDate);
	}else{
		document.getElementById('reservation_date_v').valueAsDate = new Date();
	}
	
	$(".resBtn").on("click", function(){
		validate();
		return false;
	});
});

function validate(){
	if($(":radio[name='stay_price_no_n']:checked").length < 1){
		alert("구분을 지정해주세요");
		$(":radio[name='stay_price_no_n']:eq(0)").focus();
		return false;
	}
	if($(":radio[name='reservation_type_v']:checked").length < 1){
		alert("타입을 지정해주세요");
		$(":radio[name='reservation_type_v']:eq(0)").focus();
		return false;
	}
	if($("#user_tel_v").val()==""){
		alert("전화번호를 입력해주세요");
		$("#user_tel_v").focus();
		return false;
	}
	
	if(confirm("예약하시겠습니까?")){
		var stay_no_n = $("#stay_no_n").val();
		var stay_price_no_n = $("#stay_price_no_n").val();
		var date = $("#reservation_date_v").val();
		var time = $("#reservation_time_v").val();
		
		$.ajax({
			type:"GET",
			url:"/project_datesystem/admin/stay/reservation/reservation_date_dupl.jsp",
			dataType:"json",
			data:{"stay_no_n":stay_no_n,
				  "stay_price_no_n":stay_price_no_n,
				  "date":date,
				  "time":time},
			success:function(data, status, request){
				
				if(parseInt(data.count) > 0){
					alert("예약이 중복되었습니다");
					return false;
				}else{
					if(confirm("예약이 가능합니다\n예약하시겠습니까?")){
						var price = $(":radio[name='reservation_type_v']:checked").prev().text();
						var price_name = $(":radio[name='stay_price_no_n']:checked").parent().prev().text();
						
						$("#stay_price_v").val(price);
						$("#stay_price_nm_v").val(price_name);
						
						$("#resForm").attr("action", "reservation_result.jsp").submit();
						return true;
					}
				}
			},
			error:function(response, status, request){
				alert("에러");
				return false;
			}
		});
	}
	return false;
}
</script>

<link rel="stylesheet" href="../../css/main/main_layout.css" />
<link rel="stylesheet" href="../../css/main/top_menu.css" />
<link rel="stylesheet" href="../../css/main/top_menu.css" />
<link rel="stylesheet" href="../../css/main/tour_style.css" />
<style type="text/css">

#search_box{
	margin-bottom: -10px;
}
div.imgBox{
	width:480px;
	height:320px;
}
dl.imgDl{
	list-style: none;
}

dl.imgDl dt img{
	width:455px;
	height:200px;
}
dl.imgDl dd{
	float:left;
	margin-right:5px;
}

dl.imgDl dd img{
	width:110px;
	height:65px;
}

div.infoBox {
	position:relative;
	left:500px;
	top: -320px;
}
.footer {
	clear:both;
	margin-top:100px !important;
	margin-bottom:100px !important;
}

#tab {
	margin-top:10px !important;
}
#tab li{
	font-size: 20px;
	background:#eee;
	margin-left: 0px !important;
	width: 70px;
	text-align: center;
	border:1px solid #ccc !important;
	border-top-right-radius:5px;
	border-top-left-radius:5px;
}

#tab li:HOVER, #tab li:FOCUS, #tab li.selected_tab {
	border-bottom:1px solid #fff !important;
	background:#fff; color:#f60;
}
#tab li a {
	display:block;
}
.contentView{
	margin-top:5px;
	clear:both;
	border:1px solid #ccc;
	padding:20px;
}
.resBtn {
	width: 63px;
	height: 30px;
	padding:5px 15px 5px 15px;
	text-shadow : 3px 3px 3px #FF70B2;
	border: 1px solid #FF70B2;
	color : white;
	font-size : 15px;
	font-weight : bold;
	background: linear-gradient(to top, #FF70B2, #FFA5BE);
	border-radius: 10px;
}
</style>
</head>
<body>
	<div id="content">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		

		<div id="list_search">
			
		</div>

		<div id="search_box">
			<div id="line">
				<h2 id="title"><%=vo.getStay_title_v()%></h2>
			</div>
			<ul id="tab">
				
			</ul>
		</div>
		<div class="contentView">
			
			<div id="payView">
				<div class="list">
					<form id="resForm">
						<input type="hidden" name="stay_no_n" id="stay_no_n" value="<%=stay_no_n%>"/>
						<input type="hidden" name="stay_price_v" id="stay_price_v" />
						<input type="hidden" name="stay_price_nm_v" id="stay_price_nm_v" />
						<input type="hidden" name="confirm_yn_v" id="confirm_yn_v" value="N"/>
						<table>
							<thead>
								<tr>
									<th colspan="2" rowspan="2">구분</th>
									<th colspan="2">대실</th>
									<th colspan="2">숙박</th>
								</tr>
								<tr>
									<td>월~금</td>
									<td>토~일</td>
									<td>월~금</td>
									<td>토~일</td>
								</tr>
							</thead>
							<tbody>
								<%
								for(StayPriceVO pvo : paylist){
								%>
								<tr>
									<th class="ar"><%=pvo.getStay_price_nm_v() %></th><td style="border-left:0px"><input type="radio" value="<%=pvo.getStay_price_no_n() %>" name="stay_price_no_n" /></td>
									<td><span><%=pvo.getStay_d_w_v() %></span> <input type="radio" value="dw" name="reservation_type_v" /></td>
									<td><span><%=pvo.getStay_d_h_v() %></span> <input type="radio" value="dh" name="reservation_type_v" /></td>
									<td><span><%=pvo.getStay_s_w_v() %></span> <input type="radio" value="sw" name="reservation_type_v" /></td>
									<td><span><%=pvo.getStay_s_h_v() %></span> <input type="radio" value="sh" name="reservation_type_v" /></td>
								</tr>
								<%
								}
								%>
							</tbody>
						</table>
						
						<div class="view" style="margin-top:30px">
							<table>
								<tr>
									<th>예약날짜</th><td><input type="date" name="reservation_date_v" id="reservation_date_v" /></td>
								</tr>
								<tr>
									<th>예약시간</th><td><input type="time" name="reservation_time_v" id="reservation_time_v" value="22:00" step="1800"/></td>
								</tr>
								<tr>
									<th><label for="people">인원추가</label></th>
									<td>
										<select name="people" id="people">
										<%
										for(int i=0; i<=4; i++){
										    out.println("<option value='"+(i*10000)+"' />"+i+"명");
										}
										%>
										</select>
										<span>(1명당 10,000원 추가)</span>
									</td>
								</tr>
								<tr>
									<th><label for="reservation_day_v">숙박기간</label></th>
									<td>
										<select name="reservation_range_v" id="reservation_range_v">
										<%
										for(int i=1; i<=7; i++){
										    out.println("<option value='"+i+"' />"+i+"일");
										}
										%>
										</select>
									</td>
								</tr>
								<tr>
									<th>전화번호</th><td><input type="text" name="user_tel_v" id="user_tel_v" value="<%=member_cell_v%>"/></td>
								</tr>
							</table>
						</div>
					</div>
					<div style="margin-top:10px;clear:both;float:right">
						<a href="#" class="resBtn">예약</a>
					</div>
				</form>
			</div>
			
			
			
			<div class="footer"></div>
			
			<div class="footer"></div>
		</div>
	</div>
</body>
</html>