<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.StayDAO, dao.StayVO, util.StringUtil" %>
<%@ include file="../../common/sub_header.jsp" %>
<%
	/* ******************** paging ******************** */
	int totalcount = 0;
	int pageCount = 0;
	int pageNo = Integer.parseInt(StringUtil.nvl(request.getParameter("pageNo"), "1"));
	int pageSize = 10;
	int startNum = ((pageNo - 1) * pageSize) + 1;
	int endNum = ((pageNo - 1) * pageSize) + pageSize;
	
	int searchStart = 0;
	int searchEnd = 0;
	searchStart = (pageNo-5 <= 0)?1:pageNo-5;
	searchEnd = (pageNo-5 <=0)?10:pageNo+5;

	String searchOption = StringUtil.nvl(request.getParameter("searchOption"), "");
	String searchKeyword = StringUtil.nvl(request.getParameter("searchKeyword"), "");
	/* ******************** paging ******************** */
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="../../css/main/main_layout.css" />
<link rel="stylesheet" href="../../css/main/top_menu.css" />
<link rel="stylesheet" href="../../css/main/top_menu.css" />
<link rel="stylesheet" href="../../css/main/tour_style.css" />

<link rel='stylesheet' href='../../js/fullcalendar-2.3.1/lib/cupertino/jquery-ui.min.css' />
<link href='../../js/fullcalendar-2.3.1/fullcalendar.css' rel='stylesheet' />
<link href='../../js/fullcalendar-2.3.1/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='../../js/fullcalendar-2.3.1/lib/moment.min.js'></script><!-- 
<script src='../../js/fullcalendar-2.3.1/lib/jquery.min.js'></script> -->
<script src='../../js/fullcalendar-2.3.1/fullcalendar.js'></script>
<script src='../../js/fullcalendar-2.3.1/lang-all.js'></script>

<script type="text/javascript">
$(document).ready(function() {
	var currentLangCode = 'ko';

	function renderCalendar() {
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '2015-04-29',
			businessHours: true, // display business hours
			lang: currentLangCode,
			buttonIcons: false, // show the prev/next text
			weekNumbers: true,
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				{
					title: 'All Day Event',
					start: '2015-04-01'
				},
				{
					start: '2015-04-02',
					overlap: true,
					rendering: 'background',
					color: '#ff9f89'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2015-04-09T16:00:00'
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2015-04-16T16:00:00',
					color: '#257e4a'

				},
				{
					title: 'Conference',
					start: '2015-04-11',
					end: '2015-04-13'
				},
				{
					title: 'Meeting',
					start: '2015-04-12T10:30:00',
					end: '2015-04-12T12:30:00'
				},
				{
					title: 'Lunch',
					start: '2015-04-12T12:00:00'
				},
				{
					title: 'Meeting',
					start: '2015-04-12T14:30:00'
				},
				{
					title: 'Happy Hour',
					start: '2015-04-12T17:30:00'
				},
				{
					title: 'Dinner',
					start: '2015-04-12T20:00:00'
				},
				{
					title: 'Birthday Party',
					start: '2015-04-13T07:00:00'
				},
				{
					title: 'Long Event',
					start: '2015-04-20',
					end: '2015-04-24'

				},
				{
					id: 999,
					title: 'Repeating Event',
					start: '2015-04-22T16:00'
				},
				{
					title: 'Click for Google',
					url: 'http://google.com/',
					start: '2015-04-28'

				}
			]
		});
	}

	renderCalendar();
});

</script>
<style type="text/css">
.reservation_cont{
	clear:both;
}
</style>
</head>
<body>
	<div id="content">
		<jsp:include page="../../main/menubar.jsp"></jsp:include>
		<div id="checklist">
			<div id="checktext">
				<br />지역<br />세부사항
			</div>
			<div id="check_box">
				<ul>
					<!-- 컨텐츠 추가 및 삭제는 추후에 진행함 지역구를 어떻게 나눠야 할지 모르겠음 -->
					<li><input type="checkbox" name="" id="chb">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
				</ul>
			</div>
			<div id="checktext">
				<br />테마<br />세부사항
			</div>
			<div id="check_box">
				<ul>
					<!--테마 나누는게 힘듬... 한식 일식 양식 중식 인도 이탈리안 카페 이런식은 어떤지?? -->
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
					<li><input type="checkbox" name="" id="">전체</li>
				</ul>
			</div>
		</div>

		<div id="list_search">
			
		</div>

		<div id="search_box">
			<div id="line">
				<h2>맛있게 먹자!!</h2>
			</div>
		</div>
		<div class="reservation_cont">
			
		
			<div id='calendar'></div>
						
			
		</div>
	</div>
</body>
</html>