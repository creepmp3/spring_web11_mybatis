function ShowSuccessMess(mess) {
	var html = '<div class="alert alert-success" ><a class="close" data-dismiss="alert" href="#">×</a>'
			+ mess + '</div>';
	$("#alert_body").html(html);
}

function ShowInfoMess(mess) {
	var html = '<div class="container"><div class="row"><div class="col-md-12"><div class="panel-body">';
	html = html
			+ '<div class="alert alert-info" ><a class="close" data-dismiss="alert" href="#">×</a>'
			+ mess + '</div>';
	html = html + '</div></div></div></div>';
	$("#notes_block").html(html);
}

function ShowAlertMess(mess) {
	var html = '<div class="alert alert-error" ><a class="close" data-dismiss="alert" href="#">×</a>'
			+ mess + '</div>';
	$("#alert_body").html(html);
}

function errorHandler(e) {
	console.log(e);
	ShowAlertMess(e.status + ' ' + e.statusText);
}

function ParseJson(JSONtext) {
	try {
		JSONobject = JSON.parse(JSONtext);
	} catch (e) {
		ShowAlertMess('Error JSON');
		return;
	}

	if (JSONobject.cod != '200') {
		ShowAlertMess('Error ' + JSONobject.cod + ' (' + JSONobject.message
				+ ')');
		return;
	}
	var mes = JSONobject.cod;
	if (JSONobject.calctime)
		mes = mes + ' ' + JSONobject.calctime;
	if (JSONobject.message)
		mes = mes + ' ' + JSONobject.message;
	console.log(mes);
	return JSONobject;
}
function set_cookie(name, value, expires) {
	if (!expires) {
		expires = new Date();
	}
	document.cookie = name + "=" + escape(value) + "; expires="
			+ expires.toGMTString() + "; path=/";
}

function get_cookie(name) {
	var matches = document.cookie
			.match(new RegExp("(?:^|; )"
					+ name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1')
					+ "=([^;]*)"))
	return matches ? decodeURIComponent(matches[1]) : undefined
}
function set_lang(lang) {
	expires = new Date();
	expires.setTime(expires.getTime() + (1000 * 60 * 60 * 24));
	set_cookie('lang', lang, expires);
	window.location.reload();
}

function set_units() {
	var units = 'metric';
	if (document.getElementById("units_check").checked)
		units = 'imperial';
	expires = new Date();
	expires.setTime(expires.getTime() + (1000 * 60 * 60 * 24));
	set_cookie('units', units, expires);
	window.location.reload();
}

var time_zone = 1000 * (new Date().getTimezoneOffset()) * (-60);

var forecast = [];
var daily = [];

jQuery(document).ready(function() {

	var t = document.location.href.split('#');
	if (t[1])
		$("#" + t[1] + '_a').click();

	// Tab dispacher

	$('a[data-toggle="tab"]').on('shown', function(e) {

		if ($(this).attr('href') == "#forecast") {
		}

		if ($(this).attr('href') == "#forecast-chart") {
			showForcastChartVal('temp');
		}

		if ($(this).attr('href') == "#map") {
			// ShowMap();
		}

	})

});

// **************************** forecast *******************************

function showCurrentCity(d) {
	current_city_id = d.id;
	$('#city_name').html(d.name + ', ' + d.sys.country);
}

function showForecast(d) {
	forecast = d.list;
	showForecastSmall();
	showHourlyForecastChart();
	showForcastHourlyListLong();
	showForcastChartVal('temp');
}

function showForecastDaily(d) {
	daily = d.list;
	showDailyListVertical();
	showDailyChart();
}

var month = [ "1월", "2월", "3월", "4월", "5월", "6월", "7월", "8월", "9월", "10월",
		"11월", "12월" ];

function showDailyListVertical() {

	var html = ''
	var html_off;

	for (var i = 0; i < 7; i++) { // daily.length-1

		var dt = new Date(daily[i].dt * 1000 + time_zone);
		var day = month[dt.getMonth()] + " " + dt.getDate() + "일";

		var temp = Math.round(10 * (daily[i].temp.day)) / 10;
		var eve = Math.round(10 * (daily[i].temp.eve)) / 10;
		var morn = Math.round(10 * (daily[i].temp.morn)) / 10;
		var night = Math.round(10 * (daily[i].temp.night)) / 10;
		var icon = daily[i].weather[0].icon;
		var text = daily[i].weather[0].description;

		html += '<div id = "date">'+ day + "</div>"+' <img src="http://openweathermap.org/img/w/' + icon
				+ '.png" >';
/*		if (temp > 0)
			html += '<span class="label label-warning">';
		else
			html += '<span class="label label-primary">';*/
		html += "<div id = 'celsius'>"+ temp + '°C '+/*</span>&nbsp<span class="label label-default">+*/'/ ' + night
				+ '°C </div>';/* + text + '</i> <p> '
				+ daily[i].speed + 'm/s <br>구름 : ' + daily[i].clouds + '%, '
				+ daily[i].pressure + ' hpa</p></td></tr>';*/

	/*	html_off += '<tr><td>' + day
				+ '<img src="http://openweathermap.org/img/w/' + icon
				+ '.png" ></td></tr>';
		html_off += "<tr><td>낮:" + temp
				+ '°C </span>&nbsp<span class="label label-default">밤:' + night
				+ '°C </span></td></tr>';
*/
	}

	html = '<div id = "wearther_style">' + html + '</div>';

	$("#daily_list").html(html);

}
