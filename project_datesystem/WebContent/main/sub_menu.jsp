<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel = "stylesheet" href="../css/main/sub_menu_style.css"/>
</head>
<body>
	<div id="sub_text_sub">
		<div id="sub_text">추천 이슈!</div>
	</div>
	<div id="sub_content_2">
		<div id="content_sub_image">
			<img src="../images/sub_main/sub_main_1.jpg">
		</div>
		<div id="content_sub_image">
			<img src="../images/sub_main/sub_main_2.jpg">
		</div>
		<div id="content_sub_image">
			<img src="../images/sub_main/sub_main_3.jpg">
		</div>
		<div id="content_text_1">
			 <table>
			 	<tr>
			 		<td class = "t_text">추천 영화</td>
			 		<td class = "t_text">추천 맛집</td>
			 		<td class = "t_text">추천 숙박</td>
			 	</tr>
			 	<tr>
			 		<td><img src="../images/movie/movie_1.jpg"></td>
			 		<td><img src="../images/cook/food_cook_1.jpg"></td>
			 		<td><img src="../images/room/stay_1.jpg"></td>
			 	</tr>
			 </table>
		</div>
		
	</div>
</body>
</html>