<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!-- 메뉴바 css -->
<link rel="stylesheet" href="/project_datesystem/css/main/top_menu.css"/>
</head>
<body>
<div id="logo"><img src = "/project_datesystem/images/logo/logo.png"></div>
<div id="menubar">
				<ul>
					<li class = "menu"><a href = "/project_datesystem/main/main.jsp">홈</a></li>
					<li class = "menu"><a href = "/project_datesystem/main/tour_layout.jsp">주변 먹거리</a></li>
					<li class = "menu"><a href = "/project_datesystem/user/tour/tour_list.jsp">데이트장소</a></li>
					<li class = "menu"><a href = "/project_datesystem/user/stay/stay_list.jsp">주변 숙박시설</a></li>
					<li class = "menu">오늘의 영화</li>
					<li class = "menu">우리들 이야기</li>
					<li class = "menu">고 객 센 터</li>
				</ul>
			</div>
</body>
</html>