<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.StayDAO, dao.StayVO, util.StringUtil"%>
<%@ page import="dao.FoodDAO, dao.FoodVO"%>
<%@ include file="../../common/sub_header.jsp"%>
<%
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="../css/main/slider_style.css"/>
<link rel="stylesheet" href="../js/main/bxslider-4-development/dist/jquery.bxslider2.css" />
<script src="../js/main/bxslider-4-development/dist/jquery.bxslider2.js"></script>
<script src="../js/main/slide_banner.js"></script>
<script type="text/javascript" src="../js/main/slide.js"></script>
<script type="text/javascript">
$(function(){
	
	/* ********************** 날씨 ********************** */
	expires = new Date();					
	expires.setTime(expires.getTime() + (1000 * 3600 * 24 * 365));	
	set_cookie('cityid', 1835848, expires);	
	$.getJSON("http://api.openweathermap.org/data/2.5/forecast/daily?callback=?&id=1835848&units=metric&cnt=14", showForecastDaily).error(errorHandler);
	/* ********************** 날씨 ********************** */
	
	/* 이미지슬라이드 */
	imageSilide(2500);
	
	
});

</script>
</head>
<body>
	<div id="slide" class="sliderWrapper">
			<!-- <img src = "../images/slide/top/1.jpg"> -->
			<ul class="bxslider">
			<%
				FoodDAO dao = new FoodDAO();
				ArrayList<FoodVO> list = dao.selectAll(1, 5, "", "", "date");
				
				for(FoodVO vo : list){
			%>
				<li class="slide"><img src="<%=vo.getFood_imgpath_1_v() %>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /></li>
			<% 
				}
			%>
			</ul>
		</div>

</body>
</html>