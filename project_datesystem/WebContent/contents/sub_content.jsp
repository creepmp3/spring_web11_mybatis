<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.BoardManDAO, dao.BoardManVO, java.util.*"%>
<%@ page import="dao.BoardWoManDAO, dao.BoardWoManVO"%>
<%
	BoardManDAO mdao = new BoardManDAO();
	ArrayList<BoardManVO> boardM_list = mdao.selectAll(1, 7, "", "", "");
	
	BoardWoManDAO wdao = new BoardWoManDAO();
	ArrayList<BoardWoManVO> boardW_list = wdao.selectAll(1, 7, "", "", "");
%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/content_css/sub_content_style.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<title>Insert title here</title>
<script type="text/javascript">
$(function(){
	$("a[name='boardM']").each(function(i, e){
		$(e).on("click", function(){
			if("<%=session_member_no_n%>" > 0){
				if("<%=session_member_gender_v%>" == "M" || "<%=session_member_type_v%>" == "S"){
					$("#seq_n").val($(e).attr("seq"));
					$("#moveForm").attr("action", "/project_datesystem/user/board/m/boardM_detail.jsp").submit();
				}else{
					alert("남성전용 게시판입니다");
				}
			}else{
				if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
					$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
				}
			}
			return false;
		});
	});
	
	$("a[name='boardW']").each(function(i, e){
		$(e).on("click", function(){
			if("<%=session_member_no_n%>" > 0){
				if("<%=session_member_gender_v%>" == "W" || "<%=session_member_type_v%>" == "S"){
					$("#seq_n").val($(e).attr("seq"));
					$("#moveForm").attr("action", "/project_datesystem/user/board/w/boardW_detail.jsp").submit();
				}else{
					alert("여성전용 게시판입니다");
				}
			}else{
				if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
					$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
				}
			}
			return false;
		});
	});
});

function moveBoardM(){
	if("<%=session_member_no_n%>" > 0){
		if("<%=session_member_gender_v%>" == 'M' || "<%=session_member_type_v%>" == "S"){
			location.href = "/project_datesystem/user/board/m/boardM_list.jsp";
		}else{
			alert("남성전용 게시판입니다");
			return false;
		}
	}else{
		if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
			$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
			return false;
		}
	}
	
}


function moveBoardW(){
	if("<%=session_member_no_n%>" > 0){
		if("<%=session_member_gender_v%>" == 'W' || "<%=session_member_type_v%>" == "S"){
			location.href = "/project_datesystem/user/board/w/boardW_list.jsp";
		}else{
			alert("여성전용 게시판입니다");
			return false;
		}
	}else{
		if(confirm("로그인이 필요한 서비스입니다.\n로그인페이지로 이동하시겠습니까?")){
			$("#moveForm").attr("action", "/project_datesystem/login/login.jsp").submit();
			return false;
		}
	}
	
}
</script>
<style type="text/css">
.li_st {
	border-bottom:1px solid #FFB3C2;
	margin-top:10px;
	margin-left:5px;
	font-size:14pt;
}
.li_st a, .li_st a:ACTIVE, .li_st a:HOVER , .li_st a:VISITED {	
	color:#4186A1;
	text-shadow: 1px 1px 3px #909090;
}
.regdM {
	margin-top:5px;
	position: absolute;
	right: 255px;
	font-size:9pt;
}
.regdW {
	margin-top:5px;
	position: absolute;
	right: 3px;
	font-size:9pt;
}
</style>
</head>
<body>
<form id="moveForm" >
	<input type="hidden" name="seq_n" id="seq_n"/>
</form>
	<div id="slider">
	<jsp:include page="slider.jsp"></jsp:include> 
	</div>
	<div id="content">
		<div id="content_sub_text">오늘 어디 갈까?</div>
		<div id="content_sub">
			<div id="content_top_image">
				<a href = "../user/stay/stay_list.jsp"><img src="../images/slide/sub/stay_sub_1.jpg">
			</a></div>
			<div id="content_board_girl">
				<div id="girl"><a href="#" onclick="return moveBoardW()">여성 게시판</a></div>
				<ul>
					<%
					for(BoardWoManVO wvo : boardW_list){
					    String reStr = "";
					    String reSp = "";
						if(wvo.getRef_n()>0){
						    for(int i=1; i<wvo.getLv(); i++){
						        reSp+="　";
						        reStr = "└>";
						    }
						}
					%>
					<li class="li_st">
						<a href="#" name="boardW" seq="<%=wvo.getSeq_n()%>"><%=reSp%><%=reStr%><%=wvo.getTitle_v() %></a> 
						<span class="regdW"><%=wvo.getReg_d() %></span>
					</li>
					<%
					}
					%>
				</ul>
				
			</div>
			<div id="content_board_boy">
				<div id="boy"><a href="#" onclick="return moveBoardM()">남성 게시판</a></div>
				<ul>
					<%
					for(BoardManVO mvo : boardM_list){
					    String reStr = "";
					    String reSp = "";
						if(mvo.getRef_n()>0){
						    for(int i=1; i<mvo.getLv(); i++){
						        reSp+="　";
						        reStr = "└>";
						    }
						}
					%>
					<li class="li_st">
						<a href="#" name="boardM" seq="<%=mvo.getSeq_n()%>"><%=reSp%><%=reStr%><%=mvo.getTitle_v() %></a> 
						<span class="regdM"><%=mvo.getReg_d() %></span>
					</li>
					<%
					}
					%>
				</ul>
			</div>
		</div>
		<div id="t_text">오늘의 이슈!!</div>
		<div id="content_t_image">
			<ul>
				<li class="c_image"><img src="../images/sub_main/sub_main_1.jpg"></li>
				<li class="c_image"><img
					src="../images/sub_main/sub_main_2.jpg"></li>
				<li class="c_image"><img
					src="../images/sub_main/sub_main_3.jpg"></li>
			</ul>
		</div>
		<div id="content_list_text">
			<ul>
				<li>추천 장소</li>
				<li>추천 맛집</li>
				<li>추천 영화</li>
			</ul>
		</div>
		<div id="content_t_image">
			<ul>
				<li class="c_image"><a href="/project_datesystem/user/tour/tour_list.jsp"><img src="../images/room/stay_1.jpg"></a></li>
				<li class="c_image"><a href="/project_datesystem/user/food/food_list.jsp"><img src="../images/cook/food_cook_1.jpg"></a></li>
				<li class="c_image"><img src="../images/movie/movie_1.jpg"></li>
			</ul>
		</div>
	</div>
</body>
</html>