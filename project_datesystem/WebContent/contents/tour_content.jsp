<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript"
	src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<link rel="stylesheet" type="text/css"
	href="../css/content_css/main_contents_style.css" />
<link rel="stylesheet" href="../css/content_css/tour_content_style.css" />
<script type="text/javascript" src="../js/main/weather.js"></script>
<link rel="stylesheet" href="../css/main/weather.css" />
<script type="text/javascript">
	$(document).ready(function() {
		$("#option_content_local").hide();
		$("#option_content_tema").hide();
		$("#local").on('click', function() {
			$("#option_content_local").toggle();
		});
		$("#tema").on('click', function() {
			$("#option_content_tema").toggle();
		});

	});
</script>

</head>
<body>
	<div id="main">
		<jsp:include page="content_menu.jsp"></jsp:include>
		
		<div id = "cook_list">
		<jsp:include page="cook.jsp"></jsp:include>
		</div>
	</div>


</body>
</html>