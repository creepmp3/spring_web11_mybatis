<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<style type="text/css">
		
		#footerDesc {
			text-align: center;
			font-size: 12px;
			color: gray;
			line-height: 2.3em;
			clear:both;
		}
		
	</style>
</head>
<body>

<br/><br/>
<div id="footerDesc">
	회사소개&nbsp;&nbsp;제휴광고문의&nbsp;&nbsp;
	인재채용&nbsp;&nbsp;이용약관&nbsp;&nbsp;
	개인정보취급방침&nbsp;&nbsp;청소년보호정책&nbsp;&nbsp;
	책임의 한계와 법적고지&nbsp;&nbsp;이메일무단수집거부&nbsp;&nbsp;고객감동센터&nbsp;&nbsp;
	<hr>
	(주)HBI 3조 | 김도연, 한세호, 전태영, 태대섭, 김찬혁 | 사업자 270-78-43889, 통신판매 종로-14212호 | 고객감동센터 112 <br/>
	주소 (132-817) 서울시 종로구 돈의동 170번지 하늘빌딩 3, 4, 5층 | <b>Copyright ⓒ samjo Corp. All rights reserved.</b>
</div>
<br/><br/>

</body>
</html>