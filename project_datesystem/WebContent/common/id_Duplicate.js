
function id_Duplicate(str){
	console.log("str : "+ str);
	var msg = "";
	var color = "";
	var strLength = 0;
	var strlength = str.length;
	
	$.ajax({
		data:{"useridv":str},
		dataType:"json",
		type:"post",
		url:"/admin/user/id_duplAjax.jsp",
		success:function(data){
			
			if(space_fl_pattern.test(str) && space_m_pattern.test(str)){ 
				msg = "공백을 포함할수 없습니다";
				color = "red";
			}else{
				
				if(str==""){	
					msg = "아이디를 입력해주세요";
					color = "red";
				}else{
					
					for(var i=0; i<strlength; i++){
						if(str.charAt(i)>128) strLength +=  2;
						else strLength += 1;
					
						if(strLength > 2){
							
							if(id_pattern.test(str)){
								if(data.cnt==0){
									msg = "사용가능한 아이디입니다";
									color = "blue";
									idDupFlag = true;
								}else{
									msg = "이미사용하고 있는 아이디입니다";
									color = "red";
								}
							}else{
								msg = "문자와 숫자로만 입력해주세요";
								color = "red";
							}
							
						}else{
							msg = "2글자 이상 입력해주세요";
							color = "red";
						}
					}
					
				}
				msg += " ("+str.length+")";
			}
			$("#duplMsg").text(msg).css("color", color);
		},
		error:function(req){
			alert("error : " + req.responseText);
		}
	});
	
}