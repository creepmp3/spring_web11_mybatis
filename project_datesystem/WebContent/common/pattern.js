var id_pattern = /^[\w]|[\w]+$|^[\S]|[\S]+$/gi; //(^\S*)|(\S*$)

var space_fl_pattern = /(^\s*)|(\s*$)/; //앞뒤 공백 찾기
var space_m_pattern = /\s/g; //문자열 중 공백 찾기

//testString = testString.replace(/^\s+/,""); // 문자열 시작 부분의 공백 제거
//testString = testString.replace(/\s+$/,""); // 문자열 끝 부분의 공백 제거

//HTML 태그를 명명된 엔티티로 치환하기
//pieceOffHtml  = pieceOffHtml.replace(/</g,"&lt");
//pieceOffHtml  = pieceOffHtml.replace(/>/g,"&lt");

//특수 문자 검색하기
var re = /\\d/;
var pattern = "\\d{4}";
var pattern2 = pattern.replace(re,"\\D");

var kor_pattern = /^[가-힣]+$/; //한글만 가능
var num_pattern = /^[0-9]+$/; //(숫자)
var pwd_pattern = /^[\d]+$|^[a-zA-Z]+$/gi; //비밀번호 같은 경우 숫자+영문이 아닌 경우  
var web_pattern = /((http|https|ftp):\/\/[\w?=&.\/-;#~%-]+(?![\w\s?&.\/;#~%"=-]*>))/g;

var email_pattern = /^([0-9a-zA-Z_-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;

//이메일 형식만 가능 : [ test@nate.com ]
var email_pattern2 = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/

//var ip_pattern = /^(1|2)?\d?\d([.](1|2)?\d?\d){3}$/;
var ip_pattern = /^[0-9a-zA-Z_-]+$/;   // 한글, 영문만 가능하도록


var phone_pattern1 = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
var phone_pattern2 = /^[0-9]{2,3}-[0-9]{3,4}-[0-9]{4}$/;
var tel_pattern =  /^\d{2,3}-\d{3,4}-\d{4}$/;

var url_pattern1 =  /(?:(?:(https?|ftp|telnet):\/\/|[\s\t\r\n\[\]\`\<\>\"\'])((?:[\w$\-_\.+!*\'\(\),]|%[0-9a-f][0-9a-f])*\:(?:[\w$\-_\.+!*\'\(\),;\?&=]|%[0-9a-f][0-9a-f])+\@)?(?:((?:(?:[a-z0-9\-가-힣]+\.)+[a-z0-9\-]{2,})|(?:[\d]{1,3}\.){3}[\d]{1,3})|localhost)(?:\:([0-9]+))?((?:\/(?:[\w$\-_\.+!*\'\(\),;:@&=ㄱ-ㅎㅏ-ㅣ가-힣]|%[0-9a-f][0-9a-f])+)*)(?:\/([^\s\/\?\.:<>|#]*(?:\.[^\s\/\?:<>|#]+)*))?(\/?[\?;](?:[a-z0-9\-]+(?:=[^\s:&<>]*)?\&)*[a-z0-9\-]+(?:=[^\s:&<>]*)?)?(#[\w\-]+)?)/gmi;

//도메인 형태, http:// https:// 포함안해도 되고 해도 되고
var url_pattern2 = /^(((http(s?))\:\/\/)?)([0-9a-zA-Z\-]+\.)+[a-zA-Z]{2,6}(\:[0-9]+)?(\/\S*)?$/;

var num = ["Jan", "2", "34lasdkf", "1092480394", "Feb"];

//pattern.test(num[0]) -> num[0]가 숫자 pattern인지 조사함.