<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.oreilly.servlet.MultipartRequest" %>
<%@ page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@ page import="dao.StayDAO, dao.StayVO, util.StringUtil" %>
<%
	String uploadPath = request.getRealPath("/upload/stay");
	
	int maxSize = 1024*1024*10; // 10MB
	int result = 0;
	
		MultipartRequest multi = new MultipartRequest(request, uploadPath, maxSize, "UTF-8", new DefaultFileRenamePolicy());
	
		String stay_title_v = multi.getParameter("stay_title_v");
		String stay_info_title_v = multi.getParameter("stay_info_title_v");
		String stay_content_v = multi.getParameter("stay_content_v");
		String stay_tel_v = multi.getParameter("stay_tel_v");
		String stay_open_time_v = multi.getParameter("stay_open_time_v");
		String stay_close_time_v = multi.getParameter("stay_close_time_v");
		String stay_price_v = multi.getParameter("stay_price_v");
		String stay_holiday_v = multi.getParameter("stay_holiday_v");
		String stay_type_v = multi.getParameter("stay_type_v");
		String stay_loc_v = multi.getParameter("stay_loc_v");
		String stay_use_yn_v = multi.getParameter("stay_use_yn_v");
		String stay_imgpath_1_v = multi.getOriginalFileName("stay_imgpath_1_v");
		String stay_imgpath_2_v = multi.getOriginalFileName("stay_imgpath_2_v");
		String stay_imgpath_3_v = multi.getOriginalFileName("stay_imgpath_3_v");
		String stay_imgpath_4_v = multi.getOriginalFileName("stay_imgpath_4_v");
		String stay_imgpath_5_v = multi.getOriginalFileName("stay_imgpath_5_v");
		String stay_imgpath_6_v = multi.getOriginalFileName("stay_imgpath_6_v");
		String stay_imgpath_7_v = multi.getOriginalFileName("stay_imgpath_7_v");
		String stay_imgpath_8_v = multi.getOriginalFileName("stay_imgpath_8_v");
		
		StayDAO dao = new StayDAO();
		StayVO vo = new StayVO(stay_title_v, stay_info_title_v, stay_content_v, stay_tel_v, stay_open_time_v,
	            stay_close_time_v, stay_price_v, stay_holiday_v, stay_type_v,
	            stay_loc_v, stay_imgpath_1_v, stay_imgpath_2_v,
	            stay_imgpath_3_v, stay_imgpath_4_v, stay_imgpath_5_v,
	            stay_imgpath_6_v, stay_imgpath_7_v, stay_imgpath_8_v);
		
		result = dao.insertOne(vo);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
var result = "<%=result%>";
var msg = "등록실패";
if(result>0){
	msg ="등록성공";
}
alert(msg);
location.href = "stay_list.jsp";
</script>
<title></title>
</head>
<body>

</body>
</html>