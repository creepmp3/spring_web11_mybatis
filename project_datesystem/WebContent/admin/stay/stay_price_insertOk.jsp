<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.StayPriceDAO, dao.StayPriceVO, util.StringUtil" %>
<%
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
	
	int result = 0;
	
	if(stay_no_n>0){
	    String stay_price_nm_v = StringUtil.nvl(request.getParameter("stay_price_nm_v"), "");
	    String stay_d_w_v = StringUtil.nvl(request.getParameter("stay_d_w_v"), "");
	    String stay_d_h_v = StringUtil.nvl(request.getParameter("stay_d_h_v"), "");
	    String stay_s_w_v = StringUtil.nvl(request.getParameter("stay_s_w_v"), "");
	    String stay_s_h_v = StringUtil.nvl(request.getParameter("stay_s_h_v"), "");
	    String stay_use_yn_v = StringUtil.nvl(request.getParameter("stay_use_yn_v"), "Y");
	    
	    StayPriceDAO dao = new StayPriceDAO();
	    StayPriceVO vo = new StayPriceVO(stay_no_n, stay_price_nm_v, stay_d_w_v, 
	            stay_d_h_v, stay_s_w_v, stay_s_h_v, stay_use_yn_v);
	    result = dao.insertOne(vo);
	    
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
var result = "<%=result%>";
var msg = "등록실패";
if(result>0){
	msg ="등록성공";
}
alert(msg);
location.href = "stay_view.jsp?stay_no_n=<%=stay_no_n%>";
</script>
<title></title>
</head>
<body>

</body>
</html>