<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.StayPriceDAO, dao.StayPriceVO, java.util.ArrayList, util.StringUtil" %>
<%
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
	int stay_price_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_price_no_n"), "0"));
	String stay_price_nm_v = StringUtil.nvl(request.getParameter("stay_price_nm_v"), "");
	String stay_d_w_v = StringUtil.nvl(request.getParameter("stay_d_w_v"), "");
	String stay_d_h_v = StringUtil.nvl(request.getParameter("stay_d_h_v"), "");
	String stay_s_w_v = StringUtil.nvl(request.getParameter("stay_s_w_v"), "");
	String stay_s_h_v = StringUtil.nvl(request.getParameter("stay_s_h_v"), "");

	StayPriceDAO dao = new StayPriceDAO();
	StayPriceVO vo = new StayPriceVO(stay_no_n, stay_price_no_n, stay_price_nm_v, stay_d_w_v, stay_d_h_v, stay_s_w_v, stay_s_h_v);
	int result = dao.updateOne(vo);
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
var result = "<%=result%>";
var msg = "수정실패";
if(result>0){
	msg ="수정성공";
}
alert(msg);
location.href = "stay_price_modify.jsp?stay_no_n=<%=stay_no_n%>&stay_price_no_n=<%=stay_price_no_n%>";
</script>
<title></title>
</head>
<body>

</body>
</html>