<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.StayDAO, dao.StayVO" %>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(document).ready(function(){
	
	
});

function validate(){
	if($("#stay_title_v").val()==""){
		alert("제목 입력해주세요");
		$("#stay_title_v").focus();
		return false;
	}
	if($("#stay_content_v").val()==""){
		alert("내용을 입력해주세요");
		$("#stay_content_v").focus();
		return false;
	}
	if($("#stay_tel_v").val()==""){
		alert("전화번호를 입력해주세요");
		$("#stay_tel_v").focus();
		return false;
	}
	if($("#stay_open_time_v").val()==""){
		alert("OPEN시간을 입력해주세요");
		$("#stay_open_time_v").focus();
		return false;
	}
	if($("#stay_close_time_v").val()==""){
		alert("CLOSE시간을 입력해주세요");
		$("#stay_close_time_v").focus();
		return false;
	}
	if($("#stay_price_v").val()==""){
		alert("가격을 입력해주세요");
		$("#stay_price_v").focus();
		return false;
	}
	if($("#stay_holiday_v").val()==""){
		alert("휴일을 입력해주세요");
		$("#stay_holiday_v").focus();
		return false;
	}
	if($("#stay_type_v").val()==""){
		alert("종류를 선택해주세요");
		$("#stay_type_v").focus();
		return false;
	}
	if($("#stay_loc_v").val()==""){
		alert("지역을 입력해주세요");
		$("#stay_loc_v").focus();
		return false;
	}
	if(confirm("등록하시겠습니까?")){
		return true;
	}
}
</script>
<style type="text/css">
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.btnArea {
	clear:both;
	margin-top:10px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">	
		<div class="view">
			<h2>관리자페이지</h2>
			<form action="./stay_insertOk.jsp" onsubmit="return validate()" enctype="multipart/form-data" method="post">
				<input type="hidden" name="stay_use_yn_v" value="Y"/>
				<table class="tableView">
					<tr>
						<th><label for="stay_title_v">제목</label></th>
						<td><input type="text" name="stay_title_v" id="stay_title_v" style="width:99%" /></td>
					</tr>
					<tr>
						<th><label for="stay_info_title_v">부제목</label></th>
						<td><input type="text" name="stay_info_title_v" id="stay_info_title_v" style="width:99%" /></td>
					</tr>
					<tr>
						<th><label for="stay_content_v">내용</label></th>
						<td>
							<textarea name="stay_content_v" id="stay_content_v" cols="70" rows="10"></textarea>
						</td>
					</tr>
					<tr>
						<th><label for="stay_tel_v">전화번호</label></th>
						<td><input type="text" name="stay_tel_v" id="stay_tel_v"/></td>
					</tr>
					<tr>
						<th><label for="stay_open_time_v">OPEN시간</label></th>
						<td><input type="text" name="stay_open_time_v" id="stay_open_time_v"/></td>
					</tr>
					<tr>
						<th><label for="stay_close_time_v">CLOSE시간</label></th>
						<td><input type="text" name="stay_close_time_v" id="stay_close_time_v"/></td>
					</tr>
					<tr>
						<th><label for="stay_price_v">가격</label></th>
						<td><input type="text" name="stay_price_v" id="stay_price_v"/></td>
					</tr>
					<tr>
						<th><label for="stay_holiday_v">휴일</label></th>
						<td><input type="text" name="stay_holiday_v" id="stay_holiday_v"/></td>
					</tr>
					<tr>
						<th><label for="stay_type_v">종류</label></th>
						<td>
							<select id="stay_type_v" name="stay_type_v">
								<option value="M">모텔
								<option value="H">호텔
								<option value="G">게스트하우스
							</select>	
						</td>
					</tr>
					<tr>
						<th><label for="stay_loc_v">지역</label></th>
						<td><input type="text" name="stay_loc_v" id="stay_loc_v"/></td>
					</tr>
					<tr>
						<th>이미지1</th>
						<td><input type="file" name="stay_imgpath_1_v" id="stay_imgpath_1_v" /></td>
					</tr>
					<tr>
						<th>이미지2</th>
						<td><input type="file" name="stay_imgpath_2_v" id="stay_imgpath_2_v" /></td>
					</tr>
					<tr>
						<th>이미지3</th>
						<td><input type="file" name="stay_imgpath_3_v" id="stay_imgpath_3_v" /></td>
					</tr>
					<tr>
						<th>이미지4</th>
						<td><input type="file" name="stay_imgpath_4_v" id="stay_imgpath_4_v" /></td>
					</tr>
					<tr>
						<th>이미지5</th>
						<td><input type="file" name="stay_imgpath_5_v" id="stay_imgpath_5_v" /></td>
					</tr>
					<tr>
						<th>이미지6</th>
						<td><input type="file" name="stay_imgpath_6_v" id="stay_imgpath_6_v" /></td>
					</tr>
					<tr>
						<th>이미지7</th>
						<td><input type="file" name="stay_imgpath_7_v" id="stay_imgpath_7_v" /></td>
					</tr>
					<tr>
						<th>이미지8</th>
						<td><input type="file" name="stay_imgpath_8_v" id="stay_imgpath_8_v" /></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" value="확인" class="btnPink ar"/>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>