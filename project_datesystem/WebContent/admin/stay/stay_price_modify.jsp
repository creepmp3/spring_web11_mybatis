<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.StayPriceDAO, dao.StayPriceVO, java.util.ArrayList, util.StringUtil" %>
<%
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
	int stay_price_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_price_no_n"), "0"));
%>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#priceListBtn").on("click", function(){
		$("#frm").attr("action", "stay_view.jsp").submit();
	});
	
	$("#priceModifyBtn").on("click", function(){
		$("#frm").attr("action", "stay_price_modifyOk.jsp").submit();
	});
	
	$("#priceDelBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			$("#frm").attr("action", "stay_price_deleteOk.jsp").submit();
		}
	});
	
});
</script>
<style type="text/css">
img {
	width:300px;
	height:200px;
}

.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.btnArea {
	clear:both;
	margin-top:10px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<form id="frm">
			<input type="hidden" name="stay_no_n" value="<%=stay_no_n%>"/>
			<input type="hidden" name="stay_price_no_n" value="<%=stay_price_no_n%>" />
						
			<div class="list">
			<h2>관리자페이지</h2>
				<table>
					<thead>
						<tr>
							<th rowspan="2">구분</th>
							<th colspan="2">대실</th>
							<th colspan="2">숙박</th>
						</tr>
						<tr>
							<td>월~금</td>
							<td>토~일</td>
							<td>월~금</td>
							<td>토~일</td>
						</tr>
					</thead>
					<tbody>
					<%
					
					StayPriceDAO dao = new StayPriceDAO();
					StayPriceVO vo = dao.selectOne(stay_no_n, stay_price_no_n);
					
					%>
						<tr>
							<th><input name="stay_price_nm_v" value="<%=vo.getStay_price_nm_v() %>" /></th>
							<td><input name="stay_d_w_v" value="<%=vo.getStay_d_w_v() %>"/></td>
							<td><input name="stay_d_h_v" value="<%=vo.getStay_d_h_v() %>"/></td>
							<td><input name="stay_s_w_v" value="<%=vo.getStay_s_w_v() %>"/></td>
							<td><input name="stay_s_h_v" value="<%=vo.getStay_s_h_v() %>"/></td>
						</tr>
					</tbody>
				</table>
				<div class="btnArea">
					<a href="#" class="btnPink" id="priceDelBtn">삭제</a>
					<a href="#" class="btnPink" id="priceModifyBtn">확인</a>
					<a href="#" class="btnPink" id="priceListBtn">상위페이지</a>
				</div>
			</div>
		</form>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>