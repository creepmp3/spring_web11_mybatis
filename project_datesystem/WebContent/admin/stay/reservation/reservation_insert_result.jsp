<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String resDate = StringUtil.nvl(request.getParameter("resDate"), "");
	String user_tel_v = StringUtil.nvl(request.getParameter("user_tel_v"), "");
	String reservation_date_v = StringUtil.nvl(request.getParameter("reservation_date_v"), "");
	String reservation_time_v = StringUtil.nvl(request.getParameter("reservation_time_v"), "");
	String reservation_day_v = StringUtil.nvl(request.getParameter("reservation_day_v"), "");
	int stay_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_no_n"), "0"));
	int stay_price_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_price_no_n"), "0"));
	int reservation_range_v = Integer.parseInt(StringUtil.nvl(request.getParameter("reservation_range_v"), "0"));
	String stay_price_type_v = StringUtil.nvl(request.getParameter("stay_price_type_v"), "");
	String stay_price_type2_v = StringUtil.nvl(request.getParameter("stay_price_type2_v"), "");
	int people = Integer.parseInt(StringUtil.nvl(request.getParameter("people"), "0"));
	String stay_title_v = StringUtil.nvl(request.getParameter("stay_title_v"), "");
	String stay_type_v = StringUtil.nvl(request.getParameter("stay_type_v"), "");
	String stay_price_nm_v = StringUtil.nvl(request.getParameter("stay_price_nm_v"), "");
	int stay_price_v = Integer.parseInt(StringUtil.nvl(request.getParameter("stay_price_v"), "0"));
	String reservation_type_v = "";
	String stay_type_v_name = "";
	
	if(stay_type_v.equals("M")){
		stay_type_v_name = "모텔";	
	}else if(stay_type_v.equals("H")){
		stay_type_v_name = "호텔";	
	}else if(stay_type_v.equals("G")){
		stay_type_v_name = "게스트하우스";	
	}
	
	if(stay_price_type_v.equals("숙박")){
	    if(stay_price_type2_v.equals("평일")){
	    	reservation_type_v = "sw";
	    }else if(stay_price_type2_v.equals("주말")){
	        reservation_type_v = "sh";
	    }
	}if(stay_price_type_v.equals("대실")){
	    if(stay_price_type2_v.equals("평일")){
	    	reservation_type_v = "dw";
	    }else if(stay_price_type2_v.equals("주말")){
	        reservation_type_v = "dh";
	    }
	}
	
	int totalPay = (stay_price_v+people) * reservation_range_v;

	people /= 10000; 
	
%>
<%@ include file="/common/sub_header.jsp" %>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(document).ready(function(){
	
	
	$("#resInsertBtn").on("click", function(){
		if(confirm("예약하시겠습니까?")){
			$("#insertForm").submit();
			return false;
		}
	});
});

</script>


<div class="view">
	<form id="insertForm" action="./reservation_insertOk.jsp">
		<input type="hidden" name="stay_no_n" id="stay_no_n" value="<%=stay_no_n%>" /> 
		<input type="hidden" name="stay_price_no_n" id="stay_price_no_n" value="<%=stay_price_no_n%>"/>
		<input type="hidden" name="user_tel_v" id="user_tel_v" value="<%=user_tel_v%>"/>
		<input type="hidden" name="reservation_date_v" id="reservation_date_v" value="<%=reservation_date_v%>"/>
		<input type="hidden" name="reservation_time_v" id="reservation_time_v" value="<%=reservation_time_v%>"/>
		<input type="hidden" name="reservation_range_v" id="reservation_range_v" value="<%=reservation_range_v%>"/>
		<input type="hidden" name="stay_title_v" id="stay_title_v" value="<%=stay_title_v%>"/>
		<input type="hidden" name="stay_price_nm_v" id="stay_price_nm_v" value="<%=stay_price_nm_v%>"/>
		<input type="hidden" name="stay_price_type_v" id="stay_price_type_v" value="<%=stay_price_type_v%>"/>
		<input type="hidden" name="stay_price_type2_v" id="stay_price_type2_v" value="<%=stay_price_type2_v%>"/>
		<input type="hidden" name="stay_price_v" id="stay_price_v" value="<%=stay_price_v%>"/>
		<input type="hidden" name="stay_type_v_name" id="stay_type_v_name" value="<%=stay_type_v_name%>"/>
		<input type="hidden" name="totalPay" id="totalPay" value="<%=totalPay%>"/>
		<input type="hidden" name="stay_use_yn_c" value="Y"/>
		<table>
			<tr>
				<th>전화번호</th>
				<td><%=user_tel_v %></td>
			</tr>
			<tr>
				<th>예약날짜</th>
				<td><%=reservation_date_v %></td>
			</tr>
			<tr>
				<th>예약시간</th>
				<td><%=reservation_time_v %></td>
			</tr>
			<tr>
				<th>추가인원</th>
				<td><%=people %>명</td>
			</tr>
			<tr>
				<th>숙박기간</th>
				<td><%=reservation_range_v %>일</td>
			</tr>
		</table>
		
		<table style="margin-top:10px">
			<tr style="display:none">
				<th>>번호</th>
				<td>
				</td>
			</tr>
			<tr>
				<th>이름</th>
				<td><%=stay_title_v %></td>
			</tr>
			<tr>
				<th>등급</th>
				<td><%=stay_price_nm_v %></td>
			</tr>
			<tr>
				<th>구분1</th>
				<td><%=stay_price_type_v %></td>
			</tr>
			<tr>
				<th>구분2</th>
				<td><%=stay_price_type2_v %></td>
			</tr>
			<tr>
				<th>가격</th>
				<td><%=stay_price_v %></td>
			</tr>
			<tr>
				<th>종류</th>
				<td><%=stay_type_v_name %></td>
			</tr>
		</table>
		<table style="margin-top:10px">
			<tr>
				<th>총 결제금액</th>
				<td><%= totalPay %> 원</td>
			</tr>
			<tr>
				<th>결제상태</th>
				<td>
					<select name="pay_yn_v" id="pay_yn_v">
						<option value="N">미결제
						<option value="Y">결제완료
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="button" value="확인" id="resInsertBtn" class="btn"/>
				</td>
			</tr>
		</table>
	</form>
</div>
<%@ include file="/common/sub_footer.jsp" %>