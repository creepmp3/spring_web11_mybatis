<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String resDate = StringUtil.nvl(request.getParameter("resDate"), "");
%>
<%@ include file="/common/sub_header.jsp" %>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(document).ready(function(){
	$("#staySearchBtn").on("click", function(){
		var w = 950;
		var h = 500;
		LeftPosition = (screen.width-w)/2;
		TopPosition = (screen.height-h)/2;
		window.open("../stay_list.jsp", "stay", "left="+LeftPosition+",top="+TopPosition+",width="+w+",height="+h+",status=no, scrollbars=no,resizable=no,location=no,titlebar=no");
	});
	
	var resDate = "<%=resDate%>";
	if(resDate==""){
		document.getElementById('reservation_date_v').valueAsDate = new Date();
	}else{
		document.getElementById('reservation_date_v').valueAsDate = new Date(resDate);
	}
	
	$("#reservation_range_v").on("change", function() {
	    $(".rangeTxt").text($(this).val());
	});
	
	if($("#stay_price_type_v").val()!="대실" && $("#stay_no_n").val()!=""){
		$(".reservation_tr").css("display", "block");
	}
	
	$("#resInsertBtn").on("click", function(){
		validate();
	});
});

function validate(){
	if($("#user_tel_v").val()==""){
		alert("전화번호를 입력해주세요");
		$("#user_tel_v").focus();
		return false;
	}
	if($("#stay_title_v").val()==""){
		alert("숙박정보를 불러와주세요");
		$("#stay_title_v").focus();
		return false;
	}
	if($("#confirm_yn_v").val()==""){
		alert("예약승인 타입을 지정해주세요");
		$("#confirm_yn_v").focus();
		return false;
	}
	
	if(confirm("예약하시겠습니까?")){
		var stay_no_n = $("#stay_no_n").val();
		var stay_price_no_n = $("#stay_price_no_n").val();
		var date = $("#reservation_date_v").val();
		var time = $("#reservation_time_v").val();
		
		$.ajax({
			type:"GET",
			url:"reservation_date_dupl.jsp",
			dataType:"json",
			data:{"stay_no_n":stay_no_n,
				  "stay_price_no_n":stay_price_no_n,
				  "date":date,
				  "time":time},
			success:function(data, status, request){
				
				if(parseInt(data.count) > 0){
					alert("예약이 중복되었습니다");
					return false;
				}else{
					/* if(confirm("예약이 가능합니다\n예약하시겠습니까?")){ */
						$("#insertForm").submit();
						return true;
					/* } */
				}
			},
			error:function(response, status, request){
				alert("에러");
				return false;
			}
		});
	}
	return false;
}
</script>

<style type="text/css">


:read-only {
    background-color:#ccc;
}
</style>

<div class="view">
	<h2>관리자페이지</h2>
	<form id="insertForm" action="./reservation_insert_result.jsp">
		<input type="hidden" name="stay_use_yn_c" value="Y"/>
		<table>
			<tr>
				<th><label for="user_tel_v">전화번호</label></th>
				<td><input type="text" name="user_tel_v" id="user_tel_v"/></td>
			</tr>
			<tr>
				<th><label for="user_tel_v">예약날짜</label></th>
				<td><input type="date" id="reservation_date_v" name="reservation_date_v" id="reservation_date_v" /></td>
			</tr>
			<tr>
				<th><label for="reservation_time_v">예약시간</label></th>
				<td><input type="time" name="reservation_time_v" id="reservation_time_v" value="22:30" step="1800"/></td>
			</tr>
			<tr>
				<th><label for="people">인원추가</label></th>
				<td>
					<select name="people" id="people">
					<%
					for(int i=0; i<=4; i++){
					    out.println("<option value='"+(i*10000)+"' />"+i+"명");
					}
					%>
					</select>
					<span>(1명당 10,000원 추가)</span>
				</td>
			</tr>
			<tr>
				<th><label for="reservation_day_v">숙박기간</label></th>
				<td>
					<select name="reservation_range_v" id="reservation_range_v">
					<%
					for(int i=1; i<=7; i++){
					    out.println("<option value='"+i+"' />"+i+"일");
					}
					%>
					</select>
				</td>
			</tr>
			<tr>
				<th><label for="">숙박정보검색</label></th>
				<td><a href="#" id="staySearchBtn" class="btn">검색</a></td>
			</tr>
		</table>
		
		<table style="margin-top:10px">
			<tr style="display:none">
				<th><label for="stay_title_v">번호</label></th>
				<td>
					<input type="text" name="stay_no_n" id="stay_no_n" readonly style="width:30px"/> 
					<input type="text" name="stay_price_no_n" id="stay_price_no_n" readonly style="width:30px"/>
				</td>
			</tr>
			<tr>
				<th><label for="stay_title_v">이름</label></th>
				<td><input type="text" name="stay_title_v" id="stay_title_v" readonly/></td>
			</tr>
			<tr>
				<th><label for="stay_price_nm_v">등급</label></th>
				<td><input type="text" name="stay_price_nm_v" id="stay_price_nm_v" readonly/></td>
			</tr>
			<tr>
				<th><label for="stay_price_type_v">구분1</label></th>
				<td><input type="text" name="stay_price_type_v" id="stay_price_type_v" readonly/></td>
			</tr>
			<tr>
				<th><label for="stay_price_type2_v">구분2</label></th>
				<td><input type="text" name="stay_price_type2_v" id="stay_price_type2_v" readonly/></td>
			</tr>
			<tr>
				<th><label for="stay_price_v">가격</label></th>
				<td><input type="text" name="stay_price_v" id="stay_price_v" readonly/></td>
			</tr>
			<tr>
				<th><label for="stay_type_v">종류</label></th>
				<td>
					<select id="stay_type_v" name="stay_type_v" 
					onFocus='this.initialSelect = this.selectedIndex;'
					onChange='this.selectedIndex = this.initialSelect;'>
						<option value="M">모텔
						<option value="H">호텔
						<option value="G">게스트하우스
					</select>	
				</td>
			</tr>
			<tr>
				<th>이미지</th>
				<td><img name="stay_imgpath_1_v" id="stay_imgpath_1_v" src="" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="button" value="확인" id="resInsertBtn" class="btn"/>
				</td>
			</tr>
		</table>
	</form>
</div>
<%@ include file="/common/sub_footer.jsp" %>