<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.BoardManDAO, dao.BoardManVO, util.StringUtil"%>
<%
	int seq_n = Integer.parseInt(StringUtil.nvl(request.getParameter("seq_n"), "0"));

	BoardManDAO dao = new BoardManDAO();
	BoardManVO vo = dao.selectOne(seq_n);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/community_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/main/weather.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="/project_datesystem/js/main/movie_js_style.js" /></script>

<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<title>Insert title here</title>
<script type="text/javascript">
$(function(){
	
	$("#listBtn").on("click", function(){
		location.href = "./boardM_list.jsp";
	});
	
	$("#reWriteBtn").on("click", function(){
		$("#viewForm").attr("action", "boardM_insert.jsp").submit();
		return false;
	});
	
	$("#delBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			$("#viewForm").attr("action", "boardM_delete.jsp").submit();
		}
		return false;
	});
});
</script>
<style type="text/css">
</style>
</head>
<body>
	<div id = "main">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		<div id = "sub_menu">
			<div id = "profile">
				xx님 <br/>
				상태 상태 <br/>
				뭐야 뭐야 <br/>
			</div>
			<ul>
				<li>사이드 메뉴1</li>
				<li>사이드 메뉴2</li>
				<li>사이드 메뉴3</li>
				<li>사이드 메뉴4</li>
				<li>사이드 메뉴5</li>
				<li>사이드 메뉴6</li>
				<li>사이드 메뉴7</li>
			</ul>
		</div>
		<div id = "sign_area">광고 영역</div>
		<div id = "board_content">
			<h2>관리자페이지</h2>	
			<div class="view">
				<form id="viewForm">
					<input type="hidden" name="seq_n" id="seq_n" value="<%=seq_n %>"/>
					<input type="hidden" name="ref_n" id="ref_n" value="<%=seq_n %>"/>
					<table>
						<tbody>
							<tr>
								<th>제목</th>
								<td><%=vo.getTitle_v() %></td>
							</tr>
							<tr>
								<th>내용</th>
								<td><%=vo.getContent_v() %></td>
							</tr>
							<tr>
								<th>글쓴이</th>
								<td><%=vo.getMember_nick_v() %>(<%=vo.getMember_id_v() %>)</td>
							</tr>
							<tr>
								<th>조회수</th>
								<td><%=vo.getRead_cnt_n() %></td>
							</tr>
							<tr>
								<th>등록일</th>
								<td><%=vo.getReg_d() %></td>
							</tr>
						</tbody>
					</table>
				</form>
				<div class="btnArea">
					<a href="#" class="button" id="delBtn">삭제</a>
					<a href="#" class="button" id="reWriteBtn">답글</a>
					<a href="#" class="button" id="listBtn">목록</a>
				</div>
			</div>
		</div>
	</div>
</body>
</html>