<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%
	int ref_n = Integer.parseInt(StringUtil.nvl(request.getParameter("ref_n"), "0"));
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/community_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/content_css/main_contents_style.css" />
<link rel="stylesheet" type="text/css" href="/project_datesystem/css/main/weather.css" />

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script type="text/javascript" src="/project_datesystem/js/main/movie_js_style.js" /></script>
<script type="text/javascript" src="/project_datesystem/js/se2/js/HuskyEZCreator.js" charset="utf-8"></script>
<script type="text/javascript" src="./quick_photo_uploader/plugin/hp_SE2M_AttachQuickPhoto.js" charset="utf-8"> </script>
<title>Insert title here</title>
<script type="text/javascript">
$(function(){
	
	var oEditors = [];
	nhn.husky.EZCreator.createInIFrame({
	    oAppRef: oEditors,
	    elPlaceHolder: "content_v",
	    sSkinURI: "/project_datesystem/js/se2/SmartEditor2Skin.html",
	    fCreator: "createSEditor2"
	});

	$("#insertBtn").on("click", function(){
		 // 에디터의 내용이 textarea에 적용된다.
	    oEditors.getById["content_v"].exec("UPDATE_CONTENTS_FIELD", []);
	 
	    // 에디터의 내용에 대한 값 검증은 이곳에서
	    // document.getElementById("ir1").value를 이용해서 처리한다.
		$("#insertForm").submit();
		return false;
	});

	
});

//‘저장’ 버튼을 누르는 등 저장을 위한 액션을 했을 때 submitContents가 호출된다고 가정한다.
/* function submitContents(elClickedObj) {
   
 
    try {
        elClickedObj.form.submit();
    } catch(e) {}
} */
</script>
</head>
<body>
	<div id = "main">
	<h2>관리자페이지</h2>
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		<div id = "sub_menu">
			<div id = "profile">
				xx님 <br/>
				상태 상태 <br/>
				뭐야 뭐야 <br/>
			</div>
			<ul>
				<li>사이드 메뉴1</li>
				<li>사이드 메뉴2</li>
				<li>사이드 메뉴3</li>
				<li>사이드 메뉴4</li>
				<li>사이드 메뉴5</li>
				<li>사이드 메뉴6</li>
				<li>사이드 메뉴7</li>
			</ul>
		</div>
		<div id = "sign_area">광고 영역</div>
		<div id = "board_content">
			<div style="border:0px solid black;width:735px;height:655px;position:absolute;top:-227px">
				<form id="insertForm" action="boardM_insertOk.jsp">
					<input type="hidden" name="member_no_n" id="member_no_n" value="<%=session_member_no_n %>"/>
					<input type="hidden" name="ref_n" id="ref_n" value="<%=ref_n %>"/>
					<table style="border:1px solid black;width:735px;height:auto">
						<colgroup>
							<col width="20%"/>
							<col width="80%"/>
						</colgroup>
						<tr>
							<th>제목</th>
							<td><input type="text" name="title_v" id="title_v" style="width:99%"/></td>
						</tr>
						<tr>
							<th>내용</th>
							<td>
								<textarea name="content_v" id="content_v" cols="80" rows="15"></textarea>
							</td>
						</tr>
					</table>
					<div class="btnArea">
						<a href="#" id="insertBtn" >글쓰기</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>