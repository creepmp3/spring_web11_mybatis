<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.TourDAO, dao.TourVO" %>
<%
	int tour_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("tour_no_n"),"0"));
	String tourPath = "file:///C:/eclipse/web_workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/project_datesystem/upload/tour/";

%>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#listBtn").on("click", function(){
		$("#moveForm").submit();
	});
	
	$("#deleteBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			$("#frm").attr("action", "tour_deleteOk.jsp").submit();
		}
	});
	
	$("#priceAddBtn").on("click", function(){
		$("#frm").attr("action", "tour_price_insert.jsp").submit();
	});
	
	$("#modifyBtn").on("click", function(){
		if(confirm("수정하시겠습니까?")){
			$("#frm").attr("action", "tour_modifyOk.jsp").submit();
		}
	});
	
});
</script>
<style type="text/css">
img {
	width:300px;
	height:200px;
}
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<form action="tour_view.jsp" id="moveForm">
			<input type="hidden" name="tour_no_n" value="<%=tour_no_n %>" />
		</form>
		<div class="view">
			<%
				TourDAO dao = new TourDAO();
				TourVO vo = dao.selectOne(tour_no_n);
				
				if(vo != null){
			%>
			<form id="frm" enctype="multipart/form-data" method="post">
				<input type="hidden" name="tour_no_n" id="tour_no_n" value="<%=tour_no_n %>" />
				<table>
					<tbody>
						<tr>
							<th><label for="tour_title_v">제목</label></th>
							<td><input type="text" name="tour_title_v" id="tour_title_v" value="<%=vo.getTour_title_v() %>" /></td>
						</tr>
						<tr>
							<th><label for="tour_content_v">내용</label></th>
							<td><textarea name="tour_content_v" id="tour_content_v" cols="70" rows="10"><%=vo.getTour_content_v() %></textarea></td>
						</tr>
						<tr>
							<th><label for="tour_tel_v">전화번호</label></th>
							<td><input type="text" name="tour_tel_v" id="tour_tel_v" value="<%=vo.getTour_tel_v() %>" /></td>
						</tr>
						<tr>
							<th><label for="tour_open_time_v">OPEN TIME</label></th>
							<td><input type="text" name="tour_open_time_v" id="tour_open_time_v" value="<%=vo.getTour_open_time_v() %>" /></td>
						</tr>
						<tr>
							<th><label for="tour_close_time_v">CLOSE TIME</label></th>
							<td><input type="text" name="tour_close_time_v" id="tour_close_time_v" value="<%=vo.getTour_close_time_v() %>" /></td>
						</tr>
						<tr>
							<th>가격</th>
							<td><input type="text" name="tour_price_v" id="tour_price_v" value="<%=vo.getTour_price_v() %>" /></td>
						</tr>
						<tr>
							<th><label for="tour_holiday_v">휴일</label></th>
							<td><input type="text" name="tour_holiday_v" id="tour_holiday_v" value="<%=vo.getTour_holiday_v() %>" /></td> 
						</tr>
						<tr>
							<th><label for="tour_type_v">종류</label></th>
							<td>
								<select id="tour_type_v" name="tour_type_v">
									<option value="place" <%=vo.getTour_type_v().equals("place")?"'selected=selected'":"" %>>명소
									<option value="working" <%=vo.getTour_type_v().equals("working")?"'selected=selected'":"" %>>산책로								
								</select>	
							</td>
						</tr>
						<tr>
							<th><label for="tour_loc_v">지역</label></th>
							<td><input type="text" name="tour_loc_v" id="tour_loc_v" value="<%=vo.getTour_loc_v() %>" /></td>
						</tr>
						<tr>
							<th>등록일</th>
							<td><%=vo.getTour_reg_d()%></td>
						</tr>
						<tr>
							<th>이미지1</th>
							<td>
								<img src="<%=vo.getTour_imgpath_1_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt=""/><br/>
								<input type="file" name="tour_imgpath_1_v" id="tour_imgpath_1_v" />
							</td>
						</tr>
						<tr>
							<th>이미지2</th>
							<td>
								<img src="<%=vo.getTour_imgpath_2_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="tour_imgpath_2_v" id="tour_imgpath_2_v" />
							</td>
						</tr>
						<tr>
							<th>이미지3</th>
							<td>
								<img src="<%=vo.getTour_imgpath_3_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="tour_imgpath_3_v" id="tour_imgpath_3_v" />
							</td>
						</tr>
						<tr>
							<th>이미지4</th>
							<td>
								<img src="<%=vo.getTour_imgpath_4_v()%>" alt="" /><br/>
								<input type="file" name="tour_imgpath_4_v" id="tour_imgpath_4_v" />
							</td>
						</tr>
						<tr>
							<th>이미지5</th>
							<td>
								<img src="<%=vo.getTour_imgpath_5_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="tour_imgpath_5_v" id="tour_imgpath_5_v" />
							</td>
						</tr>
						<tr>
							<th>이미지6</th>
							<td>
								<img src="<%=vo.getTour_imgpath_6_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="tour_imgpath_6_v" id="tour_imgpath_6_v" />
							</td>
						</tr>
						<tr>
							<th>이미지7</th>
							<td>
								<img src="<%=vo.getTour_imgpath_7_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="tour_imgpath_7_v" id="tour_imgpath_7_v" />
							</td>
						</tr>
						<tr>
							<th>이미지8</th>
							<td>
								<img src="<%=vo.getTour_imgpath_8_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
								<input type="file" name="tour_imgpath_8_v" id="tour_imgpath_8_v" />
							</td>
						</tr>
					</tbody>
				</table>
				<%
						}
				%>
				<div class="btnArea">
					<!-- <a href="#" class="btn"  id="deleteBtn">삭제</a> -->
					<a href="#" class="btnPink" id="modifyBtn">수정하기</a>
					<a href="#" class="btnPink" id="listBtn">상위페이지</a>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>