<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.oreilly.servlet.MultipartRequest" %>
<%@ page import="com.oreilly.servlet.multipart.DefaultFileRenamePolicy"%>
<%@ page import="dao.FoodDAO, dao.FoodVO, util.StringUtil" %>
<%
	
	String uploadPath = request.getRealPath("/upload/food");
	int maxSize = 1024*1024*10; // 10MB
	int result = 0;
	
		MultipartRequest multi = new MultipartRequest(request, uploadPath, maxSize, "UTF-8", new DefaultFileRenamePolicy());
		
		int food_no_n = Integer.parseInt(StringUtil.nvl(multi.getParameter("food_no_n"), "0"));

		String food_title_v = multi.getParameter("food_title_v");
		String food_info_title_v = multi.getParameter("food_info_title_v");
		String food_content_v = multi.getParameter("food_content_v");
		String food_tel_v = multi.getParameter("food_tel_v");
		String food_open_time_v = multi.getParameter("food_open_time_v");
		String food_close_time_v = multi.getParameter("food_close_time_v");
		String food_price_v = multi.getParameter("food_price_v");
		String food_holiday_v = multi.getParameter("food_holiday_v");
		String food_type_v = multi.getParameter("food_type_v");
		String food_loc_v = multi.getParameter("food_loc_v");
		String food_use_yn_c = multi.getParameter("food_use_yn_c");
		String food_imgpath_1_v = multi.getOriginalFileName("food_imgpath_1_v");
		String food_imgpath_2_v = multi.getOriginalFileName("food_imgpath_2_v");
		String food_imgpath_3_v = multi.getOriginalFileName("food_imgpath_3_v");
		String food_imgpath_4_v = multi.getOriginalFileName("food_imgpath_4_v");
		String food_imgpath_5_v = multi.getOriginalFileName("food_imgpath_5_v");
		String food_imgpath_6_v = multi.getOriginalFileName("food_imgpath_6_v");
		String food_imgpath_7_v = multi.getOriginalFileName("food_imgpath_7_v");
		String food_imgpath_8_v = multi.getOriginalFileName("food_imgpath_8_v");
		
		FoodDAO dao = new FoodDAO();
		FoodVO vo = new FoodVO(food_no_n, food_title_v, food_info_title_v, food_content_v, 
		        food_tel_v, food_open_time_v,food_close_time_v, 
		        food_price_v, food_holiday_v, food_type_v,food_loc_v, 
	            food_imgpath_1_v, food_imgpath_2_v,food_imgpath_3_v, food_imgpath_4_v, 
	            food_imgpath_5_v,food_imgpath_6_v, food_imgpath_7_v, food_imgpath_8_v);
		
		result = dao.updateOne(vo);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
var result = "<%=result%>";
var msg = "수정실패";
if(result>0){
	msg ="수정성공";
}
alert(msg);
location.href = "food_view.jsp?food_no_n=<%=food_no_n%>";
</script>
<title></title>
</head>
<body>

</body>
</html>