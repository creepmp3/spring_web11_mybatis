<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%@ page import="dao.FoodDAO, dao.FoodVO" %>
<%
	int food_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("food_no_n"),"0"));
	String foodPath = "file:///C:/eclipse/web_workspace/.metadata/.plugins/org.eclipse.wst.server.core/tmp0/wtpwebapps/project_datesystem/upload/food/";

%>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#listBtn").on("click", function(){
		$("#moveForm").submit();
	});
	
	$("#deleteBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			$("#frm").attr("action", "food_deleteOk.jsp").submit();
		}
	});

	$("#modifyBtn").on("click", function(){
		if(confirm("수정하시겠습니까?")){
			$("#frm").attr("action", "food_modifyOk.jsp").submit();
		}
	});
	
});
</script>
<style type="text/css">
img {
	width:300px;
	height:200px;
}
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<form action="food_view.jsp" id="moveForm">
			<input type="hidden" name="food_no_n" value="<%=food_no_n %>" />
		</form>
			<div class="view">
				<%
					FoodDAO dao = new FoodDAO();
					FoodVO vo = dao.selectOne(food_no_n);
					
					if(vo != null){
				%>
				<form id="frm" enctype="multipart/form-data" method="post">
					<input type="hidden" name="food_no_n" id="food_no_n" value="<%=food_no_n %>" />
					<table>
						<tbody>
							<tr>
								<th><label for="food_title_v">제목</label></th>
								<td><input type="text" name="food_title_v" id="food_title_v" value="<%=vo.getFood_title_v() %>" /></td>
							</tr>
							<tr>
								<th><label for="food_info_title_v">부제목</label></th>
								<td><input type="text" name="food_info_title_v" id="food_info_title_v" value="<%=vo.getFood_info_title_v() %>" /></td>
							</tr>
							<tr>
								<th><label for="food_content_v">내용</label></th>
								<td><textarea name="food_content_v" id="food_content_v" cols="70" rows="10"><%=vo.getFood_content_v() %></textarea></td>
							</tr>
							<tr>
								<th><label for="food_tel_v">전화번호</label></th>
								<td><input type="text" name="food_tel_v" id="food_tel_v" value="<%=vo.getFood_tel_v() %>" /></td>
							</tr>
							<tr>
								<th><label for="food_open_time_v">OPEN TIME</label></th>
								<td><input type="text" name="food_open_time_v" id="food_open_time_v" value="<%=vo.getFood_open_time_v() %>" /></td>
							</tr>
							<tr>
								<th><label for="food_close_time_v">CLOSE TIME</label></th>
								<td><input type="text" name="food_close_time_v" id="food_close_time_v" value="<%=vo.getFood_close_time_v() %>" /></td>
							</tr>
							<tr>
								<th><label for="food_close_time_v">가격</label></th>
								<td><input type="text" name="food_price_v" id = "food_price_v" value="<%=vo.getFood_price_v() %> "/></td>
							</tr>
							<tr>
								<th><label for="food_holiday_v">휴일</label></th>
								<td><input type="text" name="food_holiday_v" id="food_holiday_v" value="<%=vo.getFood_holiday_v() %>" /></td> 
							</tr>
							<tr>
								<th><label for="food_type_v">종류</label></th>
								<td>
									<select id="food_type_v" name="food_type_v">
										<option value="K" <%=vo.getFood_type_v().equals("K")?"'selected=selected'":"" %>>한식
										<option value="C" <%=vo.getFood_type_v().equals("C")?"'selected=selected'":"" %>>중식
										<option value="J" <%=vo.getFood_type_v().equals("J")?"'selected=selected'":"" %>>일식
									</select>	
								</td>
							</tr>
							<tr>
								<th><label for="food_loc_v">지역</label></th>
								<td><input type="text" name="food_loc_v" id="food_loc_v" value="<%=vo.getFood_loc_v() %>" /></td>
							</tr>
							<tr>
								<th>등록일</th>
								<td><%=vo.getFood_reg_d()%></td>
							</tr>
							<tr>
								<th>이미지1</th>
								<td>
									<img src="<%=vo.getFood_imgpath_1_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt=""/><br/>
									<input type="file" name="food_imgpath_1_v" id="food_imgpath_1_v" />
								</td>
							</tr>
							<tr>
								<th>이미지2</th>
								<td>
									<img src="<%=vo.getFood_imgpath_2_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
									<input type="file" name="food_imgpath_2_v" id="food_imgpath_2_v" />
								</td>
							</tr>
							<tr>
								<th>이미지3</th>
								<td>
									<img src="<%=vo.getFood_imgpath_3_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
									<input type="file" name="food_imgpath_3_v" id="food_imgpath_3_v" />
								</td>
							</tr>
							<tr>
								<th>이미지4</th>
								<td>
									<img src="<%=vo.getFood_imgpath_4_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
									<input type="file" name="food_imgpath_4_v" id="food_imgpath_4_v" />
								</td>
							</tr>
							<tr>
								<th>이미지5</th>
								<td>
									<img src="<%=vo.getFood_imgpath_5_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
									<input type="file" name="food_imgpath_5_v" id="food_imgpath_5_v" />
								</td>
							</tr>
							<tr>
								<th>이미지6</th>
								<td>
									<img src="<%=vo.getFood_imgpath_6_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
									<input type="file" name="food_imgpath_6_v" id="food_imgpath_6_v" />
								</td>
							</tr>
							<tr>
								<th>이미지7</th>
								<td>
									<img src="<%=vo.getFood_imgpath_7_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
									<input type="file" name="food_imgpath_7_v" id="food_imgpath_7_v" />
								</td>
							</tr>
							<tr>
								<th>이미지8</th>
								<td>
									<img src="<%=vo.getFood_imgpath_8_v()%>" onerror="this.src='/project_datesystem/images/no_image.jpg'" alt="" /><br/>
									<input type="file" name="food_imgpath_8_v" id="food_imgpath_8_v" />
								</td>
							</tr>
						</tbody>
					</table>
					<%
							}
					%>
					<div class="btnArea">
						<!-- <a href="#" class="btn"  id="deleteBtn">삭제</a> -->
						<a href="#" class="btnPink" id="modifyBtn">수정하기</a>
						<a href="#" class="btnPink" id="listBtn">상위페이지</a>
					</div>
				</form>
			</div>
		</div>
	</div>
<%@ include file="/common/sub_footer.jsp" %>