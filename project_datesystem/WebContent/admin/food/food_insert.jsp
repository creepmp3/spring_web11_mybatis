<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(document).ready(function(){
	
	
});

function validate(){
	if($("#food_title_v").val()==""){
		alert("제목 입력해주세요");
		$("#food_title_v").focus();
		return false;
	}
	if($("#food_content_v").val()==""){
		alert("내용을 입력해주세요");
		$("#food_content_v").focus();
		return false;
	}
	if($("#food_tel_v").val()==""){
		alert("전화번호를 입력해주세요");
		$("#food_tel_v").focus();
		return false;
	}
	if($("#food_open_time_v").val()==""){
		alert("OPEN시간을 입력해주세요");
		$("#food_open_time_v").focus();
		return false;
	}
	if($("#food_close_time_v").val()==""){
		alert("CLOSE시간을 입력해주세요");
		$("#food_close_time_v").focus();
		return false;
	}
	if($("#food_price_v").val()==""){
		alert("가격을 입력해주세요");
		$("#food_price_v").focus();
		return false;
	}
	if($("#food_holiday_v").val()==""){
		alert("휴일을 입력해주세요");
		$("#food_holiday_v").focus();
		return false;
	}
	if($("#food_type_v").val()==""){
		alert("종류를 선택해주세요");
		$("#food_type_v").focus();
		return false;
	}
	if($("#food_loc_v").val()==""){
		alert("지역을 입력해주세요");
		$("#food_loc_v").focus();
		return false;
	}
	if(confirm("등록하시겠습니까?")){
		return true;
	}
}
</script>
<style type="text/css">
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<div class="view">
			<form action="./food_insertOk.jsp" onsubmit="return validate()" enctype="multipart/form-data" method="post">
				<input type="hidden" name="food_use_yn_c" value="Y"/>
				<table class="tableView">
					<tr>
						<th><label for="food_title_v">제목</label></th>
						<td><input type="text" name="food_title_v" id="food_title_v" style="width:99%" /></td>
					</tr>
					<tr>
						<th><label for="food_info_title_v">부제목</label></th>
						<td><input type="text" name="food_info_title_v" id="food_info_title_v" style="width:99%" /></td>
					</tr>
					<tr>
						<th><label for="food_content_v">내용</label></th>
						<td>
							<textarea name="food_content_v" id="food_content_v" cols="70" rows="10"></textarea>
						</td>
					</tr>
					<tr>
						<th><label for="food_tel_v">전화번호</label></th>
						<td><input type="text" name="food_tel_v" id="food_tel_v"/></td>
					</tr>
					<tr>
						<th><label for="food_open_time_v">OPEN시간</label></th>
						<td><input type="text" name="food_open_time_v" id="food_open_time_v"/></td>
					</tr>
					<tr>
						<th><label for="food_close_time_v">CLOSE시간</label></th>
						<td><input type="text" name="food_close_time_v" id="food_close_time_v"/></td>
					</tr>
					<tr>
						<th><label for="food_price_v">가격</label></th>
						<td><input type="text" name="food_price_v" id="food_price_v"/></td>
					</tr>
					<tr>
						<th><label for="food_holiday_v">휴일</label></th>
						<td><input type="text" name="food_holiday_v" id="food_holiday_v"/></td>
					</tr>
					<tr>
						<th><label for="food_type_v">종류</label></th>
						<td>
							<select id="food_type_v" name="food_type_v">
								<option value="K">한식
								<option value="C">중식
								<option value="J">일식
							</select>	
						</td>
					</tr>
					<tr>
						<th><label for="food_loc_v">지역</label></th>
						<td><input type="text" name="food_loc_v" id="food_loc_v"/></td>
					</tr>
					<tr>
						<th>이미지1</th>
						<td><input type="file" name="food_imgpath_1_v" id="food_imgpath_1_v" /></td>
					</tr>
					<tr>
						<th>이미지2</th>
						<td><input type="file" name="food_imgpath_2_v" id="food_imgpath_2_v" /></td>
					</tr>
					<tr>
						<th>이미지3</th>
						<td><input type="file" name="food_imgpath_3_v" id="food_imgpath_3_v" /></td>
					</tr>
					<tr>
						<th>이미지4</th>
						<td><input type="file" name="food_imgpath_4_v" id="food_imgpath_4_v" /></td>
					</tr>
					<tr>
						<th>이미지5</th>
						<td><input type="file" name="food_imgpath_5_v" id="food_imgpath_5_v" /></td>
					</tr>
					<tr>
						<th>이미지6</th>
						<td><input type="file" name="food_imgpath_6_v" id="food_imgpath_6_v" /></td>
					</tr>
					<tr>
						<th>이미지7</th>
						<td><input type="file" name="food_imgpath_7_v" id="food_imgpath_7_v" /></td>
					</tr>
					<tr>
						<th>이미지8</th>
						<td><input type="file" name="food_imgpath_8_v" id="food_imgpath_8_v" /></td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" value="확인" class="btnPink ar"/>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>