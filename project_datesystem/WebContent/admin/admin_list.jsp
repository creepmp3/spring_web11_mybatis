<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<style>
.contentView{
	margin-top:5px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}

.ul {
	list-style-image:url('/project_datesystem/images/bullet_blue_2.png');
}
ul li a, ul li {
	text-decoration:none;
	color:#4186A1;
	text-shadow: 1px 1px 3px #909090;
}
.ul2 li {
	list-style-image:url('/project_datesystem/images/bullet_blue_3.png');
	margin-left:15px;
}
</style>
	<div id="content">
		<jsp:include page="/contents/content_menu.jsp"></jsp:include>
		
		<div class="contentView">
				<h2>관리자페이지</h2>
				<ul class="ul">
					<li><a href="./member/member_list.jsp">회원관리</a></li>
					<li><a href="./stay/stay_list.jsp">숙박관리</a></li>
					<li><a href="./stay/reservation/reservation_list.jsp">숙박예약관리</a></li>
					<li><a href="./food/food_list.jsp">주변먹거리관리</a></li>
					<li><a href="./tour/tour_list.jsp">명소관리</a></li>
					<li>
						<ul class="ul2">게시판
							<li><a href="./board/m/boardM_list.jsp">남성게시판</a></li>
						</ul>
					</li>
				</ul>
		</div>
	</div>
<%@ include file="/common/sub_footer.jsp" %>