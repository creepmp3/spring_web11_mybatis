<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<%
	int member_no_n = Integer.parseInt(StringUtil.nvl(request.getParameter("member_no_n"),"0"));
%>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#listBtn").on("click", function(){
		location.href = "member_list.jsp";
		return false;
	});
	
	$("#deleteBtn").on("click", function(){
		if(confirm("삭제하시겠습니까?")){
			$("#frm").attr("action", "./member_deleteOk.jsp").submit();
		}
		return false;
	});
});
</script>
<style type="text/css">
.contentView{
	margin-top:5px;
	margin-bottom:50px;
	clear:both;
	border:0px solid #ccc;
	padding:20px;
}
.btnArea {
	clear:both;
	margin-top:10px;
}
.paging {
	border:0px solid #ccc;
	clear:both;
}
</style>
</head>
<body>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<div class="view">
			<%
				MemberDAO dao = new MemberDAO();
				MemberVO vo = dao.selectOne(member_no_n);
				
				if(vo != null){
			%>
			<form id="frm">
				<table class="tableView">
					<input type="hidden" name="member_no_n" id="member_no_n" value="<%=vo.getMember_no_n() %>" />
					<tr>
						<th>번호</th>
						<td><%=vo.getMember_no_n() %></td>
					</tr>
					<tr>
						<th>아이디</th>
						<td><%=vo.getMember_id_v() %></td>
					</tr>
					<tr>
						<th>비밀번호</th>
						<td><%=vo.getMember_pw_v() %></td>
					</tr>
					<tr>
						<th>닉네임</th>
						<td><%=vo.getMember_nick_v() %></td>
					</tr>
					<tr>
						<th>이름</th>
						<td><%=vo.getMember_nm_v() %></td>
					</tr>
					<tr>
						<th>생년월일</th>
						<td><%=vo.getMember_bday_v() %></td>
					</tr>
					<tr>
						<th>전화번호</th>
						<td><%=vo.getMember_tel_v() %></td>
					</tr>
					<tr>
						<th>휴대폰번호</th>
						<td><%=vo.getMember_cell_v() %></td> 
					</tr>
					<tr>
						<th>아이피</th>
						<td><%=vo.getMember_ip_v()%></td>
					</tr>
					<tr>
						<th>상태</th>
						<td><%=vo.getMember_use_yn_v()%></td>
					</tr>
					<tr>
						<th>타입</th>
						<td><%=vo.getMember_type_v()%></td>
					</tr>
					<tr>
						<th>등록일</th>
						<td><%=vo.getMem_reg_d()%></td>
					</tr>
				</table>
				<%
						}
				%>
				<div id="btnArea">
					<input type="button" id="listBtn" class="btnPink" value="목록"/>
					<input type="button" id="deleteBtn" class="btnPink" value="삭제"/>
					<input type="button" id="writeBtn" class="btnPink" value="등록하기"/>
				</div>
			</form>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>