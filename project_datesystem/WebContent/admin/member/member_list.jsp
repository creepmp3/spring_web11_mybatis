<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/sub_header.jsp" %>
<link rel="stylesheet" href="/project_datesystem/css/table/table.css" />
<script type="text/javascript">
$(function(){
	$("#writeBtn").on("click", function(){
		location.href = "member_insert.jsp";
	});

	$("a[name='idv']").each(function(i, elem){
		$(this).on("click", function(){
			var no = $(this).parent().prev().text();
			$("#member_no_n").val(no);
			$("#moveForm").attr("action", "member_view.jsp").submit();
		});
	});
	
});
</script>
</head>
<body>
<form id="moveForm">
	<input type="hidden" name="member_no_n" id="member_no_n" />
</form>
<div id="content">
	<jsp:include page="/contents/content_menu.jsp"></jsp:include>
	
	<div class="contentView">
		<div class="list">
			<table>
				<thead>
					<tr>
						<th><input type="checkbox" id="chkAll"/></th>
						<th>번호</th>
						<th>아이디</th>
						<th>닉네임</th>
						<th>상태</th>
						<th>타입</th>
						<th>등록일</th>
					</tr>
				</thead>
				<tbody>
					<%
						MemberDAO dao = new MemberDAO();
						ArrayList<MemberVO> list = dao.selectAll();
						
						if(list.size()>0){
							for(MemberVO vo : list){
					%>
							<tr>
								<td><input type="checkbox" name="chk"/></td>
								<td><%=vo.getMember_no_n() %></td>
								<td><a href="#" name="idv"><%=vo.getMember_id_v() %></a></td>
								<td><%=vo.getMember_nick_v() %></td>
						<%-- 		
								<td><%=vo.getMember_pw_v() %></td>
								<td><%=vo.getMember_nm_v() %></td>
								<td><%=vo.getMember_bday_v() %></td>
								<td><%=vo.getMember_tel_v() %></td>
								<td><%=vo.getMember_cell_v() %></td> 
								<td><%=vo.getMember_ip_v()%></td>
						--%>
								<td><%=vo.getMember_use_yn_v()%></td>
								<td><%=vo.getMember_type_v()%></td>
								<td><%=vo.getMem_reg_d()%></td>
							</tr>
					<%
							}
						}else{
					%>
						<tr><td colspan="8">데이터가 없습니다</td></tr>
					<%
						} 
					%>
				</tbody>
			</table>
			<div id="btnArea">
				<input type="button" id="deleteBtn" class="btnPink" value="삭제"/>
				<input type="button" id="writeBtn" class="btnPink" value="등록하기"/>
			</div>
		</div>
	</div>
</div>
<%@ include file="/common/sub_footer.jsp" %>