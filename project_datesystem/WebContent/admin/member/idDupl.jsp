<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="dao.MemberDAO, dao.MemberVO, util.StringUtil" %>
<%
	String uri = request.getRequestURI();
	String id = StringUtil.nvl(request.getParameter("id"), "");
	
	MemberDAO dao = new MemberDAO();
	int count = dao.idDupl(id);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<title> 오후 2:50:35</title>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.js"></script>
<script type="text/javascript">
$(function(){
	var count = "<%=count%>";
	var msg = "";
	var btn = "";
	if(count>0){
		msg = "중복된 아이디 입니다";
	}else{
		msg = "사용가능한 아이디 입니다";
		btn = "<input type='button' id='useId' value='사용하기' />";
	}
	var id = "<%=id%>";
	if(id != ""){
		$("#msg").append("<span>"+msg+"</span>"+btn);
	}


	$("#useId").on("click", function(){
		opener.document.getElementById("member_id_v").value=$("#id").val();
		window.close();
	});
});
</script>
<style type="text/css">
a {text-decoration:none;}
#wrap {
	width:100%;
}
.tableList {
	width:100%;
}
.tableList, .tableList th, .tableList td {
	color:#505739;
	border:1px solid #A9A9A9;
	border-collapse:collapse;
	text-align:center;
}
.tableList td a {
	color:#505739;
}
.tableList th {
	background:linear-gradient(to bottom, #eae0c2 5%, #ccc2a6 150%);
	background-color:#eae0c2;
}
.tableList td:nth-child(3) {
	padding-left:5px;
	text-align:left;
}
#btnArea {
	clear:both;
	float:right;
}
#paging {
	width:450px;
	height:30px;
	border:0px solid black;
	margin-top:-5px;
	float:right;
}

.pagingTalbe a, .pagingTalbe p {
	float:left;
	width:20px;
	margin-right:2px;
}
p.pagingBtn {
	font-weight:bold;
}

.pagingBtn {
	-moz-box-shadow: 0px 0px 2px 0px #1c1b18;
	-webkit-box-shadow: 0px 0px 2px 0px #1c1b18;
	box-shadow: 0px 0px 2px 0px #1c1b18;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #eae0c2), color-stop(1, #ccc2a6));
	background:linear-gradient(to bottom, #eae0c2 5%, #ccc2a6 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#eae0c2', endColorstr='#ccc2a6',GradientType=0);
	background-color:#eae0c2;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border-radius:5px;
	border:0px solid #333029;
	display:inline-block;
	cursor:pointer;
	color:#505739;
	font-family:arial;
	font-size:14px;
	padding:5px 5px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
}
.pagingBtn:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ccc2a6), color-stop(1, #eae0c2));
	background:-moz-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:-webkit-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:-o-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:-ms-linear-gradient(top, #ccc2a6 5%, #eae0c2 100%);
	background:linear-gradient(to bottom, #ccc2a6 5%, #eae0c2 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ccc2a6', endColorstr='#eae0c2',GradientType=0);
	background-color:#ccc2a6;
}
.pagingBtn:active {
	position:relative;
	top:1px;
}

.pagingTalbe {
	text-align:center;
	width:450px;
	margin:-5px;
	height:30px;
}
.pagingTalbe, .pagingTalbe td {
	padding:0px;
	border:0px solid black;
	border-collapse:collapse;
}
#searchWrap {
	margin:0px;
	float:right;
}
</style>
</head>
<body>
	<div id="wrap">
		<form id="frm" action="<%=uri%>">
			<table class="tableList">
				<tr>
					<td><input type="text" name="id" id="id" value="<%=id%>"/></td>
					<td><input type="submit" class="pagingBtn" value="확인"/></td>
				</tr>
			</table>
		</form>
		<div id="msg"></div>
	</div>
</body>
</html>