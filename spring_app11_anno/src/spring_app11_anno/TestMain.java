package spring_app11_anno;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestMain {
    public static void main(String[] args) {
        
        ApplicationContext ctx = new AnnotationConfigApplicationContext(conf.class);
        
        Monitor m = ctx.getBean("monitor", Monitor.class);
        m.showMonitor();
    }
}
