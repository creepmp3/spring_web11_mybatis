package kr.co.hbilab.app;

import java.util.List;

//20150527
public class EmpDAO implements Dao{
    
    private ConnectionManager cm;
    
    public void setCm(ConnectionManager cm) {
        this.cm = cm;
    }

    @Override
    public List<EmpDTO> selectAll() {
        return cm.getFactory().openSession(true).selectList("selectAll");
    }

    @Override
    public EmpDTO selectOne(int no) {
        return cm.getFactory().openSession(true).selectOne("selectOne", no);
    }

    @Override
    public void insertOne(EmpDTO dto) {
        int result = cm.getFactory().openSession(true).update("insertOne", dto);
        if(result>0){
            System.out.println("등록성공");
        }else{
            System.out.println("등록실패");
        }
    }

    @Override
    public void updateOne(EmpDTO dto) {
        int result = cm.getFactory().openSession(true).update("updateOne", dto);
        if(result>0){
            System.out.println("수정성공");
        }else{
            System.out.println("수정실패");
        }
    }

    @Override
    public void deleteOne(int no) {
        int result = cm.getFactory().openSession(true).update("deleteOne", no);
        if(result>0){
            System.out.println("삭제성공");
        }else{
            System.out.println("삭제실패");
        }
    }
    
}
