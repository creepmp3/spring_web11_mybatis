/**
 * 2015. 6. 8.
 * @author KDY
 */
package kr.co.hbilab.web;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class MemDAO {
    private JdbcTemplate jdbcTemplate;
    StringBuffer sql = new StringBuffer();
    
    public MemDAO(){
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    public List<MemDTO> selectAll(){
        sql.setLength(0);
        sql.append("\n SELECT * FROM MEM ");
        
        RowMapper rm = new RowMapper(){
            @Override
            public MemDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                String id = rs.getString("id");
                String pw = rs.getString("pw");
                String email = rs.getString("email");
                
                MemDTO dto = new MemDTO();
                dto.setId(id);
                dto.setPw(pw);
                dto.setEmail(email);
                return dto;
            }
        };
        
        List<MemDTO> list = jdbcTemplate.query(sql.toString(), rm);
        return list;
    }
    
    public int insertOne(MemDTO dto){
        int result = 0;
        sql.setLength(0);
        sql.append("\n INSERT INTO MEM(id, pw, email) ");
        sql.append("\n VALUES(?, ?, ?) ");
        
        result = jdbcTemplate.update(sql.toString(), dto.getId(), dto.getPw(), dto.getEmail());
        return result;
    }
}
