/**
 * 2015. 6. 8.
 * @author KDY
 */
package kr.co.hbilab.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/member")
public class RegisterController {
    
    @Autowired
    MemDAO dao;
    
    @RequestMapping(value="registerForm")
    public String registerForm(Model model){
        MemDTO dto = new MemDTO();
        model.addAttribute("dto", dto);
        return "registerForm";
    }
    
    @RequestMapping(value="registerOk")
    public String registerOk(@ModelAttribute("dto") MemDTO dto, BindingResult result){
        new RegisterValidator().validate(dto, result);
        if(result.hasErrors()){
            return "registerForm";
        }else{
            int res = dao.insertOne(dto);
            if(res>0){
                System.out.println("등록성공");
            }else{
                System.out.println("등록실패");
            }
            return "registerOk";
        }
    }
    
    @RequestMapping(value="memList")
    public String memList(Model model){
        List<MemDTO> list = dao.selectAll();
        model.addAttribute("list", list);
        return "memList";
    }
}
