/**
 * 2015. 6. 8.
 * @author KDY
 */
package kr.co.hbilab.web;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class RegisterValidator implements Validator{

    @Override
    public boolean supports(Class<?> clazz) {
        // 전달받은 clazz 매개변수가 MemDTO.class와 같은지 검사
        return MemDTO.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        
        // 각 필드에 값을 입력하지 않았을때
        // 필수입력사항 임을 알리는 에러
        // ValidationUtils.rejectIfEmpty : Empty 일때
        // ValidationUtils.rejectIfEmptyOrWhitespace : 공백포함?
        
        // ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "id.required", "ID 입력은 필수입니다");
        // ValidationUtils.rejectIfEmptyOrWhitespace에 에러메세지를 직접 주지않고 
        // errorCode에 대한 메세지를 Message_ko.properties에 따로설정
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "id", "id.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirm", "confirm.required");
        
        MemDTO dto = (MemDTO) target;
        // 패스워드는 최소6자~ 최대20자 까지만 지원
        // pw == confirm 
        
        
        if(dto.getPw()==null || dto.getPw().equals("")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pw", "pw.required");
        }else{
            if(dto.getPw().length()<6){
                errors.rejectValue("pw", "pw.short");
            }else if(dto.getPw().length()>20){
                errors.rejectValue("pw", "pw.long");
            }else if(!dto.getPw().equals(dto.getConfirm())){
                errors.rejectValue("confirm", "pw.diff");
            }
        }
        if(dto.getEmail()==null || dto.getEmail().equals("")){
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "email.required");
        }else{
            if(dto.getEmail().indexOf("@") == -1 || dto.getEmail().indexOf(".") == -1){
                errors.rejectValue("email", "email.notAt");
            }
        }
    }
    
}
