package kr.co.hbilab.dao;

/**
 * 2015. 6. 1.
 * @author KDY
 */

import java.util.List;

import kr.co.hbilab.dto.MemDTO;

public interface MemDAO {
    public void insertOne(MemDTO dto);
    public List<MemDTO> selectAll();
    public MemDTO selectOne(MemDTO dto);
}
