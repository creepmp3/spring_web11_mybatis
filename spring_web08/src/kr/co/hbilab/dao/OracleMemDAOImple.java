package kr.co.hbilab.dao;

import java.util.List;

import kr.co.hbilab.dto.MemDTO;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 2015. 6. 1.
 * @author KDY
 */
public class OracleMemDAOImple implements MemDAO{
    private JdbcTemplate jdbcTemplate;
    StringBuffer sql = new StringBuffer();
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void insertOne(MemDTO dto) {
        sql.setLength(0);
        System.out.println("jdbcTemplate : "+jdbcTemplate);
        sql.append("\n INSERT INTO MEM(id, pw, email)VALUES(?, ?, ?) ");
        int result = jdbcTemplate.update(sql.toString(), dto.getId(), dto.getPw(), dto.getEmail());
        if(result>0){
            System.out.println("등록완료");
        }else{
            System.out.println("등록실패");
        }
    }

    @Override
    public List<MemDTO> selectAll() {
        sql.setLength(0);
        sql.append("\n SELECT * FROM MEM ");
        List<MemDTO> list = jdbcTemplate.queryForList(sql.toString(), MemDTO.class);
        return list;
    }

    @Override
    public MemDTO selectOne(MemDTO dto) {
        
        return null;
    }

    
    
    
}
