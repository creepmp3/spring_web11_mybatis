package kr.co.hbilab.app;

import java.sql.Connection;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class TestMain{
    public static void main(String[] args) {
            
        BeanFactory factory = new XmlBeanFactory(new FileSystemResource("src/app.xml"));
        CommonDAO dao = factory.getBean("dao", CommonDAO.class);
        
        Connection conn = dao.connect();
        System.out.println("conn : "+conn);
        dao.selectAll();
    }
}
