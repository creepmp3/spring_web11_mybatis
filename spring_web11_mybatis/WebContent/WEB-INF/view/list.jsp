<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
th, td {
    border:1px solid black;
    border-collapse:collapse;
}
th {
    background-color:#dedede;
}
table tr:last-child td {
    border:0px solid black;
    text-align:right;
}
</style>
</head>
<body>
    <form action="delDept" method="post">
	    <table>
	        <tr>
	            <th></th>
	            <th>deptno</th>
	            <th>dname</th>
	            <th>loc</th>
	        </tr>
	    <c:forEach items="${list }" var="dto">
	        <tr>
	            <td><input type="checkbox" name="deptno" value="${dto.deptno }"/></td>
	            <td><a href="view?deptno=${dto.deptno }"><c:out value="${dto.deptno }"/></a></td>
	            <td><c:out value="${dto.dname }"/></td>
	            <td><c:out value="${dto.loc }"/></td>
	        </tr>
	    </c:forEach>
	        <tr>
                <td colspan="4">
                    <a href="addDept"><input type="button" value="부서추가"/></a>
                    <input type="submit" value="삭제" />
                </td>
            </tr>
	    </table>
    </form>
</body>
</html>