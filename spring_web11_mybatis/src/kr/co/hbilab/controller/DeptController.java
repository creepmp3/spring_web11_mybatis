/**
 * 2015. 6. 10.
 * @author KDY
 */

package kr.co.hbilab.controller;

import kr.co.hbilab.dao.Dao;
import kr.co.hbilab.dto.DeptDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DeptController {
    @Autowired
    Dao dao;

    
    @RequestMapping(value="addDept", method=RequestMethod.GET)
    public String addDept(Model model){
        DeptDTO dto = new DeptDTO();
        model.addAttribute("dto", dto);
        return "addDeptForm";
    }
    
    @RequestMapping(value="addDept", method=RequestMethod.POST)
    public String addDeptOk(Model model, @ModelAttribute("dto") DeptDTO dto){
        int result = dao.insertOne(dto);
        if(result>0){
            System.out.println("등록성공");
            return "redirect:deptList";
        }else{
            System.out.println("등록실패");
            return "addDeptForm";
        }
    }
    
    @RequestMapping(value="/deptList")
    public String list(Model model){
        model.addAttribute("list", dao.selectAll());
        return "list";
    }
    
    @RequestMapping(value="/view")
    public String view(@RequestParam("deptno") int deptno, Model model){
        model.addAttribute("dto", dao.selectOne(deptno));
        return "view";
    }
    
    @RequestMapping(value="modify")
    public String modify(@RequestParam("deptno") int deptno, Model model){
        model.addAttribute("dto", dao.selectOne(deptno));
        return "modify";
    }
    
    @RequestMapping(value="modifyOk")
    public String modifyOk(@ModelAttribute("dto") DeptDTO dto, Model model){
        int result = dao.updateOne(dto);
        if(result>0){
            System.out.println("수정성공");
            return "redirect:deptList";
        }else{
            System.out.println("수정실패");
            model.addAttribute("deptno", dto.getDeptno());
            return "modify";
        }
    }
    
    @RequestMapping(value="delDept", method=RequestMethod.GET)
    public String delDept(Model model, @RequestParam("deptno") int deptno){
        model.addAttribute("deptno", deptno);
        return "delDept";
    }
    
    @RequestMapping(value="delDept", method=RequestMethod.POST)
    public String delDeptOk(@RequestParam("deptno") int[] deptno, Model model){
        int result = 0;
        for(int i=0; i<deptno.length; i++){
            result = dao.deleteOne(deptno[i]);
        }
        if(result>0){
            System.out.println("삭제성공");
            return "redirect:deptList";
        }else{
            System.out.println("삭제실패");
            return "redirect:view";
        }
    }
    
}
