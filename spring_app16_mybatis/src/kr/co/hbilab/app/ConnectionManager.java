package kr.co.hbilab.app;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

//20150527
public class ConnectionManager {
   
    static SqlSessionFactory factory;
   
   
    // static block
    // 객체가 new 되기 전에 먼저 실행되는 구역
    static{
        try{
            Reader r = Resources.getResourceAsReader("resource/SqlConfig.xml");
            SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
            factory = builder.build(r);
            r.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    
    public static SqlSessionFactory getFactory(){
        return factory;
    }
}
