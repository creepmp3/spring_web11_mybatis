package kr.co.hbilab.app;

public class MessageImple implements Message{
    String msg;
    
    public MessageImple(){
    }
    
    public MessageImple(String msg){
        this.msg = msg;
    }
    
    @Override
    public void sayHello() {
        System.out.println("안녕하세요");
        System.out.println("오늘의 명언 : " + msg);
    }
    
}
