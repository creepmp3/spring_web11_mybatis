package spring_app7_collection;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class CollectionImple implements CollectionInter{

    private List<String> list;
    private Map<String, String> map;
    
    
    public CollectionImple(){
        
    }
    
    public CollectionImple(List<String> list, Map<String, String> map){
        super();
        this.list = list;
        this.map = map;
    }
    
    public void setList(List<String> list){
        this.list = list;
    }
    public void setMap(Map<String, String> map){
        this.map = map;
    }


    @Override
    public void printList() {
        // 리스트안에 있는 객체 출력
        for(String s : list){
            System.out.println(s);
        }
    }

    @Override
    public void printMap() {
        // 맵 안에 등록되어 있는 모든 객체 출력
        // map.get(key);
        
        Set<String> key = map.keySet();
        Iterator it = key.iterator();
        
        while(it.hasNext()){
            Object obj = it.next();
            System.out.println(map.get(obj));
        }
    }
    
    
}
